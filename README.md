# FanPy #
FanPy (Field and Neuron Python) is a Python package for computing the field distribution during deep brain stimulation and estimating the neural activation resulting from the applied electrical stimulus. Although the model pipeline is based on an application for deep brain stimulation, it is not limited to it and can be easily transfered to various bioelectrical applications.
The Python package was used to estimate the neural activation extent in models of deep brain stimulation rendering different post-operative phases and target area properties, while reducing the computational expense by employing an approximation of the neural activation extent by field threshold values determined from the threshold-distance relationship of the applied axon models. For further details please refer to:

Schmidt, C and van Rienen, U, Adaptive Estimation of the Neural Activation Extent in Computational Volume Conductor Models of Deep Brain Stimulation, 2017, [arXiv:1705.10478](https://arxiv.org/abs/1705.10478) [q-bio.NC]

The code is highly experimental and should be used with caution and a proper amount of healthy suspicion.

FanPy is published under the GNU Lesser General Public License version 3.

### Installation of the Package ###
It is recommended to setup the package under Linux (Ubuntu), as it was tested and run in this environment. The python code was implemented using Python 2.7. If not noted differently, it is recommended to install the packages using [PIP](https://pypi.python.org/pypi/pip) with the command "pip install modulename --user".

* Download the current release
* Meet the following Module and software dependencies:
    * **FEnICS**: For Install instructions see https://fenicsproject.org/download/.
    * **NEURON**: Has to be installed with the Python API. For Install instructions see https://www.neuron.yale.edu/neuron/download/getstd.
    * **gmsh**: Currently used for meshing. Has to be installed and added to system path. For Install instructions see http://gmsh.info
    * **numpy**: Fundamental package for scientific computing. Can be installed with PIP 
    * **scipy**: Collection of high level science and engineering modules. Can be installed with PIP
    * **pyevtk**: Exporting field solutions and activation extents to the Paraview readable VTK format. Can be installed with PIP
    * **h5py**: Storing data in HDF5 format. Can be installed with PIP
    * **cPickle**: Storing python class instance. Can be installed with PIP
    * **enum**: Enum support for Python 2.7. Can be installed with PIP
    * **matplotlib**: For plotting the results. Can be installed with PIP
* For running the study of the mentioned paper execute ./study/studies.py (it is recommended to enable the debug flag in the studies.py code for a first test run)