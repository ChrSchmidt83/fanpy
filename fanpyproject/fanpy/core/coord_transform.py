"""
Created on 03.02.2017

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPy
"""

import numpy as np

class Transform():
    """
    Transform()
    
    Transformation of coordinates using linear transformations.
    """
    
    def __init__(self):
        self.__position_vector = np.array([0, 0, 0])  # __position_vector [x,y,z]
        self.__rotation_matrix = np.eye(4, 4) # standard rotation matrix
        self.__transform_matrix = np.eye(4, 4)  # standard transform matrix
        self.__global_transform_vector = np.zeros(3,) # standard x,y,z coordinate system
        
    def __get_position_matrix(self):
        return np.array([[1, 0, 0, self.__position_vector[0]],
                         [0, 1, 0, self.__position_vector[1]],
                         [0, 0, 1, self.__position_vector[2]],
                         [0, 0, 0, 1]])
        
    def __get_global_transform_matrix(self):
        global_transform_vector = self.__global_transform_vector.copy()
        if np.linalg.norm(global_transform_vector)>0:
            global_transform_vector = global_transform_vector/np.linalg.norm(global_transform_vector)
        
        global_transform_matrix = np.eye(4)
        global_transform_matrix[0:3, 0:3] = np.eye(3)-2*np.outer(global_transform_vector,
                                                                 global_transform_vector)
        
        return global_transform_matrix

    def transform_coords(self, coords):
        """
        Transform coordinates.
    
        Parameters
        ----------
        coords : numpy array 
            Coordinates array of shape (n,3)
    
        Returns
        -------
        numpy array
            Transformed coordinates
    
        """
        transformed_coords = np.zeros((coords.shape[0], 4))
        transformed_coords[:, 0:3] = coords
        transformed_coords[:, 3] = np.ones((coords.shape[0],))
        transformed_coords = np.transpose(transformed_coords) # nx3 -> 3xn
        # apply initial transformation and __position_vector
        transformed_coords = self.__transform_matrix.dot(transformed_coords)
        transformed_coords = self.__get_position_matrix().dot(transformed_coords)
        
        # get 3d shape of nodes
        transformed_coords = transformed_coords[0:-1]        
        transformed_coords = np.transpose(transformed_coords)  # 3xn -> nx3
        
        # applay rotation and global transformation
        transformed_coords = self.transform_coords_to_plane(transformed_coords)
        
        return transformed_coords
    
    def transform_coords_to_plane(self, coords):
        """
        Transform coordinates only to the plane (omitting coordinate transformations
        inside the plane).
    
        Parameters
        ----------
        coords : numpy array
            Coordinates array of shape (n,3)
    
        Returns
        -------
        numpy array
            Transformed coordinates
    
        """
        transformed_coords = np.zeros((coords.shape[0], 4))
        transformed_coords[:, 0:3] = coords
        transformed_coords[:, 3] = np.ones((coords.shape[0],))
        transformed_coords = np.transpose(transformed_coords) # nx3 -> 3xn
        # apply rotation
        transformed_coords = self.__rotation_matrix.dot(transformed_coords)
        
        # apply transformation of global coordinate system
        transformed_coords = self.__get_global_transform_matrix().dot(transformed_coords)
        
        # get 3d shape of coords
        transformed_coords = transformed_coords[0:-1]
        
        transformed_coords = np.transpose(transformed_coords)  # 3xn -> nx3
        
        return transformed_coords
    
    def inverse_transform_coords(self, coords):
        """
        Inverse transformation of coordinates. Can be used to transform prior
        transformed coordinates back to their reference location.
    
        Parameters
        ----------
        coords : numpy array
            Coordinates array of shape (n,3)
    
        Returns
        -------
        numpy array
            Transformed coordinates
    
        """
        # apply rotation and global transformation
        transformed_coords_plane = self.inverse_transform_coords_to_plane(coords)
        
        transformed_coords = np.zeros((transformed_coords_plane.shape[0], 4))
        transformed_coords[:, 0:3] = transformed_coords_plane
        transformed_coords[:, 3] = np.ones((transformed_coords_plane.shape[0],))
        transformed_coords = np.transpose(transformed_coords) # nx3 -> 3xn

        # apply initial transformation and __position_vector
        pminv = np.linalg.inv(self.__get_position_matrix())
        transformed_coords = pminv.dot(transformed_coords)
        tminv = np.linalg.inv(self.__transform_matrix)
        transformed_coords = tminv.dot(transformed_coords)
        
        # get 3d shape of coords
        transformed_coords = transformed_coords[0:-1]
        
        transformed_coords = np.transpose(transformed_coords)  # 3xn -> nx3
        
        return transformed_coords
    
    def inverse_transform_coords_to_plane(self, coords):
        """
        Inverse transformation of coordinates to the plane. Can be used to transform
        prior transformed coordinates b ack to their reference plane.
    
        Parameters
        ----------
        coords : numpy array
            Coordinates array of shape (n,3)
    
        Returns
        -------
        numpy array
            Transformed coordinates
    
        """
        transformed_coords = np.zeros((coords.shape[0], 4))
        transformed_coords[:, 0:3] = coords
        transformed_coords[:, 3] = np.ones((coords.shape[0],))
        transformed_coords = np.transpose(transformed_coords) # nx3 -> 3xn
        # apply transformation of global coordinate system
        gtminv = np.linalg.inv(self.__get_global_transform_matrix())
        transformed_coords = gtminv.dot(transformed_coords)    
        
        # apply rotation, transformation and __position_vector
        rminv = np.linalg.inv(self.__rotation_matrix)
        transformed_coords = rminv.dot(transformed_coords)
        
        # get 3d shape of coords
        transformed_coords = transformed_coords[0:-1]
        
        transformed_coords = np.transpose(transformed_coords)  # 3xn -> nx3
        
        return transformed_coords
    
    def set_position_vector(self, position_vector):
        """
        Set the position vector for defining the local coordinate system origin
        within the global coordinate system.
    
        Parameters
        ----------
        position_vector : numpy array
            Position vector of shape (3,)
    
        """
        self.__position_vector = position_vector
        
    def get_position_vector(self):
        """
        Returns
        -------
        numpy array
            Position vector of the local coordinate system origin of shape (3,)
    
        """
        return self.__position_vector
    
    def set_transform_matrix(self, transform_matrix):
        """
        Set the transform matrix for defining the local coordinate system
        transformation, e.g. axis flipping / mirroring.
    
        Parameters
        ----------
        transform_matrx : numpy array
            Transform matrix of shape (4,4)
    
        """
        self.__transform_matrix = transform_matrix
        
    def get_transform_matrix(self):
        """
        Returns
        -------
        numpy array
            Transform matrix of the local coordinate system of shape (4,4)
    
        """
        return self.__transform_matrix
               
    def set_rotation_matrix(self, rotation_matrix):
        """
        Set the rotation matrix for defining the local coordinate system
        transformation
    
        Parameters
        ----------
        rotation_matrx : numpy array
            Rotation matrix of shape (4,4)
    
        """
        self.__rotation_matrix = rotation_matrix
        
    def get_rotation_matrix(self):
        """
        Returns
        -------
        numpy array
            Rotation matrix of the local coordinate system of shape (4,4)
    
        """
        return self.__rotation_matrix
        
    def set_global_transform(self, global_transform_vector):
        """
        Set the transform for defining the global coordinate system
        origin and axis orientation.
    
        Parameters
        ----------
        global_transform_vector : numpy array
            Transform vector of shape (3,) defining the normal vector of the
            mirroring plane applied to the default cartesian coordinate system.
    
        """
        self.__global_transform_vector = global_transform_vector
        
    def get_global_transform_vector(self):
        """
        Returns
        -------
        numpy array
            Transform vector of the globale coordinate system of shape (3,)
    
        """
        return self.__global_transform_vector
        
    def create_copy(self):
        """
        Creates a copy of this Transform instance by copying the transform vectors
        and matrices to a new Transform instance.
        
        Returns
        -------
        Transform()
    
        """
        transform = Transform()
        transform.set_position_vector(self.get_position_vector())
        transform.set_transform_matrix(self.get_transform_matrix())
        transform.set_rotation_matrix(self.get_rotation_matrix())
        transform.set_global_transform(self.get_global_transform_vector())
        return transform