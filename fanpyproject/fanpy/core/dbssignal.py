'''
Created on 13.02.2017

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPy
'''

from fanpy.core.dsp import TVector
import numpy as np

class DBSsignal(TVector):
    """
    DBSsignal(params)
    
    Creates the DBS signal

    Parameters
    ----------
    params : dict 
        FREQUENCY        float
            Repetition frequency of the DBS pulse
        PULSE_DURATION   float
            Duration of the active stimulus pulse
        AMPLITUDE        float
            Amplitude of the active stimulus pulse
        DELAY            float
            Delay for the onset of the active stimulus pulse
        NUMBER_OF_PULSES int
            Number of DBS pulses in the DBS pulse train (default = 1)
        CB_AMPLITUDE    float
            Amplitude of the (charge-) compensation pulse (default chosen to be
            high enough to counter the charge accumulation in the active stimulus pulse
        CB_DELAY        float
            Delay of the onset of the compensation pulse after the active stimulus pulse.
            If not set, no compensation pulse will be modeled.
        CB_DURATION    float
            Duration of the compensation pulse. If not set, no compensation pulse
            will be modeled.
        DT            float
            Timestep of the DB signal
        TYPE          String
            Type of stimulation signal. 'current': current-controlled
                                        'voltage': voltage-controlled

    """

    FREQUENCY = 'frequency'
    PULSE_DURATION = 'pulse_duration'
    AMPLITUDE = 'amplitude'
    DELAY = 'delay'
    NUMBER_OF_PULSES = 'pulse_number'
    CB_AMPLITUDE = 'cb_amplitude'
    CB_DURATION = 'cb_duration'
    CB_DELAY = 'db_delay'
    DT = 'dt'
    TYPE = 'type'

    def __init__(self, params):
        """Return the time vector and meta data for a DBS pulse with the
        given parameters."""
        self.params = params
        dt = params[self.DT]
        freq = params[self.FREQUENCY]
        delay = 0.0 if (not self.DELAY in params) else float(params[self.DELAY])
        amp = params[self.AMPLITUDE]
        pulse_dur = params[self.PULSE_DURATION]

        t = np.arange(0, 1./freq, dt)
        x = amp*(pulse_dur/2. > abs(t-(pulse_dur/2.+delay)))
        
        # if parameters for a biphasic pulse are given
        if self.CB_DURATION in params and self.CB_DELAY in params:
            cb_dur = params[self.CB_DURATION]
            cb_delay = params[self.CB_DELAY]
            if self.CB_AMPLITUDE in params:
                # custom defined amplitude
                cb_amp = params[self.CB_AMPLITUDE]
            else:
                # charge-balanced reversal amplitude to stimulation pulse
                cb_amp = -amp*pulse_dur/float(cb_dur);
            x_cb = cb_amp*(cb_dur/2. > abs(t-(cb_dur/2.+cb_delay+pulse_dur+delay)))
            x = x +x_cb
        
        if self.NUMBER_OF_PULSES in params:
            self.__n_pulse = params[self.NUMBER_OF_PULSES]
        else:
            self.__n_pulse = 1
        
        if self.__n_pulse > 1:
            x_single = x
            for _ in range (0,self.__n_pulse-1):
                x = np.concatenate((x, x_single), axis = 0)
        
        TVector.__init__(self, x, dt)
        
        
    def get_params(self):
        """
        Returns
        -------
        dict  
            Parameters described in the class documentation.
    
        """
        return self.params

    def get_n_pulse(self):
        """
        Returns
        -------
        int  
            Number of pulses
    
        """
        return self.__n_pulse