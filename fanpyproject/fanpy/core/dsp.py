"""
Created on 31.01.2017

Methods for Fourier Transform and holding frequency and time vectors.

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPy
"""

import numpy as np

def myfft(t_vector, smoothFlag):
    """
    Fast Fourier Transformation for a given real valued time
    vector. The FFT is performed symmetric, i.e. only half of the spectrum
    is used.
    
    Parameters
    ----------
    t_vector : TVector 
        Time vector to be transformed into frequency-domain
    smoothFlag : Boolean
        If true, the spectrum is smoothed by Lanczos filtering

    Returns
    -------
    Fvector
        Frequency vector of the given time vector

    """
    N = t_vector.get_vector().size  # number of time samples
    X = np.fft.fft(t_vector.get_vector())  # FFT spectrum of DBS signal
    # half the samples required (inlcuding dc component)
    cutOff = int(np.ceil((N+1)/2.))
    Xs = X[0:cutOff]  # half the spectrum required

    if smoothFlag is True:
        Xs = Xs*np.sinc(
                 np.arange(0, cutOff, 1)/float(cutOff))

    if t_vector.get_dt() is None:
        fs = None
    else:
        fs = 1./t_vector.get_dt()

    return FVector(Xs, N, fs)


def myifft(f_vector):
    """
    Inverse Fast Fourier Transformation for a given given symmetric
    spectrum obtained by myfft(t_vector, smoothFlag).
    
    Parameters
    ----------
    f_vector : FVector 
        Frequency vector to be transformed into time-domain

    Returns
    -------
    Tvector
        Time vector of the given frequency vector

    """
    if np.mod(f_vector.get_nfft(), 2):  # if the FT vector is odd
        fv_conj = np.conjugate(f_vector.get_vector()[-1:0:-1])
    else:  # if the FT vector is even
        fv_conj = np.conjugate(f_vector.get_vector()[-2:0:-1])
    Y = np.concatenate((f_vector.get_vector(), fv_conj), axis=0)

    if f_vector.get_fs() is None:
        dt = None
    else:
        dt = 1./f_vector.get_fs()

    return TVector(np.fft.ifft(Y).real, dt)

def convolute(f_signal, f_transfer):
    """
    Convolute two frequency vectors (signal and transfer function)
    
    Parameters
    ----------
    f_signal : FVector 
        Frequency vector of the signal
    f_transfer : FVector
        Frequency vector of the transfer function

    Returns
    -------
    Fvector
        Convoluted frequency vector

    """
    return FVector(f_signal.get_vector()*f_transfer.get_vector(),
                   f_signal.get_nfft(),
                   f_signal.get_fs())

class TVector():
    """
    TVector(x, dt)
    
    Time vector holding information on a time-dependent signal.
    
    Parameters
    ----------
    x : numpy.array 
        The discretized signal
    dt : float
        Time step of the signal

    """

    def __init__(self, x, dt):
        self.__x = x
        self.__dt = dt

    def get_vector(self):
        """
        Returns
        -------
        numpy.array
            Discretized signal
    
        """
        return self.__x
    
    def get_dt(self):
        """
        Returns
        -------
        float
            Time step.
    
        """
        return self.__dt

    def get_time_vector(self):
        """
        Returns
        -------
        numpy.array
            Time samples for the corresponding time vector
    
        """
        if self.__dt is None:
            dt = 1
        else:
            dt = self.__dt
        return np.arange(0, self.__x.size, 1)*dt


class FVector():
    """
    FVector(xf_symm, nfft, f_sample)
    
    Frequency vector holding information on a frequency-dependent signal.
    
    Parameters
    ----------
    xf_symm : numpy.array 
        The discretized signal in frequency domain (only the half spectrum)
    nfft : int
        Number of fourier components (full spectrum)
    f_sample : float
        Sampling frequency

    """

    def __init__(self, xf_symm, nfft, f_sample):
        self.__xf_symm = xf_symm
        self.__nfft = nfft
        self.__f_sample = f_sample

    def get_vector(self):
        """
        Returns
        -------
        numpy.array
            Components of the frequency-dependent signal (symmetric half spectrum)
    
        """
        return self.__xf_symm

    def get_nfft(self):
        """
        Returns
        -------
        int
            Number of frequency components (full spectrum)
    
        """
        return self.__nfft
    
    def get_fs(self):
        """
        Returns
        -------
        float
            Sampling frequency
    
        """
        return self.__f_sample

    def get_frequency_vector(self):
        """
        Returns
        -------
        numpy.array
            Frequency samples vector for the corresponding spectrum
    
        """
        fv = np.arange(0, self.__xf_symm.size, 1)
        fv = fv/float(self.__nfft)
        if self.__f_sample is None:
            fs = 1
        else:
            fs = self.__f_sample
        return fs*fv