"""
Created on 30.01.2017

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPy
"""

import numpy as np
from enum import Enum


class DielectricProperties:
    """
    DielectricProperties(tissue)
    
    Dielectric properties of biological tissue based on a 4-term ColeCole model
    with parameters determined in:
    Gabriel S, Lau R W, Gabriel C, 1996, The dielectric properties of
    biological tissue: III. Parametric models for the dielectric spectrum of
    tissues, Phys Med Biol, vol. 41, pp. 2271-2293    
    
    Parameters
    ----------
    tissue : ETissue 
        name of the biological tissue

    """

    FREQUENCIES = 'frequencies'
    CONDUCTIVITY = 'conductivity'
    PERMITTIVITY = 'permittivity'

    def __init__(self, tissue):
        self.tissue = tissue
        self.params = ColeColeParams(tissue)
        self.poles = 4
        
    def set_poles(self, poles):
        """
        Number of poles for the ColeCole model.
        
        Parameters
        ----------
        poles : int 
            1 to 4 poles, beginning with the pole with lowest frequency
            default = 1

        """
        if poles > 0 and poles < 5:
            self.poles = poles
        else:
            print 'Number of relaxation poles has to be in [1,4]'

    def get_dielectrics(self, frequencies):
        """
        Dielectric tissue properties for the given frequency (frequencies)
        
        Parameters
        ----------
        frequencies : float or numpy.array 
            Frequency or frequencies at which the dielectric should be determined
            
        Returns
        -------
        dict
            FREQUENCIES: float or numpy.array
                frequencies to the corresponding dielectric properties
            CONDUCTIVITY: float or numpy.array
                conductivity values
            PERMITTIVITY: float or numpy.array
                relative permittivity values
    
        """
        isVector = isinstance(frequencies, (list, tuple, np.ndarray))
        freq = frequencies
        if isVector is False:
            freq = np.array([frequencies])
        
        idxs_zero = np.where(frequencies == 0)
        idxs_nonzero = np.where(frequencies != 0)
        e0 = 8.854e-12
        su = np.zeros(len(freq),)
        # order of the parameter vectors is flipped to be sorted from
        # low frequency pole to high frequency pole
        for i in range(3, (4-self.poles)-1, -1):
            su = su + \
                self.params.de[i]/(1 +
                                   (2j*np.pi*freq*self.params.t[i]) **
                                   (1-self.params.a[i]))

        # treat DC (f=0) components
        # self.params.de is flipped to be sorted from low frequency pole to
        # high frequency pole
        perm_dc = np.sum(self.params.de[::-1][0:self.poles])+self.params.einf
        cond_dc = self.params.sig
        
        # generate complex permittivity vector
        permc = self.params.einf+ \
            su[idxs_nonzero]+self.params.sig/(2j*np.pi*freq[idxs_nonzero]*e0)
        
        # generate permittivity vector
        perm = np.zeros(len(freq),)
        perm[idxs_nonzero] = permc.real
        perm[idxs_zero] = perm_dc
        
        # generate conductivity vector
        cond = np.zeros(len(freq),)
        cond[idxs_nonzero] = -permc.imag*2*np.pi*freq[idxs_nonzero]*e0
        cond[idxs_zero] = cond_dc
        
        if isVector is False:
            cond = cond[0]
            perm = perm[0]
            freq = freq[0]
        
        return {
                self.FREQUENCIES: freq,
                self.CONDUCTIVITY: cond,
                self.PERMITTIVITY: perm
                }


class ETissue(Enum):
    """
    Names of the supported biological tissue types.
    """
    AORTA = 1
    BLADDER = 2
    BLOOD = 3
    BONE_CANCELLOUS = 4
    BONE_CORTICAL = 5
    BONE_MARROW_INFILTRATED = 6
    BONE_MARROW_NOT_INFILTRATED = 7
    BRAIN_GREY_MATTER = 8
    BRAIN_WHITE_MATTER = 9
    BREAST_FAT = 10
    CARTILAGE = 11
    CEREBELLUM = 12
    CEREBRO_SPINAL_FLUID = 13
    CERVIX = 14
    COLON = 15
    CORNEA = 16
    DURA = 17
    EYE_TISSUES_SCLERA = 18
    FAT_AVERAGE_INFILTRATED = 19
    FAT_NOT_INFILTRATED = 20
    GALL_BLADDER = 21
    GALL_BLADDER_BILE = 22
    HEART = 23
    KIDNEY = 24
    LENS_CORTEX = 25
    LENS_NUCLEUS = 26
    LIVER = 27
    LUNG_DEFLATED = 28
    LUNG_INFLATED = 29
    MUSCLE = 30
    NERVE = 31
    OVARY = 32
    SKIN_DRY = 33
    SKIN_WET = 34
    SMALL_INTESTINE = 35
    SPLEEN = 36
    STOMACH = 37
    TENDON = 38
    TESTIS = 39
    THYROID = 40
    TONGUE = 41
    TRACHEA = 42
    UTERUS = 43
    VITREOUS_HUMOR = 44


class ColeColeParams:
    """
    ColeColeParams(tissue)
    
    Parameters of the ColeCole model for the given tissue.
    
    Parameters
    ----------
    tissue : ETissue 
        name of the biological tissue

    """

    def __init__(self, tissue):
        if tissue == ETissue.AORTA:
            self.name = 'Aorta'
            self.einf = 4.000
            self.sig = 0.250
            self.de = np.array([40.00, 50, 1.00e+5, 1.00e+7])
            self.a = np.array([0.100, 0.100, 0.200, 0.000])
            self.t = np.array([8.842e-12, 3.183e-9, 159.155e-6, 1.592e-3])
        elif tissue == ETissue.BLADDER:
            self.name = 'Bladder'
            self.einf = 2.500
            self.sig = 0.200
            self.de = np.array([16.00, 400, 1.00e+5, 1.00e+7])
            self.a = np.array([0.100, 0.100, 0.200, 0.000])
            self.t = np.array([8.842e-12, 159.155e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.BLOOD:
            self.name = 'Blood'
            self.einf = 4.000
            self.sig = 0.700
            self.de = np.array([56.00, 5200, 0.00e+0, 0.00e+0])
            self.a = np.array([0.100, 0.100, 0.200, 0.000])
            self.t = np.array([8.377e-12, 132.629e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.BONE_CANCELLOUS:
            self.name = 'Bone (Cancellous)'
            self.einf = 2.500
            self.sig = 0.070
            self.de = np.array([18.00, 300, 2.00e+4, 2.00e+7])
            self.a = np.array([0.220, 0.250, 0.200, 0.000])
            self.t = np.array([13.263e-12, 79.577e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.BONE_CORTICAL:
            self.name = 'Bone (Cortical)'
            self.einf = 2.500
            self.sig = 0.020
            self.de = np.array([10.00, 180, 5.00e+3, 1.00e+5])
            self.a = np.array([0.200, 0.200, 0.200, 0.000])
            self.t = np.array([13.263e-12, 79.577e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.BONE_MARROW_INFILTRATED:
            self.name = 'Bone Marrow (Infiltrated)'
            self.einf = 2.500
            self.sig = 0.100
            self.de = np.array([9.00, 80, 1.00e+4, 2.00e+6])
            self.a = np.array([0.200, 0.100, 0.100, 0.100])
            self.t = np.array([14.469e-12, 15.915e-9, 1591.549e-6, 15.915e-3])
        elif tissue == ETissue.BONE_MARROW_NOT_INFILTRATED:
            self.name = 'Bone Marrow (Not Infiltrated)'
            self.einf = 2.500
            self.sig = 0.001
            self.de = np.array([3.00, 25, 5.00e+3, 2.00e+6])
            self.a = np.array([0.200, 0.100, 0.100, 0.100])
            self.t = np.array([7.958e-12, 15.915e-9, 1591.549e-6, 15.915e-3])
        elif tissue == ETissue.BRAIN_GREY_MATTER:
            self.name = 'Brain (Grey Matter)'
            self.einf = 4.000
            self.sig = 0.020
            self.de = np.array([45.00, 400, 2.00e+5, 4.50e+7])
            self.a = np.array([0.100, 0.150, 0.220, 0.000])
            self.t = np.array([7.958e-12, 15.915e-9, 106.103e-6, 5.305e-3])
        elif tissue == ETissue.BRAIN_WHITE_MATTER:
            self.name = 'Brain (White Matter)'
            self.einf = 4.000
            self.sig = 0.020
            self.de = np.array([32.00, 100, 4.00e+4, 3.50e+7])
            self.a = np.array([0.100, 0.100, 0.300, 0.020])
            self.t = np.array([7.958e-12, 7.958e-9, 53.052e-6, 7.958e-3])
        elif tissue == ETissue.BREAST_FAT:
            self.name = 'Breast fat'
            self.einf = 2.500
            self.sig = 0.010
            self.de = np.array([3.00, 15, 5.00e+4, 2.00e+7])
            self.a = np.array([0.100, 0.100, 0.100, 0.000])
            self.t = np.array([17.680e-12, 63.660e-9, 454.700e-6, 13.260e-3])
        elif tissue == ETissue.CARTILAGE:
            self.name = 'Cartilage'
            self.einf = 4.000
            self.sig = 0.150
            self.de = np.array([38.00, 2500, 1.00e+5, 4.00e+7])
            self.a = np.array([0.150, 0.150, 0.100, 0.000])
            self.t = np.array([13.263e-12, 144.686e-9, 318.310e-6, 15.915e-3])
        elif tissue == ETissue.CEREBELLUM:
            self.name = 'Cerebellum'
            self.einf = 4.000
            self.sig = 0.040
            self.de = np.array([40.00, 700, 2.00e+5, 4.50e+7])
            self.a = np.array([0.100, 0.150, 0.220, 0.000])
            self.t = np.array([7.958e-12, 15.915e-9, 106.103e-6, 5.305e-3])
        elif tissue == ETissue.CEREBRO_SPINAL_FLUID:
            self.name = 'Cerebro Spinal Fluid'
            self.einf = 4.000
            self.sig = 2.000
            self.de = np.array([65.00, 40, 0.00e+0, 0.00e+0])
            self.a = np.array([0.100, 0.000, 0.000, 0.000])
            self.t = np.array([7.958e-12, 1.592e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.CERVIX:
            self.name = 'Cervix'
            self.einf = 4.000
            self.sig = 0.300
            self.de = np.array([45.00, 200, 1.50e+5, 4.00e+7])
            self.a = np.array([0.100, 0.100, 0.180, 0.000])
            self.t = np.array([7.958e-12, 15.915e-9, 106.103e-6, 1.592e-3])
        elif tissue == ETissue.COLON:
            self.name = 'Colon'
            self.einf = 4.000
            self.sig = 0.010
            self.de = np.array([50.00, 3000, 1.00e+5, 4.00e+7])
            self.a = np.array([0.100, 0.200, 0.200, 0.000])
            self.t = np.array([7.958e-12, 159.155e-9, 159.155e-6, 1.592e-3])
        elif tissue == ETissue.CORNEA:
            self.name = 'Cornea'
            self.einf = 4.000
            self.sig = 0.400
            self.de = np.array([48.00, 4000, 1.00e+5, 4.00e+7])
            self.a = np.array([0.100, 0.050, 0.200, 0.000])
            self.t = np.array([7.958e-12, 159.155e-9, 15.915e-6, 15.915e-3])
        elif tissue == ETissue.DURA:
            self.name = 'Dura'
            self.einf = 4.000
            self.sig = 0.500
            self.de = np.array([40.00, 200, 1.00e+4, 1.00e+6])
            self.a = np.array([0.150, 0.100, 0.200, 0.000])
            self.t = np.array([7.958e-12, 7.958e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.EYE_TISSUES_SCLERA:
            self.name = 'Eye Tissues (Sclera)'
            self.einf = 4.000
            self.sig = 0.500
            self.de = np.array([50.00, 4000, 1.00e+5, 5.00e+6])
            self.a = np.array([0.100, 0.100, 0.200, 0.000])
            self.t = np.array([7.958e-12, 159.155e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.FAT_AVERAGE_INFILTRATED:
            self.name = 'Fat (Average Infiltrated)'
            self.einf = 2.500
            self.sig = 0.035
            self.de = np.array([9.00, 35, 3.30e+4, 1.00e+7])
            self.a = np.array([0.200, 0.100, 0.050, 0.010])
            self.t = np.array([7.958e-12, 15.915e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.FAT_NOT_INFILTRATED:
            self.name = 'Fat (Not Infiltrated)'
            self.einf = 2.500
            self.sig = 0.010
            self.de = np.array([3.00, 15, 3.30e+4, 1.00e+7])
            self.a = np.array([0.200, 0.100, 0.050, 0.010])
            self.t = np.array([7.958e-12, 15.915e-9, 159.155e-6, 7.958e-3])
        elif tissue == ETissue.GALL_BLADDER:
            self.name = 'Gall Bladder'
            self.einf = 4.000
            self.sig = 0.900
            self.de = np.array([55.00, 40, 1.00e+3, 1.00e+4])
            self.a = np.array([0.050, 0.000, 0.200, 0.000])
            self.t = np.array([7.579e-12, 1.592e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.GALL_BLADDER_BILE:
            self.name = 'Gall Bladder Bile'
            self.einf = 4.000
            self.sig = 1.400
            self.de = np.array([66.00, 50, 0.00e+0, 0.00e+0])
            self.a = np.array([0.050, 0.000, 0.200, 0.200])
            self.t = np.array([7.579e-12, 1.592e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.HEART:
            self.name = 'Heart'
            self.einf = 4.000
            self.sig = 0.050
            self.de = np.array([50.00, 1200, 4.50e+5, 2.50e+7])
            self.a = np.array([0.100, 0.050, 0.220, 0.000])
            self.t = np.array([7.958e-12, 159.155e-9, 72.343e-6, 4.547e-3])
        elif tissue == ETissue.KIDNEY:
            self.name = 'Kidney'
            self.einf = 4.000
            self.sig = 0.050
            self.de = np.array([47.00, 3500, 2.50e+5, 3.00e+7])
            self.a = np.array([0.100, 0.220, 0.220, 0.000])
            self.t = np.array([7.958e-12, 198.944e-9, 79.577e-6, 4.547e-3])
        elif tissue == ETissue.LENS_CORTEX:
            self.name = 'Lens Cortex'
            self.einf = 4.000
            self.sig = 0.300
            self.de = np.array([42.00, 1500, 2.00e+5, 4.00e+7])
            self.a = np.array([0.100, 0.100, 0.100, 0.000])
            self.t = np.array([7.958e-12, 79.577e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.LENS_NUCLEUS:
            self.name = 'Lens Nucleus'
            self.einf = 3.000
            self.sig = 0.200
            self.de = np.array([32.00, 100, 1.00e+3, 5.00e+3])
            self.a = np.array([0.100, 0.200, 0.200, 0.000])
            self.t = np.array([8.842e-12, 10.610e-9, 15.915e-6, 15.915e-3])
        elif tissue == ETissue.LIVER:
            self.name = 'Liver'
            self.einf = 4.000
            self.sig = 0.020
            self.de = np.array([39.00, 6000, 5.00e+4, 3.00e+7])
            self.a = np.array([0.100, 0.200, 0.200, 0.050])
            self.t = np.array([8.842e-12, 530.516e-9, 22.736e-6, 15.915e-3])
        elif tissue == ETissue.LUNG_DEFLATED:
            self.name = 'Lung (Deflated)'
            self.einf = 4.000
            self.sig = 0.200
            self.de = np.array([45.00, 1000, 5.00e+5, 1.00e+7])
            self.a = np.array([0.100, 0.100, 0.200, 0.000])
            self.t = np.array([7.958e-12, 159.155e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.LUNG_INFLATED:
            self.name = 'Lung (Inflated)'
            self.einf = 2.500
            self.sig = 0.030
            self.de = np.array([18.00, 500, 2.50e+5, 4.00e+7])
            self.a = np.array([0.100, 0.100, 0.200, 0.000])
            self.t = np.array([7.958e-12, 63.662e-9, 159.155e-6, 7.958e-3])
        elif tissue == ETissue.MUSCLE:
            self.name = 'Muscle'
            self.einf = 4.000
            self.sig = 0.200
            self.de = np.array([50.00, 7000, 1.20e+6, 2.50e+7])
            self.a = np.array([0.100, 0.100, 0.100, 0.000])
            self.t = np.array([7.234e-12, 353.678e-9, 318.310e-6, 2.274e-3])
        elif tissue == ETissue.NERVE:
            self.name = 'Nerve'
            self.einf = 4.000
            self.sig = 0.006
            self.de = np.array([26.00, 500, 7.00e+4, 4.00e+7])
            self.a = np.array([0.100, 0.150, 0.200, 0.000])
            self.t = np.array([7.958e-12, 106.103e-9, 15.915e-6, 15.915e-3])
        elif tissue == ETissue.OVARY:
            self.name = 'Ovary'
            self.einf = 4.000
            self.sig = 0.300
            self.de = np.array([40.00, 400, 1.00e+5, 4.00e+7])
            self.a = np.array([0.150, 0.250, 0.270, 0.000])
            self.t = np.array([8.842e-12, 15.915e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.SKIN_DRY:
            self.name = 'Skin (Dry)'
            self.einf = 4.000
            self.sig = 0.000
            self.de = np.array([32.00, 1100, 0.00e+0, 0.00e+0])
            self.a = np.array([0.000, 0.200, 0.200, 0.200])
            self.t = np.array([7.234e-12, 32.481e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.SKIN_WET:
            self.name = 'Skin (Wet)'
            self.einf = 4.000
            self.sig = 0.000
            self.de = np.array([39.00, 280, 3.00e+4, 3.00e+4])
            self.a = np.array([0.100, 0.000, 0.160, 0.200])
            self.t = np.array([7.958e-12, 79.577e-9, 1.592e-6, 1.592e-3])
        elif tissue == ETissue.SMALL_INTESTINE:
            self.name = 'Small Intestine'
            self.einf = 4.000
            self.sig = 0.500
            self.de = np.array([50.00, 10000, 5.00e+5, 4.00e+7])
            self.a = np.array([0.100, 0.100, 0.200, 0.000])
            self.t = np.array([7.958e-12, 159.155e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.SPLEEN:
            self.name = 'Spleen'
            self.einf = 4.000
            self.sig = 0.030
            self.de = np.array([48.00, 2500, 2.00e+5, 5.00e+7])
            self.a = np.array([0.100, 0.150, 0.250, 0.000])
            self.t = np.array([7.958e-12, 63.662e-9, 265.258e-6, 6.366e-3])
        elif tissue == ETissue.STOMACH:
            self.name = 'Stomach'
            self.einf = 4.000
            self.sig = 0.500
            self.de = np.array([60.00, 2000, 1.00e+5, 4.00e+7])
            self.a = np.array([0.100, 0.100, 0.200, 0.000])
            self.t = np.array([7.958e-12, 79.577e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.TENDON:
            self.name = 'Tendon'
            self.einf = 4.000
            self.sig = 0.250
            self.de = np.array([42.00, 60, 6.00e+4, 2.00e+7])
            self.a = np.array([0.100, 0.100, 0.220, 0.000])
            self.t = np.array([12.243e-12, 6.366e-9, 318.310e-6, 1.326e-3])
        elif tissue == ETissue.TESTIS:
            self.name = 'Testis'
            self.einf = 4.000
            self.sig = 0.400
            self.de = np.array([55.00, 5000, 1.00e+5, 4.00e+7])
            self.a = np.array([0.100, 0.100, 0.200, 0.000])
            self.t = np.array([7.958e-12, 159.155e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.THYROID:
            self.name = 'Thyroid'
            self.einf = 4.000
            self.sig = 0.500
            self.de = np.array([55.00, 2500, 1.00e+5, 4.00e+7])
            self.a = np.array([0.100, 0.100, 0.200, 0.000])
            self.t = np.array([7.958e-12, 159.155e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.TONGUE:
            self.name = 'Tongue'
            self.einf = 4.000
            self.sig = 0.250
            self.de = np.array([50.00, 4000, 1.00e+5, 4.00e+7])
            self.a = np.array([0.100, 0.100, 0.200, 0.000])
            self.t = np.array([7.958e-12, 159.155e-9, 159.155e-6, 15.915e-3])
        elif tissue == ETissue.TRACHEA:
            self.name = 'Trachea'
            self.einf = 2.500
            self.sig = 0.300
            self.de = np.array([38.00, 400, 5.00e+4, 1.00e+6])
            self.a = np.array([0.100, 0.100, 0.200, 0.000])
            self.t = np.array([7.958e-12, 63.662e-9, 15.915e-6, 15.915e-3])
        elif tissue == ETissue.UTERUS:
            self.name = 'Uterus'
            self.einf = 4.000
            self.sig = 0.200
            self.de = np.array([55.00, 800, 3.00e+5, 3.50e+7])
            self.a = np.array([0.100, 0.100, 0.200, 0.000])
            self.t = np.array([7.958e-12, 31.831e-9, 159.155e-6, 1.061e-3])
        elif tissue == ETissue.VITREOUS_HUMOR:
            self.name = 'Vitreous Humor'
            self.einf = 4.000
            self.sig = 1.500
            self.de = np.array([65.00, 30, 0.00e+0, 0.00e+0])
            self.a = np.array([0.000, 0.100, 0.000, 0.000])
            self.t = np.array([7.234e-12, 159.155e-9, 159.155e-6, 15.915e-3])
        else:
            raise NotImplementedError('Unknown tissue type ', tissue)