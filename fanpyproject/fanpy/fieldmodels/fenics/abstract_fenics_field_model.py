'''
Created on Mar 1, 2017

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPy
'''

from fenics import *  #@UnusedWildImport
import numpy as np
import inspect
import os
import xml.etree.ElementTree as ET
import h5py
import time as pytime

class AbstractFenicsFieldModel():
    """
    AbstractFenicsFieldModel is the general class for importing, defining, solving,
    and post-processing a model geometry and definition using FEnICS-
    
    The class loads the model defined in a customized XML file.
    
    The following options should be set prior to performing init():
    
    self.MODELNAME = 'Example_Model'
        to set name of the xml file describing the model.
        
    self.model_path = './testpath'
        to set the path to the model xml file
    
    self.set_basis_order(1)
        to set the basis order (here linear order)
    
    self.set_debug(True)
        to set debug mode (coarser model, here True)
    
    self.set_quasistatic(False)
        to trigger quasistatic field equation (here False, means stationary 
        current field equation is used)
    
      
    The default model pipeline:
    
    init()
        initializes the model
    
    run(dict())
        run the model. Default model parameters are used when an empty dictionary
        is given. 
    
    evaluate_quantity(quantity, coords)
        evaluate a field solution quantity (such as electric potential or electric
        field norm) for a numpy array of shape (n,3) given by coords.
    
    """
    
    def __init__(self):
        self.MODELNAME = None
        self._basis_order = 1
        self._debug = False
        self._quasistatic = False
        self._runtime = 0
        """ get the path to the model_data folder """ 
        class_name = self.__class__.__module__
        class_file = inspect.getfile(self.__class__)
        class_file = class_file.replace(".pyc","")
        class_file = class_file.replace(".py","")
        model_path = class_file[0:len(class_file)- \
                                len(class_name)+ \
                                class_name.rfind('.')]+('\\model_data\\')
        self.model_path = model_path.replace('\\','/')
    
    def init(self):
        """
        Initialize the model. The following steps are performed:
        
        1) Checking if the model xml file is defined, existing, and if the given 
           information on the model parameters are valid (boundary conditions
           matching boundary information, material properties matching sub 
           domain definition, etc.
           
        2) Checking if the mesh is already created and if not creating the mesh
           using gmsh and dolfin-convert
           
        3) Importing the mesh and creating the FEniCS mesh, boundaries, subdomains,
           and FunctionSpace objects.
    
        """
        # is model name set?
        if self.MODELNAME is None:
            raise Exception("The model has first to be defined in self.MODELNAME")
        
        self.tmpdata_path = 'tmpdata/'+self.MODELNAME+'/'
        # ensure that xml file exists
        if not os.path.isfile(self.model_path+self.MODELNAME+'.xml'):
            raise Exception('Model Description '+self.MODELNAME+'.xml not found')
        
        # parse xml file
        modelxml = ET.parse(self.model_path+self.MODELNAME+'.xml')
        fmodel = modelxml.getroot()
        if fmodel.tag != 'fmodel':
            raise Exception(self.MODELNAME+": Could not find 'fmodel' root element in xml.")
        print 'debug mode: ',self._debug
        if self._debug:
            geofile = fmodel.findall('geofile_debug')
        else:
            geofile = fmodel.findall('geofile')
        if len(geofile)!=1:
            raise Exception(self.MODELNAME+": Requiring exactly one 'geofile' element in xml.")
        geofile = geofile[0].text
        
        xmlsubdomains = self.__get_xml_group(fmodel, 'subdomains', 'subdomain',
                                             ['name', 'material', 'physid'])
        xmlboundaries = self.__get_xml_group(fmodel, 'boundaries', 'boundary',
                                             ['name', 'physid'])
        xmlmaterials = self.__get_xml_group(fmodel, 'materials', 'material',
                                            ['name', 'conductivity'])
        xmlboundaryconditions = self.__get_xml_group(fmodel, 'boundary_conditions',
                                                     'boundary_condition',
                                                     ['name', 'type', 'boundary','value'])
        
        # verify that each subdomain has a defined material
        for subdomain in xmlsubdomains.values():
            if not xmlmaterials.has_key(subdomain['material']):
                raise Exception(self.MODELNAME+": Subdomain '"+subdomain['name']
                                +"' has an undefined material '"
                                +subdomain['material']+"'")
                
        # verify that boundary conditions have a defined boundary
        for bc in xmlboundaryconditions.values():
            if not xmlboundaries.has_key(bc['boundary']):
                raise Exception(self.MODELNAME+": Boundary Condition '"
                                +bc['name']+"' has an undefined boundary '"
                                +bc['boundary']+"'") 
                                
            
        # a) first ensure that geo file exists
        path_geofile = self.model_path+geofile       
        if not os.path.isfile(path_geofile+'.geo'):
            raise Exception('Geo Model '+path_geofile+'.geo not found')
        
        # check if number of boundaries and subdomains match with physical groups
        # in the geo file.
        geofile_content = open(path_geofile+'.geo').read()
        if geofile_content.count('Physical Volume') != len(xmlsubdomains):
            raise Exception('Number of subdomains in xml file does not match number of physical volumes in geo file '+str(path_geofile)+'.')
        for subdomain in xmlsubdomains.values():
            if not 'Physical Volume('+subdomain['physid']+')' in geofile_content:
                raise Exception("Couldn't find matching physical volume in geo file "+str(path_geofile)+" for subdomain "+str(subdomain)+" in xml.")
        # we do not require to find all boundaries in the xml file, which are in the geo file.
        # But we require that the boundaries in the xml file are defined as physical surfaces in the geo file.
        for boundary in xmlboundaries.values():
            if not 'Physical Surface('+boundary['physid']+')' in geofile_content:
                raise Exception("Couldn't find matching physical surface in geo file "+str(path_geofile)+" for boundary "+str(boundary)+" in xml.")
        
        # b) then make mesh with gmsh
        if not os.path.isdir(self.model_path+self.tmpdata_path):
            os.makedirs(self.model_path+self.tmpdata_path)
        
        # if already exist, skip
        path_msh_file = self.model_path+self.tmpdata_path+geofile+'_mesh'
        if not os.path.isfile(path_msh_file+'.msh'):
            os.system('gmsh -3 '+path_geofile+'.geo')
            # c) then ensure that msh was created
            if not os.path.isfile(path_geofile+'.msh'):
                raise Exception(self.MODELNAME+': Mesh was not created by gmsh. Check that gmsh is not installed or not in system path.')
            os.rename(path_geofile+'.msh', path_msh_file+'.msh')
        
        # d) then convert with dolfin-convert
        # if already exist, skip
        if (not os.path.isfile(path_msh_file+'.xml.gz') or 
            not os.path.isfile(path_msh_file+'_facet_region.xml.gz') or
            not os.path.isfile(path_msh_file+'_physical_region.xml.gz')):
            os.system('dolfin-convert '+path_msh_file+'.msh '+path_msh_file+'.xml')
            # e) then ensure that xml mesh files were created
            if (not os.path.isfile(path_msh_file+'.xml') or 
                not os.path.isfile(path_msh_file+'_facet_region.xml') or
                not os.path.isfile(path_msh_file+'_physical_region.xml')):
                raise Exception(self.MODELNAME+': Mesh was not properly converted by dolfin.')
        
            # f) then gzip them 
            os.system('gzip -f '+path_msh_file+'.xml')
            os.system('gzip -f '+path_msh_file+'_facet_region.xml')
            os.system('gzip -f '+path_msh_file+'_physical_region.xml')
            
            if (not os.path.isfile(path_msh_file+'.xml.gz') or 
                not os.path.isfile(path_msh_file+'_facet_region.xml.gz') or
                not os.path.isfile(path_msh_file+'_physical_region.xml.gz')):
                raise Exception(self.MODELNAME+': Could not gzip the mesh.')
            
        self._mesh = Mesh(path_msh_file+".xml.gz")
        self._subdomains = MeshFunction('size_t',self._mesh,
                                         path_msh_file+"_physical_region.xml.gz")
        self._xmlsubdomains = xmlsubdomains
        boundaries = MeshFunction('size_t',self._mesh,
                                         path_msh_file+"_facet_region.xml.gz")
        self._boundaries = boundaries
        self._xmlboundaries = xmlboundaries
        self._xmlmaterials = xmlmaterials
        self._xmlboundaryconditions = xmlboundaryconditions
        self._frequency = None
        frequency = 0
        if self._quasistatic:
            self._xmlfrequency = self.__get_xml_element(fmodel, 'frequency', ['value'])
            frequency = float(self._xmlfrequency['value'])
            # recheck that materials contain permittivity
            self._xmlmaterials = self.__get_xml_group(fmodel, 'materials', 'material',
                                            ['name', 'conductivity', 'permittivity'])
        
        self.__set_function_space_and_default_boundary_conditions(frequency)  
        print(self._mesh)
        print(self._boundaries)
        self.__print_entries('Subdomains', self._xmlsubdomains)
        self.__print_entries('Boundaries', self._xmlboundaries)
        self.__print_entries('Boundary Conditions', self._xmlboundaryconditions)        
        self.__print_entries('Materials with default values',self._xmlmaterials)
        if self._quasistatic:
            self.__print_entries('Frequency', self._xmlfrequency)
        
    def __set_function_space_and_default_boundary_conditions(self, frequency):
        if not (self._frequency is None or (
            (self._frequency == 0 and frequency != 0) or
            (self._frequency != 0 and frequency == 0))):
            return
        
        if frequency is None:
            self._frequency = 0
        else:
            self._frequency = frequency
            
        if self._frequency == 0:
            V = FunctionSpace(self._mesh, 'Lagrange', self._basis_order)    
        else:
            Er = FiniteElement("Lagrange", self._mesh.ufl_cell(), self._basis_order)
            Ei = FiniteElement("Lagrange", self._mesh.ufl_cell(), self._basis_order)
            Ec = Er * Ei
            V = FunctionSpace(self._mesh, Ec)
            self._Vr = FunctionSpace(self._mesh, Er) # required for electric field computation
        self._V = V
        
        # Set boundary conditions defined in the xml
        bcs = []
        if self._frequency == 0:
            for xmlbc in self._xmlboundaryconditions.values():
                if xmlbc['type']=='dirichlet':
                    physid = int(self._xmlboundaries[xmlbc['boundary']]['physid'])
                    bc = DirichletBC(V, Constant(float(xmlbc['value'])), self._boundaries, physid)
                    bcs.append(bc)
                else:
                    raise Exception('Unsupported boundary condition type in '+str(xmlbc))
        else:
            for xmlbc in self._xmlboundaryconditions.values():
                if xmlbc['type']=='dirichlet':
                    physid = int(self._xmlboundaries[xmlbc['boundary']]['physid'])
                    bc_r = DirichletBC(V.sub(0), Constant(float(xmlbc['value'])), self._boundaries, physid)
                    bcs.append(bc_r)
                    bc_i = DirichletBC(V.sub(1), Constant(0.0), self._boundaries, physid)
                    bcs.append(bc_i)
                else:
                    raise Exception('Unsupported boundary condition type in '+str(xmlbc))
        self._bcs = bcs
        
    def __get_xml_element(self, xmlstring, element_name, required_attributes):
        felement = xmlstring.findall(element_name)
        if felement is None:
            raise Exception(self.MODELNAME+': Missing '+element_name+' in xml.')
        attr = felement[0].attrib
        for required_attr in required_attributes:
            if required_attr not in attr.keys():
                raise Exception(self.MODELNAME+": Requiring attributes '"+str(required_attributes)
                               +"' in "+element_name+" elements in xml.") 
        return attr
        
    
    def __get_xml_group(self, xmlstring, group_name, element_name, required_attributes):
        xmlgroup = dict()
        fgroup = xmlstring.findall(group_name)
        if len(fgroup)!=1:
            raise Exception(self.MODELNAME+": Expecting exactly one "+group_name+" element in xml.") 
        for elements in fgroup[0].findall(element_name):
            attr = elements.attrib
            for required_attr in required_attributes:
                if required_attr not in attr.keys():
                    raise Exception(self.MODELNAME+": Requiring attributes "+str(required_attributes)
                                   +" in "+element_name+" elements in xml.") 
            xmlgroup[attr['name']]=attr
        
        return xmlgroup
    
    def __print_entries(self, name, dictionary):
        print name
        for k, v in dictionary.items():
            print k,':',v
        print ''
        
    def set_basis_order(self, basis_order):
        """
        Set the basis order for the ansatz functions used.
    
        Parameters
        ----------
        basis_order : int 
            Basis order for the ansatz functions.
      
        """
        self._basis_order = basis_order
        
    def get_basis_order(self):
        """
        Get the parameter for the basis order of the ansatz functions.
    
        Returns
        -------
        int
            basis order
    
        """
        return self._basis_order
    
    def set_quasistatic(self, quasistatic):
        """
        If set to True, quasistatic field equation is used.
        If set t False, stationary current field equation is used.
    
        Parameters
        ----------
        quasistatic : boolean 
            field equation trigger
       
        """
        self._quasistatic = quasistatic
        
    def get_quasistatic(self):
        """
        If set to True, quasistatic field equation is used.
        If set t False, stationary current field equation is used.
    
        Returns
        -------
        boolean
            field equation trigger
    
        """
        return self._quasistatic
        
    def get_mesh(self):
        """
        Returns
        -------
        Mesh
            FEniCS mesh object
    
        """
        return self._mesh
    
    def get_functionspace(self):
        """  
        Returns
        -------
        FunctionSpace
            FEniCS function space object
    
        """
        return self._V

    def run(self, modelparams):
        """
        Run the FEniCS model.
    
        Parameters
        ----------
        modelparams : dictionary 
            Contains the model parameters for the model. If empty dictionary is
            given, the default model parameters are used.
            For more information on model parameters see documentatino of
            prepare_modelparams(modelparams).
            
        A model run performs the following steps:
        
        1) __set_function_space_and_default_boundary_conditions(frequency)
            (private) sets function space and boundary conditions
            
        1) prepare_modelparams(modelparams)
            checks the modelparams for validity and sets them
        
        2) create_kappa()
            creates the material properties on the given mesh cells.
            
        3) __solve()
            (private) solves the model using default iterative solver and
            preconditioner.
        
        4) compute_derived_quantities()
            computes the derived quantities from the solution, such as
            the electric field norm and the current density norm.
       
        """
        start = pytime.time()
        # set function space and boundaries if necessary
        if self._quasistatic:
                if 'frequency' in modelparams:
                    frequency = modelparams['frequency']
                else:
                    frequency = self._frequency
        else:
            frequency = 0
        
        self.__set_function_space_and_default_boundary_conditions(frequency)
        # resolve and prepare model parameters
        self.prepare_modelparams(modelparams)
        
        # create material properties function
        print 'Running model for ',modelparams['dielectric_properties']
        self.create_kappa()
        
        # Solve the problem
        self.__solve()
                  
        # derive quantities from solution
        self.compute_derived_quantities()
        
        end = pytime.time()
        self._runtime = end-start
    
    def __solve(self):
        f = Constant(0.0)
        if self._frequency == 0:
            u = TrialFunction(self._V)
            v = TestFunction(self._V)
            F = (inner(grad(self._kappa*u), grad(v))*dx-f*v*dx)
            a, L = lhs(F), rhs(F)  
        else:
            u_r, u_i = TrialFunction(self._V)
            v_r, v_i = TestFunction(self._V)
            a = (inner(grad(self._kappa[0]*u_r), grad(v_r))*dx
             -inner(grad(self._kappa[1]*u_i), grad(v_r))*dx
             -inner(grad(self._kappa[1]*u_r), grad(v_i))*dx
             -inner(grad(self._kappa[0]*u_i), grad(v_i))*dx
             +inner(grad(self._kappa[0]*u_r), grad(v_i))*dx
             -inner(grad(self._kappa[1]*u_i), grad(v_i))*dx
             +inner(grad(self._kappa[1]*u_r), grad(v_r))*dx
             +inner(grad(self._kappa[0]*u_i), grad(v_r))*dx
             )
            
            L = -(f*v_r+f*v_i)*dx
  
        u = Function(self._V)
              
        # solving the problem
        # solve iterative
        problem = LinearVariationalProblem(a, L, u, self._bcs)
        solver = LinearVariationalSolver(problem)
        #solver.parameters.linear_solver = 'cg'
        solver.parameters.linear_solver = 'gmres'
        solver.parameters.preconditioner = 'ilu'
        if not self._quasistatic:
            # don't use algebraic multigrid preconditioner for quasistatic case
            # it is not converging in that case. maybe issue with
            # preconditioners and complex numbers?
            solver.parameters.preconditioner = 'hypre_amg'
        prm = solver.parameters.krylov_solver
        prm.monitor_convergence = True
        prm.absolute_tolerance=1e-7
        prm.relative_tolerance=1e-6
        
        solver.solve()
        # set solution
        self._u = u
    
    def compute_derived_quantities(self):
        """
        Computes the derived quantities, including field quantities, such as
        electric potential, electric field norm, and current density norm, as well
        as integral quantities, such as the total power.    
        """
        print self.MODELNAME+': Computing derived quantities'
        start = pytime.time()
        self.set_total_power(self.get_kappa(), self._u)
        self.set_potential(self._u)
        self.set_electric_field_norm(self.get_potential(), self._V)
        self.set_current_density_norm(self.get_kappa(), self.get_potential(), self._V)
        print self.MODELNAME+': Derived quantities computed ('+str(pytime.time()-start)+' s)'
        
    def prepare_modelparams(self, modelparams):
        """
        Checks the given modelparameters for validity and sets them.
    
        Parameters
        ----------
        modelparams : dictionary 
            'dielectric_properties': dictionary including the values for the
                                     dielectric properties
                'name': dictionary for the material with 'name'
                    'conductivity': float
                        conductivity value of the material
                    'permittivity': float (only for quasistatic field equation)
                        relative permittivity of the material    
        """
        if not 'dielectric_properties' in modelparams:
            modelparams['dielectric_properties'] = dict()
        for xmlmaterial in self._xmlmaterials.values():
            name = xmlmaterial['name']
            if not name in modelparams['dielectric_properties']:
                modelparams['dielectric_properties'][name] = dict()
            if not 'conductivity' in modelparams['dielectric_properties'][name]:
                value = np.float(xmlmaterial['conductivity'])
                modelparams['dielectric_properties'][name]['conductivity'] = value
                print self.MODELNAME+": Couldn't find material "+name+" in model parameters. Using default conductivity "+str(value)+" S/m"
            if self._frequency != 0 and not 'permittivity' in modelparams['dielectric_properties'][name]:
                value = np.float(xmlmaterial['permittivity'])
                modelparams['dielectric_properties'][name]['permittivity'] = value
                print self.MODELNAME+": Couldn't find material "+name+" in model parameters. Using default permittivity "+str(value)
        self._modelparams = modelparams   
        
    def create_kappa(self):
        """
        Creates the material properties object (vector) based on the given
        model parameters and mesh.
    
        """
        print self.MODELNAME+': Setting material properties'
        start = pytime.time()
        W = FunctionSpace(self._mesh, "DG", 0)
        kappa = Function(W)
        
        xmlsubdomain_physid_mapper = {}
        
        k_values=np.zeros(len(self._xmlsubdomains),)
        for idx, xmlsubdomain in enumerate(self._xmlsubdomains.values()):
            xmlsubdomain_physid_mapper[int(xmlsubdomain['physid'])] = idx
            mat_name = self._xmlmaterials[xmlsubdomain['material']]['name']
            k_values[idx] = self._modelparams['dielectric_properties'][mat_name]['conductivity']
          
        dm = W.dofmap()
        helper = np.asarray(self._subdomains.array(),dtype=np.int32)
        for cell in cells(self._mesh):
            subdomain = self._subdomains[cell]
            mapped_cell = xmlsubdomain_physid_mapper[subdomain]
            helper[dm.cell_dofs(cell.index())] = mapped_cell
        kappa.vector()[:] = np.choose(helper,k_values)
        if self._frequency != 0:
            W_i = FunctionSpace(self._mesh, "DG", 0)
            kappa_i = Function(W_i)
            
            xmlsubdomain_physid_mapper_i = {}
            
            k_values_i=np.zeros(len(self._xmlsubdomains),)
            for idx, xmlsubdomain in enumerate(self._xmlsubdomains.values()):
                xmlsubdomain_physid_mapper_i[int(xmlsubdomain['physid'])] = idx
                mat_name = self._xmlmaterials[xmlsubdomain['material']]['name']
                k_values_i[idx] = (2*np.pi*self._frequency*8.854e-12*
                                   self._modelparams['dielectric_properties'][mat_name]['permittivity'])
              
            dm_i = W_i.dofmap()
            helper_i = np.asarray(self._subdomains.array(),dtype=np.int32)
            for cell in cells(self._mesh):
                subdomain = self._subdomains[cell]
                mapped_cell = xmlsubdomain_physid_mapper_i[subdomain]
                helper_i[dm_i.cell_dofs(cell.index())] = mapped_cell
            kappa_i.vector()[:] = np.choose(helper_i,k_values_i)
            kappa = [kappa, kappa_i]            
            
        self._kappa = kappa
        print self.MODELNAME+': Material properties set up done ('+str(pytime.time()-start)+' s)'
    
    def get_kappa(self):
        """
    
        Returns
        -------
        Function
            FEniCS function object of the material properties for the given mesh
            and model parameters.
    
        """
        return self._kappa
    
    def get_potential(self):
        """
    
        Returns
        -------
        Function
            FEniCS function containing the electric potential determined from 
            the solution
    
        """
        return self._pot
    
    def set_potential(self, pot):
        """
        Set the electric potential.
    
        Parameters
        ----------
        pot : Function 
            FEniCS function object of the electric potential determined from the
            solution. The Function has to match the given model and its mesh.
    
        """
        self._pot = pot 
        
    def get_electric_field_norm(self):
        """
    
        Returns
        -------
        Function
            FEniCS function containing the electric field norm determined from 
            the solution
    
        """
        return self._normE
    
    def set_electric_field_norm(self, u, V):
        """
        Set the electric field norm. This method computes the electric field
        norm by projecting the gradient of the solution 'u' on the model's
        function space 'V'. 
    
        Parameters
        ----------
        u : Function 
            FEniCS function object of the solution
        
        V : FunctionSpace
            FEniCS function space object of the corresponding function space to
            the solution function.
    
        """
        start = pytime.time()
        if self._frequency == 0:
            normE = project(sqrt(inner(grad(u),grad(u))), V,
                    solver_type="gmres",
                    preconditioner_type="hypre_amg")
        else:
            normEr = project(sqrt(inner(grad(u.sub(0)), grad(u.sub(0)))
                    +inner(grad(u.sub(1)), grad(u.sub(1)))),
                             self._Vr,
                             solver_type="gmres") 
            normE = Function(V)
            assign(normE.sub(0), normEr)
        self._normE = normE
        print self.MODELNAME+': Electric field norm computed ('+str(pytime.time()-start)+' s)'
        
    def get_current_density_norm(self):
        """
    
        Returns
        -------
        Function
            FEniCS function containing the current density norm determined from
            the solution
    
        """
        return self._normJ
    
    def set_current_density_norm(self, kappa, u, V):
        """
        Set the current density norm. This method computes the current density
        norm by projecting the gradient of the solution 'u' times the material
        properties function 'kappa' on the model's function space 'V'. 
    
        Parameters
        ----------
        kappa : Function
            FEniCS function object of the material properties.
        
        u : Function 
            FEniCS function object of the solution
        
        V : FunctionSpace
            FEniCS function space object of the corresponding function space to
            the solution function.
    
        """
        start = pytime.time()
        if self._frequency == 0:
            normJ = project(kappa*sqrt(inner(grad(u),grad(u))), V,
                    solver_type="gmres",
                    preconditioner_type="hypre_amg")
        else:
            kappan = sqrt(inner(kappa[0], kappa[0])+inner(kappa[1], kappa[1]))
            normJr = project(kappan*sqrt(inner(grad(u.sub(0)), grad(u.sub(0)))
                                         +inner(grad(u.sub(1)), grad(u.sub(1)))),
                             self._Vr,
                             solver_type="gmres")
            normJ = Function(V)
            assign(normJ.sub(0), normJr)
        self._normJ = normJ
        print self.MODELNAME+': Current density norm computed ('+str(pytime.time()-start)+' s)'
        
    def export_quantity(self, quantity, filepath):
        """
        Export a field quantity for visualization with paraview to a pvd file.
        In case of a quasistatic field equation the real and imaginary part are
        exported into separate files. 
    
        Parameters
        ----------
        quantity : Function 
            FEniCS function object of the quantity
        
        filepath : String
            path and name of the file.
    
        """
        if self._frequency == 0:
            filepath = filepath.replace('.pvd','')
            vtkfile = File(filepath+'.pvd')
            vtkfile << quantity
        else:
            filepath = filepath.replace('.pvd','')
            vtkfile = File(filepath+'_real.pvd')
            vtkfile << quantity.sub(0)
            filepath = filepath.replace('.pvd','')
            vtkfile = File(filepath+'_imag.pvd')
            vtkfile << quantity.sub(1)
        
    def evaluate_quantity(self, quantity, points):
        """
        Evaluate a field quantity at given points in the model domain.
    
        Parameters
        ----------
        quantity : Function 
            FEniCS function object of the field quantity
        
        points : numpy.array
            Numpy array of shape (n,3) of coordinates in the model domain at
            which the field quantity should be evaluated.
    
        """
        if len(points.shape) == 1 and points.shape[0] == 3:
            return quantity(points)
        elif points.shape[1] != 3:
            raise ('Points should be in format (n,3)')
            
        n = points.shape[0]
        if self._frequency == 0:
            solution = np.zeros(n,)
            for i in range(0, n):
                solution[i] = quantity(points[i,:])
        else:
            solution = np.zeros((n,), dtype=np.complex)
            for i in range(0, n):
                solution[i] = np.complex(quantity.sub(0)(points[i,:]), quantity.sub(1)(points[i,:]))
        return solution
    
    def set_total_power(self, kappa, u):
        """
        Set the total power of the model. This method computes the total power
        of the model by integrating the inner product of the current density
        norm and the electric field norm over the whole model domain.
    
        Parameters
        ----------
        kappa : Function
            FEniCs function object of the material properties
            
        u : Function 
            FEniCS function object of the solution
    
        """
        if self._frequency == 0:
            power_density = dot(kappa*grad(u),grad(u))*dx
            self._total_power = assemble(power_density)
        else:
            total_power_r = (assemble(inner(kappa[0]*grad(u.sub(0)), grad(u.sub(0)))*dx)
                 -assemble(inner(kappa[1]*grad(u.sub(1)), grad(u.sub(0)))*dx)
                 -assemble(inner(kappa[0]*grad(u.sub(1)), grad(u.sub(1)))*dx)
                 -assemble(inner(kappa[1]*grad(u.sub(0)), grad(u.sub(1)))*dx)
                 )
 
            total_power_i = (assemble(inner(kappa[0]*grad(u.sub(1)), grad(u.sub(0)))*dx)
                 +assemble(inner(kappa[1]*grad(u.sub(0)), grad(u.sub(1)))*dx)
                 +assemble(inner(kappa[0]*grad(u.sub(0)), grad(u.sub(1)))*dx)
                 -assemble(inner(kappa[1]*grad(u.sub(1)), grad(u.sub(1)))*dx)
                 )
                 
            self._total_power = np.complex(total_power_r, total_power_i) 

    def get_total_power(self):
        """
    
        Returns
        -------
        numpy float or numpy complex
            total power of the model. In case of quasistatic field equation
            a complex value is returned.
    
        """
        return self._total_power
    
    def get_impedance(self):
        """
    
        Returns
        -------
        numpy float or numpy complex
            impedance of the model. In case of quasistatic field equation
            a complex value is returned.
    
        """
        # Assuming unit voltage on a monopolar active contact
        return 1/self.get_total_power()
    
    def store_model_state(self, path, filename):
        """
        Stores the model state including the solution, which implicates that the
        model have been run before. The method stores the meta information on the
        model, such as model parameters, basis order, quasistatic flag, ...
        in the file given by the filename. The solution, mesh, and boundary and
        subdomain information are stored in a datafile with the filename
        extended by '_data'.
    
        Parameters
        ----------
        path : String
            path to the model state file
            
        filename : String 
            filename of the model state file
    
        """
        # store meta information
        filename = filename.replace('.hdf5','')
        if not path.endswith('/'):
            path = path + '/'
        if not os.path.exists(path):
            os.mkdir(path)
        h5f = h5py.File(path+filename+'.hdf5', 'w')
        self.__store_dictionary(h5f, 'xmlsubdomains', self._xmlsubdomains)
        self.__store_dictionary(h5f, 'xmlboundaries', self._xmlboundaries)
        self.__store_dictionary(h5f, 'xmlmaterials', self._xmlmaterials)
        self.__store_dictionary(h5f, 'xmlbcs', self._xmlboundaryconditions)
        if self._quasistatic:
            self.__store_dictionary(h5f, 'xmlfrequency', self._xmlfrequency)
        self.__store_dictionary(h5f, 'modelparams', self._modelparams)
        h5f.create_dataset('h5datafile', data=filename+'_data.hdf5')
        h5f.create_dataset('modelpath', data=self.model_path)
        h5f.create_dataset('modelname', data=self.MODELNAME)
        h5f.create_dataset('basis_order', data=self._basis_order)
        h5f.create_dataset('quasistatic', data=self._quasistatic)
        h5f.create_dataset('runtime', data=self._runtime)
        h5f.close()
        
        # store model data
        hdf5file = HDF5File(self._mesh.mpi_comm(),path+"/"+filename+'_data.hdf5','w')
        hdf5file.write(self._mesh,"mesh")
        hdf5file.write(self._subdomains,"subdomains")
        hdf5file.write(self._boundaries,"boundaries")
        hdf5file.write(self._u,"u")
        hdf5file.close()
        
    def __store_dictionary(self, h5f, name, xml_group):
        for key, element in xml_group.items():
            if type(element) == dict().__class__:
                self.__store_dictionary(h5f, name+"/"+key, element)
            else:
                h5f.create_dataset(name+'/'+key, data=element)
    
    def load_model_state(self, path, filename):
        """
        Loads a model state, which was saved by the method 
        store_model_state(path, filename), including the solution. The method 
        loads the meta information on the model, such as model parameters, 
        basis order, quasistatic flag, ... in the file given by the filename.
        The solution, mesh, and boundary and subdomain information is loaded
        from a datafile with the filename extended by '_data'. The material
        properties and the derived quantities are determined.
        The model is then in a state as if it just have been run and can be
        treated like a model in this state (rerun, storing solution, ...).
    
        Parameters
        ----------
        path : String
            path to the model state file
            
        filename : String 
            filename of the model state file
    
        """
        # load meta information
        filename = filename.replace('.hdf5','')
        if not path.endswith('/'):
            path = path + '/'
        h5f = h5py.File(path+filename+'.hdf5', 'r')
        if 'quasistatic' in h5f: # backwards compatibility
            self._quasistatic = h5f['quasistatic'].value
        else:
            self._quasistatic = False
        self._xmlsubdomains = self.__load_dictionary(h5f, 'xmlsubdomains')
        self._xmlboundaries = self.__load_dictionary(h5f, 'xmlboundaries')
        self._xmlmaterials = self.__load_dictionary(h5f, 'xmlmaterials')
        self._xmlboundaryconditions = self.__load_dictionary(h5f, 'xmlbcs')
        self._frequency = None
        frequency = 0
        if self._quasistatic:
            self._xmlfrequency = self.__load_dictionary(h5f, 'xmlfrequency')
            frequency = float(self._xmlfrequency['value'])
        self._modelparams = self.__load_dictionary(h5f, 'modelparams')
        datafilename  = h5f['h5datafile'].value
        self._basis_order = h5f['basis_order'].value
        self.MODELNAME = h5f['modelname'].value
        self.model_path = h5f['modelpath'].value
        self._runtime = h5f['runtime'].value
        
        # load model data
        self._mesh = Mesh()
        hdf5file = HDF5File(self._mesh.mpi_comm(),path+"/"+datafilename,'r')
        hdf5file.read(self._mesh, 'mesh', False)
        self._subdomains = MeshFunction('size_t', self._mesh)
        hdf5file.read(self._subdomains, 'subdomains')
        self._boundaries = MeshFunction('size_t', self._mesh)
        hdf5file.read(self._boundaries, 'boundaries')
        
        self.__set_function_space_and_default_boundary_conditions(frequency)
        self._u = Function(self._V)
        hdf5file.read(self._u, 'u')
        hdf5file.close()
        
        # Initialize quantities
        self.create_kappa()
        self.compute_derived_quantities()
        
        
    def __load_dictionary(self, h5f, name):
        xml_group = dict()
        for key, element in h5f[name].items():
            if isinstance(element, h5py.Group):
                xml_group[key.encode()] = self.__load_dictionary(h5f, name+'/'+key.encode())
            else:
                if isinstance(element.value,np.float64):
                    xml_group[key.encode()] = element.value.item()
                else:
                    xml_group[key.encode()] = element.value
                
        return xml_group
    
    def get_runtime(self):
        """
    
        Returns
        -------
        float
            Returns the runtime of the model in seconds.
    
        """
        return self._runtime
    
    def stop(self):
        """
        Method to execute certain commands at the end of the model pipeline.
        In default only prints the message 'done'.
        """
        print 'done'