'''
Created on Mar 31, 2017

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPy
'''
from fanpy.fieldmodels.fenics.abstract_fenics_field_model import AbstractFenicsFieldModel
import numpy as np
from fenics import *  #@UnusedWildImport

class AnalyticFieldModel(AbstractFenicsFieldModel):
    """
    Analytic example model for validating the field solution computed with Fenics.
    The model comprises a layered sphere (inner and outer sphere) in a 
    homogeneous electric field.
    
    The class inherits AbstractFenicsFieldModel
    """
    
    def __init__(self):
        AbstractFenicsFieldModel.__init__(self)
        self.set_basis_order(2)
        self.MODELNAME = 'analytic_example_model'
        self.model_path +='analytic_example_model/'
    
    def set_coarse_mesh(self, basis_order):
        """
        Load a coarser version of the mesh and set the basis order.
    
        Parameters
        ----------
        basis_order : int 
            Basis order for the ansatz functions.
      
        """
        self._debug = True
        self.set_basis_order(basis_order)
    
    def __create_analytic_model(self, modelparams):
        
        # The analytic model
        self.inner_r = 0.005 # inner radius
        self.outer_r = 0.015 # outer radius
        self.field_e_z = 10 # electric field z component V/m 
                
        dielp = modelparams['dielectric_properties']
        inner_mat = float(dielp['inner_material']['conductivity'])
        shell_mat = float(dielp['shell_material']['conductivity'])
        outer_mat = float(dielp['outer_material']['conductivity'])
        
        if self._quasistatic:
            if 'frequency' in modelparams:
                freq = modelparams['frequency']
            else:
                freq = self._frequency
            e0 = 8.854e-12
            inner_mat = np.complex(inner_mat, 
                                   2*np.pi*freq*e0*float(dielp['inner_material']['permittivity']))
            shell_mat = np.complex(shell_mat,
                                   2*np.pi*freq*e0*float(dielp['shell_material']['permittivity']))
            outer_mat = np.complex(outer_mat,
                                   2*np.pi*freq*e0*float(dielp['outer_material']['permittivity']))
        
        coefficient_matrix = np.array([[pow(self.inner_r, 3), -pow(self.inner_r, 3), -1, 0],
                                       [inner_mat*pow(self.inner_r, 3), -shell_mat*pow(self.inner_r, 3), 2*shell_mat, 0],
                                       [0, pow(self.outer_r, 3), 1, -1],
                                       [0, shell_mat*pow(self.outer_r, 3), -2*shell_mat, 2*outer_mat]])
        coefficient_rhs = np.array([0, 0, -self.field_e_z*pow(self.outer_r,3), -outer_mat*self.field_e_z*pow(self.outer_r,3)])
        self.coeffs = np.linalg.solve(coefficient_matrix, coefficient_rhs)
           
    def get_exact_analytic_potential(self, points):
        """
        Get the analytic exact electric potential at the given points
        
        Parameters
        ----------
        points : numpy array 
            Numpy (n,3) array containing the cartesian coordinates.
    
        Returns
        ----------
        numpy array 
            The electric potential at the given points.
      
        """
        if self._frequency == 0:
            solution = np.zeros((points.shape[0],))
        else:
            solution = np.zeros((points.shape[0],), dtype=complex)
        polpoints = self.__cart2pol(points)
        for idx in xrange(points.shape[0]):
            if np.abs(polpoints[idx,0]) <= self.inner_r:
                solution[idx] = self.coeffs[0]*polpoints[idx,0]
            elif (np.abs(polpoints[idx,0]) > self.inner_r and np.abs(polpoints[idx,0]) <= self.outer_r):
                solution[idx] = self.coeffs[1]*polpoints[idx,0]+self.coeffs[2]*pow(polpoints[idx,0], -2)
            elif (np.abs(polpoints[idx,0]) > self.outer_r):
                solution[idx] = -self.field_e_z*polpoints[idx,0]+self.coeffs[3]*pow(polpoints[idx,0], -2)
            solution[idx] = solution[idx]*np.cos(polpoints[idx,1])
        return solution
    
    # TODO
    def get_exact_analytic_fieldnorm(self, points):
        """
        Get the analytic exact electric field norm at the given points
        
        Parameters
        ----------
        points : numpy array 
            Numpy (n,3) array containing the cartesian coordinates.
    
        Returns
        ----------
        numpy array 
            The electric field norm at the given points.
      
        """
        if self._frequency == 0:
            solution = np.zeros((points.shape[0],2))
        else:
            solution = np.zeros((points.shape[0],2), dtype=complex)
        polpoints = self.__cart2pol(points)
        for idx in xrange(points.shape[0]):
            if np.abs(polpoints[idx,0]) <= self.inner_r:
                solution[idx,0] = self.coeffs[0]*np.cos(polpoints[idx,1])
                solution[idx,1] = -self.coeffs[0]*polpoints[idx,0]*np.sin(polpoints[idx,1])
            elif (np.abs(polpoints[idx,0]) > self.inner_r and np.abs(polpoints[idx,0]) <= self.outer_r):
                solution[idx,0] = (self.coeffs[1]-2*self.coeffs[2]*pow(polpoints[idx,0], -3))*np.cos(polpoints[idx,1])
                solution[idx,1] = -(self.coeffs[1]*polpoints[idx,0]+self.coeffs[2]*pow(polpoints[idx,0], -2))*np.sin(polpoints[idx,1])
            elif (np.abs(polpoints[idx,0]) > self.outer_r):
                solution[idx,0] = (-self.field_e_z-2*self.coeffs[3]*pow(polpoints[idx,0], -3))*np.cos(polpoints[idx,1])
                solution[idx,1] = -(-self.field_e_z*polpoints[idx,0]+self.coeffs[3]*pow(polpoints[idx,0], -2))*np.sin(polpoints[idx,1])
        return np.linalg.norm(solution, axis=1)
    
    def compute_derived_quantities(self):
        """
        Computes the derived quantities, including field quantities, such as
        electric potential, electric field norm, and current density norm, as well
        as integral quantities, such as the total power.    
        """
        AbstractFenicsFieldModel.compute_derived_quantities(self)
        self.__create_analytic_model(self._modelparams)
    
    def __cart2pol(self, points):
        polpoints = np.zeros((points.shape[0],3))
        for idx in xrange(points.shape[0]):
            polpoints[idx,0] = np.linalg.norm(points[idx,:])
            polpoints[idx,1] = np.arctan2(points[idx,1],points[idx,0])
        return polpoints
        
    