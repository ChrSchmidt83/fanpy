'''
Created on Mar 1, 2017

Deep brain stimulation field models.

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPy
'''

from fenics import *  #@UnusedWildImport
from fanpy.fieldmodels.fenics.abstract_fenics_field_model import AbstractFenicsFieldModel

class AbstractDBSFieldModel(AbstractFenicsFieldModel):
    """
    AbstractDBSFieldModel inherits AbstractFenicsFieldModel
    """
    
    def __init__(self):
        AbstractFenicsFieldModel.__init__(self)
        self.set_basis_order(2)
    
    def set_coarse_mesh(self, basis_order):
        self._debug = True
        self.set_basis_order(basis_order)

class VolumeConductorDBSElectrodeWithSTNInMotorRegion(AbstractDBSFieldModel):
    """ DBS electrode model of Medtronic 3387 lead surrounded by an
    encapsulation layer and a model of the subthalamic nucleus as target area.
    The subthalamic nucleus is positioned with the center of its motor functional
    zone located at the center of the active stimulation electrode contact (contact 2)
    
    Inherits AbstractDBSFieldModel
    """
    
       
    def __init__(self):
        AbstractDBSFieldModel.__init__(self)
        self.MODELNAME = 'dbs_electrode_with_stn_motor'
        self.model_path +='dbs_electrode_with_stn/'
        
class VolumeConductorDBSElectrodeWithSTNInLimbicRegion(AbstractDBSFieldModel):
    """ DBS electrode model of Medtronic 3387 lead surrounded by an
    encapsulation layer and a model of the subthalamic nucleus as target area.
    The subthalamic nucleus is positioned with the center of its limbic functional
    zone located at the center of the active stimulation electrode contact (contact 2)
    
    Inherits AbstractDBSFieldModel
    """
    
    def __init__(self):
        AbstractDBSFieldModel.__init__(self)
        self.MODELNAME = 'dbs_electrode_with_stn_limbic'
        self.model_path +='dbs_electrode_with_stn/'
        
class VolumeConductorDBSElectrode(AbstractDBSFieldModel):
    """ DBS electrode model of Medtronic 3387 lead surrounded by an
    encapsulation layer
    
    Inherits AbstractDBSFieldModel
    """
    
    def __init__(self):
        AbstractDBSFieldModel.__init__(self)
        self.MODELNAME = 'dbs_electrode'
        self.model_path +='dbs_electrode/'
        