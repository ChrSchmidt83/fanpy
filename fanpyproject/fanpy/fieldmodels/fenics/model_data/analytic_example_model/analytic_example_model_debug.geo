Merge "analytic_example_model.brep";

// Characteristic Length for Inner Sphere
Characteristic Length {1:2} = 0.5e-3;

// Characteristic Length for Outer Sphere
Characteristic Length {3:4} = 1.0e-3;

// Characteristic Length for Bounding Box
Characteristic Length {5:12} = 20e-3;

Field[1] = Box;
Field[1].VIn = 3.0e-3;
Field[1].VOut = 20e-3;
Field[1].XMax = 0.03;
Field[1].XMin = -0.03;
Field[1].YMax = 0.03;
Field[1].YMin = -0.03;
Field[1].ZMax = 0.03;
Field[1].ZMin = -0.03;
Background Field = 1;

Mesh.ElementOrder = 1;
Mesh.Optimize = 1;

// Boundaries
// Left Boundary
Physical Surface(1) = {3};
// Right Boundary
Physical Surface(2) = {8};

// Subdomains
// Inner Volume
Physical Volume(100) = {1};
// Shell Volume
Physical Volume(101) = {2};
// Bounding Box
Physical Volume(102) = {3};
