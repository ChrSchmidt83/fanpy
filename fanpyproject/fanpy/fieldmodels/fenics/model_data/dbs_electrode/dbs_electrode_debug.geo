Merge "dbs_electrode.brep";

// Contact 4: Volume 8
// Contact 3: Volume 6
// Contact 2: Volume 1
// Contact 1: Volume 3

// Insulation Lead : Volume 9
// Insulation 4-3 : Volume 7
// Insulation 3-2 : Volume 5
// Insulation 2-1 : Volume 2
// Insulation 1-t : Volume 4

// Encapsulation : Volume 10

// Bounding Box : Volume 11

// Characteristic Length for Bounding Box
Characteristic Length {12:22} = 10.0e-3;

// Characteristic Length for Encapsulation
Characteristic Length {1:14} = 0.5e-3;

// Characteristic Length for Insulation
Characteristic Length {1:11} = 0.5e-3;

// Characteristic Length for Contacts
Characteristic Length {1:4, 7:10} = 0.2e-3;


Field[1] = Box;
Field[1].VIn = 2e-3;
Field[1].VOut = 10e-3;
Field[1].XMax = 0.01;
Field[1].XMin = -0.01;
Field[1].YMax = 0.01;
Field[1].YMin = -0.01;
Field[1].ZMax = 0.01;
Field[1].ZMin = -0.01;
Background Field = 1;

Mesh.ElementOrder = 1;
Mesh.Optimize = 1;
//Mesh.OptimizeNetgen = 1;

// Boundaries
// Contact 1
Physical Surface(1) = {6};
// Contact 2
Physical Surface(2) = {1};
// Contact 3
Physical Surface(3) = {12};
// Contact 4
Physical Surface(4) = {16};
// Ground
Physical Surface(10) = {23:28};

// Subdomains
// Contacts
Physical Volume(100) = {1, 3, 6, 8};
// Insulation
Physical Volume(101) = {2, 4, 5, 7, 9};
// Encapsulation
Physical Volume(102) = {10};
// Tissue
Physical Volume(103) = {11};