Merge "dbs_electrode_with_stn_limbic.brep";

// STN: Volume 1

// Contact 4: Volume 9
// Contact 3: Volume 7
// Contact 2: Volume 2
// Contact 1: Volume 4

// Insulation Lead : Volume 10
// Insulation 4-3 : Volume 8
// Insulation 3-2 : Volume 6
// Insulation 2-1 : Volume 3
// Insulation 1-t : Volume 5

// Encapsulation : Volume 11

// Bounding Box : Volume 12

// Characteristic Length for Bounding Box
Characteristic Length {746:756} = 5.0e-3;

// Characteristic Length for STN
Characteristic Length {1:734} = 0.2e-3;

// Characteristic Length for Encapsulation
Characteristic Length {33:34, 37:38, 47:92, 735:748} = 0.1e-3;

// Characteristic Length for Insulation
Characteristic Length {735:745} = 0.1e-3;

// Characteristic Length for Contacts
Characteristic Length {735:738, 741:744} = 0.1e-3;

Field[1] = Box;
Field[1].VIn = 0.5e-3;
Field[1].VOut = 5e-3;
Field[1].XMax = 0.01;
Field[1].XMin = -0.01;
Field[1].YMax = 0.01;
Field[1].YMin = -0.01;
Field[1].ZMax = 0.01;
Field[1].ZMin = -0.01;
Background Field = 1;

Mesh.ElementOrder = 1;
Mesh.Optimize = 1;
//Mesh.OptimizeNetgen = 1;

// Boundaries

// Boundaries
// Contact 1
Physical Surface(1) = {1392};
// Contact 2
Physical Surface(2) = {1387};
// Contact 3
Physical Surface(3) = {1398};
// Contact 4
Physical Surface(4) = {1402};
// Ground
Physical Surface(10) = {1410:1415};

// Subdomain (Caution! Have to be complete, because only physical groups get meshed and exported by gmsh!)
// Contacts
Physical Volume(100) = {2, 4, 7, 9};
// Insulation
Physical Volume(101) = {3, 5, 6, 8, 10};
// Encapsulation
Physical Volume(102) = {11};
// Tissue
Physical Volume(103) = {12};
// STN
Physical Volume(104) = {1};


