Merge "dbs_electrode_with_stn_motor.brep";

// Version for debugging with coarse mesh

// STN: Volume 1

// Contact 4: Volume 9
// Contact 3: Volume 7
// Contact 2: Volume 2
// Contact 1: Volume 4

// Insulation Lead : Volume 10
// Insulation 4-3 : Volume 8
// Insulation 3-2 : Volume 6
// Insulation 2-1 : Volume 3
// Insulation 1-t : Volume 5

// Encapsulation : Volume 11

// Bounding Box : Volume 12

// Characteristic Length for Bounding Box
Characteristic Length {744:754} = 10.0e-3;

// Characteristic Length for STN
Characteristic Length {1:732} = 1e-3;

// Characteristic Length for Encapsulation
Characteristic Length {28:29, 36:38, 40:87, 733:746} = 0.5e-3;

// Characteristic Length for Insulation
Characteristic Length {733:743} = 0.5e-3;

// Characteristic Length for Contacts
Characteristic Length {733:736, 739:742} = 0.2e-3;

Field[1] = Box;
Field[1].VIn = 2e-3;
Field[1].VOut = 10e-3;
Field[1].XMax = 0.01;
Field[1].XMin = -0.01;
Field[1].YMax = 0.01;
Field[1].YMin = -0.01;
Field[1].ZMax = 0.01;
Field[1].ZMin = -0.01;
Background Field = 1;

Mesh.ElementOrder = 1;
Mesh.Optimize = 1;
//Mesh.OptimizeNetgen = 1;

// Boundaries
// Contact 1
Physical Surface(1) = {1386};
// Contact 2
Physical Surface(2) = {1381};
// Contact 3
Physical Surface(3) = {1392};
// Contact 4
Physical Surface(4) = {1396};
// Ground
Physical Surface(10) = {1404:1409};

// Subdomain (Caution! Have to be complete, because only physical groups get meshed and exported by gmsh!)
// Contacts
Physical Volume(100) = {2, 4, 7, 9};
// Insulation
Physical Volume(101) = {3, 5, 6, 8, 10};
// Encapsulation
Physical Volume(102) = {11};
// Tissue
Physical Volume(103) = {12};
// STN
Physical Volume(104) = {1};


