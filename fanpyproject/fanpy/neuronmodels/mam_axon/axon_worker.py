'''
Created on Feb 21, 2017

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPy
'''
from multiprocessing import Process
import numpy as np

class ComputeActivationNeuronWorker(Process):
    """
    Worker process to compute the minimum required stimulation amplitude in order to
    elicit an AP in the axon.

    The workers take incoming data from the data_queue and put computed results in the 
    result_queue. 

    The NeuronWorkers are initialized as daemon processes. They are
    terminated when the parent process is terminated.
    
    Inherits Process
    
    Parameters
    ----------
    widx : int 
        Worker index
    data_queue : Queue 
        The data queue
    result_queue : Queue 
        The result queue
     
    """
    
    def __init__(self, widx, data_queue, result_queue):
        super(ComputeActivationNeuronWorker, self).__init__()
        self.data_queue = data_queue
        self.result_queue = result_queue
        self.widx = widx
        self.daemon = True  # Kill the worker if the main python script is done.
        
    def run(self):
        """
        Runs the neuron worker until it receives a poison pill (null object),
        which terminates it.
        """
        from fanpy.neuronmodels.mam_axon.mam_axon import MamAxonModel
        axon_model=MamAxonModel()
        try:
            axon_model.init()
        except Exception as e:
            print 'Could not start axon worker. ',e
            # send poison pill to result queue
            while True:
                self.data_queue.get()
                self.data_queue.task_done()
                self.result_queue.put(None)
        
        # Wait for incoming data
        while True:
            data = self.data_queue.get()
            if data is None:
                print 'Stopping worker',self.widx
                self.data_queue.task_done()
                break
            
            point = data['point']
            dbs_signal = point.get_dbs_signal()
            dt = dbs_signal.get_dt()*1e3
            tstop = dbs_signal.get_dt()*(len(dbs_signal.get_vector())+1)*1e3  # s -> ms
            n_pulse = dbs_signal.get_n_pulse()
            axonparams = point.get_axon().get_axonparams()
            axon_model.set_parameters(axonparams, dt, tstop, n_pulse)
            
            if 'mode' in data and data['mode'] == 'optimal':
                self.compute_optimal_activation(axon_model, data)
            else:
                self.compute_activation(axon_model, data)
            
            print 'Worker ',self.widx, ': ', point
            self.result_queue.put({'point':point})
            self.data_queue.task_done()
            
    def compute_activation(self, axon_model, data):
        """
        Compute for a given electric potential distribution along the axon and a
        given signal with given axon parameters if an action potential is triggered
        or not.
        
        The point object included in the data dictionary will be modified by setting
        the activated flag and increasing the number of runs.
        
        Parameters
        ----------
        axon_model : MamAxonModel 
            The axon model.
        data : dict
            'point' : ThresholdPoint
                The threshold point object including the electric potential and
                the DBS signal.
            'stimmult' : float
                The stimulation multiplier
        
        """
        point = data['point']
        stimmult = data['stimmult']
        potential = point.get_potential()
        dbs_signal = point.get_dbs_signal()
        wf=np.outer(potential, dbs_signal.get_vector())*1e3*stimmult # V -> mV
        axon_model.set_extracellular_potential(wf)
        spike = axon_model.runstim(1.0)
        point.set_activated(True if spike > 0 else False)
        point.increase_number_of_runs(1)

        
    def compute_optimal_activation(self, axon_model, data):
        """
        Compute for a given electric potential distribution along the axon and a
        given signal with given axon parameters the minimum required stimulation
        amplitude to elicit an action potential.
        
        The point object included in the data dictionary will be modified by setting
        the activated flag, the computed threshold, and increasing the number of runs.
        
        Parameters
        ----------
        axon_model : MamAxonModel 
            The axon model.
        data : dict
            'point' : ThresholdPoint
                The threshold point object including the electric potential and
                the DBS signal.
            'stim_max' : float
                The maximum stimulation multiplier used to set the first estimate
                for the upper boundary for the bisection method (default 1)
            'xtol' : float
                Absolute tolerance for the bisection method (default 1e-6)
        
        """
        point = data['point']
        potential = point.get_potential()
        dbs_signal = point.get_dbs_signal()
        data_local = dict()
        data_local['wf']=np.outer(potential, dbs_signal.get_vector())*1e3 # V -> mV, for unit stimulus
        if 'stim_max' not in data:
            data_local['stim_max'] = 1
        else:
            data_local['stim_max'] = data['stim_max']
        if 'xtol' not in data:
            data_local['xtol'] = 1e-6
        else:
            data_local['xtol'] = data['xtol']
        # requiring 'wf', and optional 'stim_max' and 'xtol' in data
        result = axon_model.runstim_optimize(data_local)
        point.increase_number_of_runs(result['iterations'])
        if result['value'] is not np.nan:
            if data['stim_max']>=result['value']:
                point.set_activated(True)
            else:
                point.set_activated(False)
            point.set_threshold(result['value'])

            