'''
Created on Feb 15, 2017

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPy
'''

import os
import inspect
import scipy.optimize
import neuron as n
import numpy as np

class MamAxonModel():
    """
    Python API to mammalian axon model from McIntyre 2002
    """
    
    def init(self):
        self.__compile_neuron_model()
        
    def __compile_neuron_model(self):
        #get the path to the model_data folder
        class_name = self.__class__.__module__
        class_file = inspect.getfile(self.__class__)
        class_file = class_file.replace(".pyc","")
        class_file = class_file.replace(".py","")
        model_path = class_file[0:len(class_file)- \
                                len(class_name)+ \
                                class_name.rfind('.')]+('\\model_data\\')
        model_path = model_path.replace('\\','/')
        
        path = os.getcwd()
        os.chdir(model_path)
        try:
            # Compile the Neuron Axon Model Mechanisms
            if not os.path.exists(model_path+"/x86_64"):
                raise MamAxonModelError("Axon model seems to be not compiled. First run "+ 
                    "'nrnivmodl' in the axon model path.")
            
            try:    
                # Load the Mechanisms Library
                n.h('nrn_load_dll("x86_64/.libs/libnrnmech.so")')
                # Load the Model
                n.h('{load_file("axon4pyfull.hoc")}')
            except Exception as e:
                raise MamAxonModelError("Couldn't load axon dynamics ", e)
        
        except Exception as e:
            raise MamAxonModelError(e)   
        finally:
            os.chdir(path)
    
    
    def set_parameters(self, params, dt, tstop, n_pulse):
        """
        Set the axon model parameters, which initializes the model again.
                
        Parameters
        ----------
        params : dict
            Keys to be looked up in Axon class function get_axonparams()
        
        dt : float
            Time step in seconds
        
        tstop : float
            Stop time in seconds
        
        n_pulse : int
            Number of pulses in the signal pulse train (default 1)
        
        """
        # delete nodes
        n.h.deletenodes()
        n.h.axonnodes=params['ranvier_nodes']
        n.h.paranodes1=params['para1_nodes']
        n.h.paranodes2=params['para2_nodes']
        n.h.axoninter=params['inter_nodes']
        n.h.axontotal=params['total_nodes']
        # create nodes
        n.h.createnodes()
        # update morphological parameters
        n.h.fiberD=params['fiberD']
        n.h.deltax=params['deltax']
        n.h.nodelength=params['ranvier_length']
        n.h.paralength1=params['para1_length']
        n.h.paralength2=params['para2_length']
        n.h.axonD=params['axon_diameter']
        n.h.nodeD=params['node_diameter']
        n.h.paraD1=params['para1_diameter']
        n.h.paraD2=params['para2_diameter']
        n.h.nl=params['lamellas']
        # compute dependent variables
        n.h.dependent_var()
        # initialize model
        n.h.initialize()
        # setup AP watchers
        n.h.setupAPWatcher_0() # 'left' end of axon
        n.h.setupAPWatcher_1() # 'right' end of axon
        # set time and time step information
        n.h.dt = dt
        n.h.tstop = tstop
        n.h.n_pulse = n_pulse
        
    def set_extracellular_potential(self, epotwf):
        """
        Set the time-dependent extracellular potential for all segments of the
        axon model.
        
        Parameters
        ----------
        epotwf : numpy array 
            The time-dependent extracellular potential along the axon for each
            axon segment and time step.
        
        """
        self.epotwf = epotwf
           
    
    def runstim(self, stimmult):
        """
        Run the axon model with the given time-dependent extracellular
        potential data. The data is linearly scaled by the stimulation
        multiplier stimmult.
        
        The time-dependent extracellular potential has to be set by using
        set_extracellular_potential(epotwf).
        
        Parameters
        ----------
        stimmult : float 
            The stimulation multiplier.
            
        Returns
        ----------
        float 
            Whether a spike was observed or not. 
            True : 0.5
            False : -0.5
            
        """
        wfscaled=self.epotwf*stimmult
        #print 'spikeobserved:',n.h.spikeobserved_0, n.h.spikeobserved_1
        #print 'countspikes: ',n.h.countspikes_0, n.h.countspikes_1
        #print 'stoprun:', n.h.stoprun
        for i in range(0,self.epotwf.shape[0],1):
            n.h.wf[i]=n.h.Vector(wfscaled[i,:])
    
        n.h.stimul()
        n.h.run()
        spike=n.h.stoprun-0.5
        # returns 0.5 on AP and -0.5 on NO AP
        return spike
    
    # Find the smallest stimulation amplitude to elicit an action potential
    def runstim_optimize(self, data):
        """
        Run the axon model with the given time-dependent extracellular
        potential data and compute the minimum stimulation amplitude to 
        elicit an action potential.
        
        Parameters
        ----------
        data : dict 
            'stim_min' : float
                Minimum stimulation amplitude (default 0.0)
            'stim_max' : float
                Maximum stimulation amplitude (first estimate, default 1.0)
            'xtol' : float
                Absolute tolerance for the bisection method (default 1e-6)
            'wf' : numpy array
                time-dependent extracellular potential along the axon for each
                segment and each time step.
            
        Returns
        ----------
        dict 
            'value' : float
                The threshold value (stimulation multiplier) to elicit an 
                action potential. Might be Numpy.NaN if it could be not computed.
            'iterations' : int
                Number of iterations required to compute the threshold value
            
        """
        wf = data['wf']
        self.set_extracellular_potential(wf)
        if 'stim_min' in data:
            stim_min = data['stim_min']
        else:
            stim_min = 0.0
        
        if 'stim_max' in data:
            stim_max = data['stim_max']
        else:
            stim_max = 1.0
       
        try:
            pre_iter = 1
            while (self.runstim(stim_max)<0 and stim_max<1e4):
                stim_min = stim_max
                stim_max = stim_min*2
                pre_iter = pre_iter+1
            
            if 'xtol' in data:
                tolerance = data['xtol']
            else:
                tolerance = 1e-6
            #print 'Searching for stimamp in ',stim_min, stim_max, ' with tolerance ',tolerance
            osa = scipy.optimize.brentq(self.runstim, stim_min, stim_max, args=(), xtol=tolerance, full_output=True, disp=True)
            
            result = {
                'value':osa[0],
                'iterations':osa[1].iterations+pre_iter
                }
        except Exception as err:
            print 'Error during stimulation amplitude finding: ', type(err), err
            result = {
                'value':np.nan,
                'iterations':0
                }
        
        return result


class MamAxonModelError(Exception): 
    """
    Helper class for MamAxonModel errors / exceptions.
    """   
    pass