'''
Created on Mar 17, 2017

Utility classes for VTA computation.

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPy
'''

import numpy as np
from fanpy.core.axon import Axon
from scipy import interpolate
from scipy.interpolate import InterpolatedUnivariateSpline
from matplotlib import _cntr as cntr
from fanpy.neuronmodels.mam_axon.axon_worker import ComputeActivationNeuronWorker
from multiprocessing import Queue, JoinableQueue
from fanpy.core.coord_transform import Transform
import cPickle as pickle
import time as pytime
from fanpy.core.dbssignal import DBSsignal
import os
import sys

class Threshold_Location(object):
    """
    Location of an axon (center location) abstracted in a grid without explicit
    cartesian coordinate information. This concept is required to easily identify
    neighbours and axons of a certain group, e.g. on a certain line or plane.
    
    Parameters
    ----------
    rotational : int
        rotational index (the plane around the electrode)
    tangential : int
        tangential index (the location along the electrode)
    normal : int
        normal index (the location perpendicular to the electrode)
    """
    
    def __init__(self, rotational, tangential, normal):
        self.__rotational = rotational
        self.__tangential = tangential
        self.__normal = normal
        
    def get_rotational(self):
        """
        Returns
        ----------
        int
            rotational index
        """ 
        return self.__rotational
        
    def get_tangential(self):
        """
        Returns
        ----------
        int
            tangential index
        """ 
        return self.__tangential
    
    def get_normal(self):
        """
        Returns
        ----------
        int
            normal index
        """ 
        return self.__normal
    
    def get_as_array(self):
        """
        Returns
        ----------
        numpy array
            [rotational, tangential, normal]
        """ 
        return np.array([self.__rotational,
                         self.__tangential,
                         self.__normal])
        
    def str(self):
        """
        Returns
        ----------
        string
            [rotational, tangential, normal]
        """ 
        return np.array_str(self.get_as_array())

class Threshold_Point():
    """
    Represents the linking object between the axon model, its location, and
    the electric potential along the axon.
    
    Parameters
    ----------
    location : Threshold_Location
        The abstract location indices of the axon model.
    """
    
    def __init__(self, location):
        self.__location = location
        self.__state = {
            'coordinate': None,
            'activated': None,
            'threshold': None,
            'axon': None,
            'transform': Transform(),
            'dbs_signal': None,
            'potential': None,
            'number_of_runs': 0
            }
        
    def get_location(self):
        """
        Returns
        ----------
        Threshold_Location
            The abstract threshold location indices
        """ 
        return self.__location
    
    def set_coordinate(self, coordinate):
        """
        Set the cartesian coordinates of the axon's center location.
        
        Parameters
        ----------
        coordinate : numpy array
            [x,y,z]
        """
        self.__state['coordinate'] = coordinate
        
    def get_coordinate(self):
        """
        Returns
        ----------
        numpy array
            Cartesian coordinates of the axon center
        """ 
        return self.__state['coordinate']
        
    def set_threshold(self, threshold):
        """
        Set the (previously computed) threshold value required to elicit an
        action potential in the axon. 
        
        Parameters
        ----------
        threshold : float
            The threshold value (stimulation multiplier)
        """
        self.__state['threshold'] = threshold
        
    def get_threshold(self):
        """
        Returns
        ----------
        float
            The threshold value required to elicit an action potential
        """ 
        return self.__state['threshold']
    
    def set_activated(self, activated):
        """
        Set the (computed) activated flag indicating if the axon was activated
        by the given stimulation multiplier and time-dependent extracellular
        potential.
        
        Parameters
        ----------
        activated : boolean
            True, if got activated.
        """
        self.__state['activated'] = activated
        
    def get_activated(self):
        """
        Returns
        ----------
        boolean
            True, if axon is activated by the given stimulation multiplier
        """ 
        return self.__state['activated']
    
    def set_axon(self, axon):
        """
        Set the axon model including the axon parameters.
        
        Parameters
        ----------
        axon : Axon
            The axon model.
        """
        self.__state['axon'] = axon
        
    def get_axon(self):
        """
        Returns
        ----------
        Axon
            The Axon model including the axon parameters.
        """ 
        return self.__state['axon']
    
    def set_transform(self, transform):
        """
        Set the transform object to relocate the axon coordinates in the
        cartesian coordinate system.
        
        Parameters
        ----------
        transform : Transform
            The transform object.
        """
        self.__state['transform'] = transform
        
    def get_transform(self):
        """
        Returns
        ----------
        Transform
            The coordinate transform object to relocate the axon in the
            cartesian coordinate system.
        """ 
        return self.__state['transform']
    
    def set_dbs_signal(self, dbs_signal):
        """
        Set the dbs signal object including the dbs signal parameters
        
        Parameters
        ----------
        dbs_signal : DBSsignal
            The DBSsignal object including the dbs signal parameters
        """
        self.__state['dbs_signal'] = dbs_signal
        
    def get_dbs_signal(self):
        """
        Returns
        ----------
        DBSsignal
            The dbs signal object including the dbs signal parameters
        """ 
        return self.__state['dbs_signal']
    
    def set_potential(self, potential):
        """
        Set the time-dependent extracellular potential along the axon for
        each axon segment and time step.
        
        Parameters
        ----------
        potential : numpy array
            The time-dependent extracellular potential
        """
        self.__state['potential'] = potential
        
    def get_potential(self):
        """
        Returns
        ----------
        numpy array
            The time-dependent electric potential along the axon for each
            axon segment and time step.
        """ 
        return self.__state['potential']
    
    def increase_number_of_runs(self, value):
        """
        Increase the number of runs counter.
        
        Parameters
        ----------
        value : int
            Value for which the number of runs counter should be increased.
        """
        self.__state['number_of_runs'] =  self.__state['number_of_runs'] + value
        
    def get_number_of_runs(self):
        """
        Returns
        ----------
        int
            The number of runs performed until now.
        """ 
        return self.__state['number_of_runs']
    
    def get_state(self):
        """
        Returns
        ----------
        dict
            Return the current state of the object.
        """ 
        return self.__state
    
    def apply_state(self, point):
        """
        Apply the state of the given Threshold_Point object to this
        Threshold_Point object.
        
        Parameters
        ----------
        point : Threshold_Point
            The Threshold_Point object, from which the state should be applied
            to this Threshold_Point object.
        """
        self.__state.update(point.get_state())
        
    def __repr__(self):
        return ('Point loc='+str(self.__location.str())
                +'. Activated: '+str(self.__state['activated'])
                +' Threshold: '+str(self.__state['threshold']))
    
class Threshold_Line():
    """
    Represents a collection of Threshold Points along an abstract line.
    """
    
    def __init__(self):
        self.__points = {}
        self.__tangential = None
        self.__rotational = None
        
    def has_point(self, threshold_point):
        """
        Checks if a Threshold Point with the same abstract location indices
        already exists.
        
        Parameters
        ----------
        threshold_point : Threshold_Point
            The Threshold_Point object.
            
        Returns
        ----------
        boolean
            True, if it already exists.       
        """
        key = threshold_point.get_location().get_normal()
        tangential = threshold_point.get_location().get_tangential()
        rotational = threshold_point.get_location().get_rotational()
        if ((self.__tangential is not None and 
             self.__tangential != tangential) or
            (self.__rotational is not None and
             self.__rotational != rotational)):
            raise Exception('point with loc=',threshold_point.get_location().str(),' does not'+
                            'fit to this line: rot=',self.__rotational,',tan=',self.__tangential)
        elif self.__points.has_key(key):
            return True
        return False
    
    def get_tangential(self):
        """
        Returns
        ----------
        int
            The tangential index of this line.       
        """
        return self.__tangential
    
    def get_rotational(self):
        """
        Returns
        ----------
        int
            The rotational index of this line.       
        """
        return self.__rotational
                    
    def add_point(self, threshold_point):
        """
        Adds a point to the threshold line. This happens only if not another
        point with the same location existed on the line. If already such a 
        point existed, then the state of the new threshold point will be applied
        to the already existing one.
        
        Parameters
        ----------
        threshold_point : Threshold_Point
            The Threshold_Point object.
            
        Returns
        ----------
        boolean
            True, if it already exists.       
        """
        tangential = threshold_point.get_location().get_tangential()
        rotational = threshold_point.get_location().get_rotational()
        key = threshold_point.get_location().get_normal()
        if not(self.has_point(threshold_point)):
            self.__points[key] = threshold_point
            self.__tangential = tangential
            self.__rotational = rotational
        else:
            self.__points[key].apply_state(threshold_point)
        
    def get_points(self):
        """
        Returns
        ----------
        List of Threshold_Point objects
            All points of this line.
        """
        sorted_keys = sorted(self.__points)
        points = []
        for key in sorted_keys:
            points.append(self.__points[key])
        return points
    
    def get_coords(self):
        """
        Returns
        ----------
        numpy array
            A numpy (n,3) array including all coords of the points on this line.        
        """
        points = self.get_points()
        coords_coarse = np.zeros((len(points),3))
        for idx, point in enumerate(points):
            coords_coarse[idx,:] = point.get_coordinate()
        return coords_coarse
    
    def get_coord_threshold_matrix(self):
        """
        Returns
        ----------
        numpy array
            A numpy (n,4) array including all coords and the threshold values
            of the threshold points on this line.       
        """
        points = self.get_points()
        coord_threshs = np.zeros((len(points),4))
        for idx, point in enumerate(points):
            coord_threshs[idx,0:3] = point.get_coordinate()
            coord_threshs[idx,3] = point.get_threshold()
        return coord_threshs
    
    def get_coord_for_optimal_threshold(self, stimmultiplier):
        """
        Returns
        ----------
        numpy array
            The (minimum) coordinates of an axon which would elicit an action
            potential for the given stimulation multiplier.       
        """
        ct_matrix = self.get_coord_threshold_matrix()
        transform = self.get_points()[0].get_transform()
        coords_in_std_plane = transform.inverse_transform_coords_to_plane(ct_matrix[:,0:3])
        #distance = np.interp(stimmultiplier, ct_matrix[:,3], coords_in_std_plane[:,1])
        interpolator = InterpolatedUnivariateSpline(ct_matrix[:,3], coords_in_std_plane[:,1], k=1)
        distance = interpolator(stimmultiplier)
        if np.isnan(distance):
            # this is the case if the threshold-distance relationship is probably not monotonous!
            print("WARN! Found a probably non-monotonous threshold-distance relationship. Using quadratic polyfit as fallback! Please investigate issue and check results with caution!")
            pfit = np.polyfit(ct_matrix[:,3], coords_in_std_plane[:,1], 2)
            distance = np.polyval(pfit, stimmultiplier)
            print("New distance: "+str(distance))
            
        hcoord = np.array([[0,distance,0]])
        hcoord = transform.transform_coords_to_plane(hcoord)
        return hcoord
    
    def get_number_of_runs(self):
        """
        Returns
        ----------
        int
            The number of runs summed up for all points on this line.       
        """
        number_of_runs = 0
        sorted_keys = sorted(self.__points)
        for key in sorted_keys:
            number_of_runs = number_of_runs + self.__points[key].get_number_of_runs()
        return number_of_runs
    
    def get_point(self, point):
        """
        Returns the Threshold_Point object with the same threshold location
        as the given threshold point.
        
        Parameters
        ----------
        point : Threshold_Point
            The Threshold_Point object.
            
        Returns
        ----------
        Threshold_Point
            The Threshold_Point object with the same threshold location as the
            given threshold point.       
        """
        key = point.get_location().get_normal()
        return self.__points[key]
    
    def has_zero_activation_hull(self):
        """
        Returns
        ----------
        boolean
            True, if the threshold point on this line with the largest (normal)
            index is not activated.         
        """ 
        point = self.__points[max(self.__points.keys())]
        if point.get_activated() is None or point.get_activated() is True:
            return False
        return True
            
        

class Threshold_Plane():
    """
    Represents a collection of Threshold Lines in abstract plane.
    """
    
    def __init__(self):
        self.__lines = {}
        self.__rotational = None
        
    def has_line(self, threshold_point):
        """
        Checks if a Threshold Line with the same abstract rotational and 
        tangential indices as the given threshold point already exists.
        
        Parameters
        ----------
        threshold_point : Threshold_Point
            The Threshold_Point object.
            
        Returns
        ----------
        boolean
            True, if it already exists.       
        """
        key = threshold_point.get_location().get_tangential()
        rotational = threshold_point.get_location().get_rotational()
        if (self.__rotational is not None and
             self.__rotational != rotational):
            raise Exception('point with loc=',threshold_point.get_location().str(),' does not'+
                            'fit to this plane: rot=',self.__rotational)
        elif self.__lines.has_key(key):
            return True
        return False
    
    def has_point(self, threshold_point):
        """
        Checks if a Threshold Point with the same abstract location indices
        already exists.
        
        Parameters
        ----------
        threshold_point : Threshold_Point
            The Threshold_Point object.
            
        Returns
        ----------
        boolean
            True, if it already exists.       
        """
        if self.has_line(threshold_point):
            key = threshold_point.get_location().get_tangential()
            return self.__lines[key].has_point(threshold_point)
        return False
    
    def add_point(self, threshold_point):
        """
        Adds a point to the threshold plane. This happens only if not another
        point with the same location existed on the plane. If already such a 
        point existed, then the state of the new threshold point will be applied
        to the already existing one.
        
        Parameters
        ----------
        threshold_point : Threshold_Point
            The Threshold_Point object.
            
        Returns
        ----------
        boolean
            True, if it already exists.       
        """
        key = threshold_point.get_location().get_tangential()
        rotational = threshold_point.get_location().get_rotational()
        if not self.has_line(threshold_point):
            self.__lines[key] = Threshold_Line()
            self.__rotational = rotational
        self.__lines[key].add_point(threshold_point)
        
    def get_points(self):
        """
        Returns
        ----------
        List of Threshold_Point objects
            All points of this line.
        """
        points = []
        sorted_keys = sorted(self.__lines)
        for key in sorted_keys:
            points.extend(self.__lines[key].get_points())
        return points
    
    def get_coords(self):
        """
        Returns
        ----------
        numpy array
            A numpy (n,3) array including all coords of the points on this line.        
        """
        sorted_keys = sorted(self.__lines)
        coords_coarse = np.zeros((0,3))
        for key in sorted_keys:
            coords_coarse = np.concatenate((coords_coarse, self.__lines[key].get_coords()), axis=0)
        return coords_coarse
    
    def get_coord_threshold_matrix(self):
        """
        Returns
        ----------
        numpy array
            A numpy (n,4) array including all coords and the threshold values
            of the threshold points on this line.       
        """
        sorted_keys = sorted(self.__lines)
        coord_threshs = np.zeros((0,4))
        for key in sorted_keys:
            coord_threshs = np.concatenate((coord_threshs, self.__lines[key].get_coord_threshold_matrix()), axis=0)
        return coord_threshs
    
    def get_coords_for_optimal_threshold(self, stimmult):
        """
        Returns
        ----------
        numpy array
            The (minimum) coordinates of axons which would elicit an action
            potential for the given stimulation multiplier.       
        """
        # Data has to be temporarily rotated back to standard plane
        ct_matrix = self.get_coord_threshold_matrix()
        # representative transform in this plane used for transforming the coords_coarse
        transform = self.__lines[0].get_points()[0].get_transform()
        coords_in_std_plane = transform.inverse_transform_coords_to_plane(ct_matrix[:,0:3])
        xi = np.linspace(np.min(coords_in_std_plane[:,1]), np.max(coords_in_std_plane[:,1]), 100)
        yi = np.linspace(np.min(coords_in_std_plane[:,2]), np.max(coords_in_std_plane[:,2]), 100)
        XI, YI = np.meshgrid(xi, yi)
        npoints = np.vstack((coords_in_std_plane[:,1], coords_in_std_plane[:,2])).T
        npoints = np.asarray(npoints)
        nvalues = np.asarray(ct_matrix[:,3])
        DEM = interpolate.griddata(npoints, nvalues, (XI,YI), method='linear')
        c = cntr.Cntr(XI,YI, DEM)
        res = c.trace(stimmult)
        nseg = len(res) // 2
        segments = res[:nseg]
        cpoints = np.zeros((0,2))
        # find not nan segment list
        countnan=0
        for segment in segments:
            for idx in range(segment.shape[0]):
                if np.max(np.isnan(segment[idx,:])):
                    countnan = countnan + 1
                    continue
                cpoints = np.concatenate((cpoints,np.array([segment[idx,:]])),axis=0)
        
        if cpoints.shape[0]==0:        
            print 'WARN: Found no points in Plane '+str(self.__rotational)+' for stimmultiplier '+str(stimmult)
    
        cshape = cpoints.shape[0]
        hcoords = np.zeros((cshape,3))
        hcoords[:,0]= 0
        hcoords[:,1]=cpoints[:,0]
        hcoords[:,2]=cpoints[:,1]
        hcoords = transform.transform_coords_to_plane(hcoords)
        return hcoords
    
    def get_number_of_runs(self):
        """
        Returns
        ----------
        int
            The number of runs summed up for all points on this line.       
        """
        number_of_runs = 0
        sorted_keys = sorted(self.__lines)
        for key in sorted_keys:
            number_of_runs = number_of_runs + self.__lines[key].get_number_of_runs()
        return number_of_runs  
    
    def get_point(self, point):
        """
        Returns the Threshold_Point object with the same threshold location
        as the given threshold point.
        
        Parameters
        ----------
        point : Threshold_Point
            The Threshold_Point object.
            
        Returns
        ----------
        Threshold_Point
            The Threshold_Point object with the same threshold location as the
            given threshold point.       
        """
        key = point.get_location().get_tangential()
        return self.__lines[key].get_point(point)
    
    def get_lines(self):
        """
        Returns
        ----------
        dict
            Keys are the tangential indices and the values the corresponding
            Threhold_Line objects
        """ 
        return self.__lines
    
    def get_line(self, tangential_index):
        """
        Return the threshold line for the given tangential index
        
        Parameters
        ----------
        tangential_index : int
            The tangential index.
                   
        Returns
        ----------
        Threshold_Line
            The Threshold line object with the given tangential index
        """ 
        return self.__lines[tangential_index]
    
    def has_zero_activation_hull(self):
        """
        Returns
        ----------
        boolean
            True, if each threshold point with the largest (normal) index 
            in each line is not activated.         
        """ 
        for line in self.__lines.values():
            if line.has_zero_activation_hull() is False:
                return False
        return True
    
class Threshold_Volume():
    """
    Represents a collection of Threshold Planes in abstract volume.
    
    This class is the main class for the computation of the volume of 
    tissue activated (VTA). It uses for that the class structures of threshold
    planes, lines and points.
    """
    
    def __init__(self):
        self._planes = {}
        self._state = {
            'impedance': 0.0, # for testing
            'symmetric': False,
            'rotation_step': 10.0,
            'approx_by_field': False
            }
        self._runtime = 0
        self._stim_min = None
        self._stim_max = None
        
    def set_stim_min(self, stim_min):
        """
        Set the minimum stimulation amplitude multiplier.
        
        Parameters
        ----------
        stim_min : float
            The minimum stimulation amplitude multiplier.
                   
        """ 
        self._stim_min = stim_min
        
    def get_stim_min(self):
        """
        Returns
        ----------
        float
            Get the minimum stimulation amplitude multiplier.
        """ 
        return self._stim_min

    def set_stim_max(self, stim_max):
        """
        Set the maximum stimulation amplitude multiplier.
        
        Parameters
        ----------
        stim_min : float
            The maximum stimulation amplitude multiplier.
                   
        """ 
        self._stim_max = stim_max
        
    def get_stim_max(self):
        """
        Returns
        ----------
        float
            Get the maximum stimulation amplitude multiplier.
        """ 
        return self._stim_max
        
    def get_state(self):
        """
        Returns
        ----------
        dict
            The state of the threshold volume.
        """ 
        return self._state
    
    def set_state(self, state):
        """
        Set the state of the threshold volume. Keys can be found in the init
        method of the threshold volume class.
        
        Parameters
        ----------
        state : dict
            The state dictionary for the threshold volume class.
                   
        """ 
        self._state = state
        
    def set_impedance(self, impedance):
        """
        (Experimental) Set the impedance of the field model for later evaluation.
        
        Parameters
        ----------
        impedance : float
            The impedance of the field model.
                   
        """ 
        self.get_state()['impedance']=impedance
        
    def get_impedance(self):
        """
        Returns
        ----------
        float
            (Experimental). The impedance of the field model.
        """ 
        return self.get_state()['impedance']
    
    def set_rotation_step(self, rotation_step):
        """
        Set the rotation stepping for the threshold planes.
        
        Parameters
        ----------
        rotation_step : float
            Value between 0 and 180. 180 would result in 2 planes, 90 would
            result in 4 planes, and so on.
            The value 0 is special resulting in a rotational symmetric
            computation of the VTA from one representative plane.
                   
        """ 
        self.get_state()['rotation_step']=rotation_step
        
    def get_rotation_step(self):
        """
        Returns
        ----------
        float
            The rotational stepping between the planes.
        """ 
        return self.get_state()['rotation_step']
    
    def set_symmetric(self, symmetric):
        """
        Flag if the VTA is rotational symmetric
        
        Parameters
        ----------
        symmetric : boolean
            True, if rotational symmetric
                   
        """ 
        self.get_state()['symmetric']=symmetric
    
    def get_symmetric(self):
        """
        Returns
        ----------
        boolean
            True, if rotational symmetric
        """ 
        return self.get_state()['symmetric']
    
    def set_approxfield(self, approxfield):
        """
        Flag if the VTA computation should be carried out using field
        threshold approximations.
        
        Parameters
        ----------
        approxfield : boolean
            True, if field threshold values should be used.
                   
        """ 
        self.get_state()['approx_by_field']=approxfield
    
    def get_approxfield(self):
        """
        Returns
        ----------
        boolean
            True, if field threshold values should be used to approximate the VTA.
        """ 
        return self.get_state()['approx_by_field']
    
    def has_plane(self, threshold_point):
        """
        Checks if a Threshold Plane with the same abstract rotational index as
        the given threshold point already exists.
        
        Parameters
        ----------
        threshold_point : Threshold_Point
            The Threshold_Point object.
            
        Returns
        ----------
        boolean
            True, if it already exists.       
        """
        key = threshold_point.get_location().get_rotational()
        if self.get_planes().has_key(key):
            return True
        return False
    
    def has_line(self, threshold_point):
        """
        Checks if a Threshold Line with the same abstract rotational and 
        tangential indices as the given threshold point already exists.
        
        Parameters
        ----------
        threshold_point : Threshold_Point
            The Threshold_Point object.
            
        Returns
        ----------
        boolean
            True, if it already exists.       
        """
        if self.has_plane(threshold_point):
            key = threshold_point.get_location().get_rotational()
            return self.get_planes()[key].has_line(threshold_point)
        return False
        
    def has_point(self, threshold_point):
        """
        Checks if a Threshold Point with the same abstract location indices
        already exists.
        
        Parameters
        ----------
        threshold_point : Threshold_Point
            The Threshold_Point object.
            
        Returns
        ----------
        boolean
            True, if it already exists.       
        """
        if self.has_line(threshold_point):
            key = threshold_point.get_location().get_rotational()
            return self.get_planes()[key].has_point(threshold_point)
        return False
    
    def add_point(self, threshold_point):
        """
        Adds a point to the threshold volume. This happens only if not another
        point with the same location existed in the volume. If already such a 
        point existed, then the state of the new threshold point will be applied
        to the already existing one.
        
        Parameters
        ----------
        threshold_point : Threshold_Point
            The Threshold_Point object.
            
        Returns
        ----------
        boolean
            True, if it already exists.       
        """
        key = threshold_point.get_location().get_rotational()
        if not self.has_plane(threshold_point):
            self.get_planes()[key] = Threshold_Plane()
        self.get_planes()[key].add_point(threshold_point)
        
    def get_points(self):
        """
        Returns
        ----------
        List of Threshold_Point objects
            All points of this line.
        """
        points = []
        sorted_keys = sorted(self.get_planes())
        for key in sorted_keys:
            points.extend(self.get_planes()[key].get_points())
            
        return points
    
    def get_point(self, point):
        """
        Returns the Threshold_Point object with the same threshold location
        as the given threshold point.
        
        Parameters
        ----------
        point : Threshold_Point
            The Threshold_Point object.
            
        Returns
        ----------
        Threshold_Point
            The Threshold_Point object with the same threshold location as the
            given threshold point.       
        """
        key = point.get_location().get_rotational()
        return self.get_planes()[key].get_point(point)
    
    def get_plane(self, rotational_index):
        """
        Returns the Threshold_Plane object for the given rotational index.
        
        Parameters
        ----------
        rotational_index : int
            The rotational index.
        Returns
        ----------
        Threshold_Plane
            The Threshold_Plane object for the given rotational index.
        """
        return self.get_planes()[rotational_index]
        
    
    def has_zero_activation_hull(self):
        """
        Returns
        ----------
        boolean
            True, if each threshold point with the largest (normal) index 
            in each line of each plane is not activated.         
        """ 
        for plane in self.get_planes().values():
            if plane.has_zero_activation_hull() is False:
                return False
        return True
    
    def get_initial_points(self):
        """
        Returns
        ----------
        List of Threshold_Point objects
            Initial points from which the adaptive algorithm should start computing
            the VTA.
        """ 
        points = []
        for point in self.get_points():
            if point.get_activated() is None:
                points.append(point)
        return points
    
    def get_number_of_runs(self):
        """
        Returns
        ----------
        int
            The number of runs for all threshold points.
        """ 
        number_of_runs = 0
        sorted_keys = sorted(self.get_planes())
        for key in sorted_keys:
            number_of_runs = number_of_runs + self.get_planes()[key].get_number_of_runs()
        return number_of_runs
    
    def _generate_workers(self, number, request_queue, resultqueue):
        # start axon worker clients
        workers = []
        for proc in range(number):
            worker = ComputeActivationNeuronWorker(proc, request_queue, resultqueue)
            workers.append(worker)
            worker.start()
            
        return workers      
    
    def compute_activation_range(self, number_of_workers, params, stimmult):
        """
        Determines all axon locations which get activated by the given field
        distribution and stimulation multiplier, and given initial seed
        points.
        
        The NEURON workers get started and will be terminated after a
        zero-activation hull is found. 
        
        Parameters
        ----------
        number_of_workers : int
            The number of workers (processes) which should run the axon model
            with NEURON.
            
        stimmult : float
            The stimulation multiplier
            
        params : dict
            'fiberD' : float
                axon fiber diameter in um. (Default 5.7)
            'g_step' : float
                spatial stepping in normal and tangential direction in m. (Default 0.5)
            'rot_degree_step' : float
                rotation degree stepping in degree. (Default 10.0)
            'symmetric' : boolean
                Rotational symmetry flag. (Default False)
            'approx_by_field' : boolean
                Field Threshold Apporoximation flag. (Default False)
                If activated, only the threshold line in each plane is determined
                by the adaptive algorithm, which is required to compute the
                threshold-distance relation.
                
            'dbs_signal' : DBSsignal
                The DBSsignal.
            'field' : AbstractFenicsFieldModel
                The Field model.
            'Z' : float
                The field model impedance.
                                    
        """
        start = pytime.time()
        request_queue = JoinableQueue()
        resultqueue = Queue()
        workers = self._generate_workers(number_of_workers, request_queue, resultqueue)
           
        initial_points = self.get_initial_points()
        if not 'fiberD' in params:
                params['fiberD']=5.7
        if not 'g_step' in params:
            params['g_step'] = 0.5e-3
        if 'rot_degree_step' in params:
            rot_degree = params['rot_degree_step']
            if rot_degree > 0 and rot_degree < 180:
                self.set_rotation_step(rot_degree)
            else:
                print ('Rotation degree step of '+str(rot_degree)
                       +' not applicable. Using default rotation degree step of '
                       +str(self.get_rotation_step())+'.')
        if 'symmetric' in params:
            symmetric = params['symmetric']
            if symmetric == True or symmetric == False:
                self.set_symmetric(symmetric)
            else:
                print ('Awaiting symmetric flag to be True or False, but is '
                       +str(symmetric)+'. Using default value of '+str(self.get_symmetric()))
                
        if 'approx_by_field' in params:
            approxfield = params['approx_by_field']
            if approxfield == True or approxfield == False:
                self.set_approxfield(approxfield)
            else:
                print ('Awaiting approx_by_Field flag to be True or False, but is '
                       +str(approxfield)+'. Using default value of '+str(self.get_approxfield()))
        
        if len(initial_points) == 0:
            # seed a single centered activation point in the standard plane
            print 'No initial points found. Setting point at 0,0,0'
            if symmetric == True:
                # only in plane
                initial_points.append(Threshold_Point(Threshold_Location(0,0,0)))
            else:
                n_rot = int(np.ceil(360/rot_degree))
                for i in range(n_rot):
                    initial_points.append(Threshold_Point(Threshold_Location(i,0,0)))
        
        for initial_point in initial_points:    
            self.add_point(initial_point)
            self.request_activation_computation(initial_point, params, stimmult, request_queue)
        
        while not self.has_zero_activation_hull():
            data = resultqueue.get()
            if data is None:
                print 'Error during neural activation computation.'
                sys.exit()
            point = data['point']
            self.get_point(point).apply_state(point)
            if point.get_activated() is True:
                rloc = point.get_location().get_as_array()
                rloc_right = Threshold_Location(rloc[0],rloc[1],rloc[2]+1)
                point_right = Threshold_Point(rloc_right)
                if not self.has_point(point_right):
                    self.add_point(point_right)
                    self.request_activation_computation(point_right, params, stimmult, request_queue)
                if self.get_approxfield() == False:
                    rloc_up = Threshold_Location(rloc[0],rloc[1]+1,rloc[2])
                    point_up = Threshold_Point(rloc_up)
                    if not self.has_point(point_up):
                        self.add_point(point_up)
                        self.request_activation_computation(point_up, params, stimmult, request_queue)
                    rloc_down = Threshold_Location(rloc[0],rloc[1]-1,rloc[2])
                    point_down = Threshold_Point(rloc_down)
                    if not self.has_point(point_down):
                        self.add_point(point_down)
                        self.request_activation_computation(point_down, params, stimmult, request_queue)
        
        # terminate workers with poison pill
        for _ in workers:
            request_queue.put(None)
            
        request_queue.join()
        end = pytime.time()
        
        self._runtime += (end-start)
        
    def request_activation_computation(self, point, params, stimmult, request_queue):
        """
        Request the computation of the activation with NEURON for the axon model
        linked in the Threshold point object.
        
        The NEURON workers have to be initialized and been linked to the request_queue.
        
        Parameters
        ----------
        point : Threshold_Point
            The Threshold Point object linking the axon model, the time-dependent
            electric potential and the axon location.
            
        params : dict
            See compute_activation_range for further details on the keys.
            
        stimmult : float
            The stimulation multiplier with which the field values from the
            field model are scaled
        
        request_queue : Queue
            The data queue for the computation requests to the NEURON workers.
                     
        """
        gstep = params['g_step']
        rot_degree = params['rot_degree_step']
        dbs_signal = params['dbs_signal']
        field = params['field']
        fiberD = params['fiberD']
        Z = params['impedance']
        
        loc = point.get_location()
        new_coordinate = np.array([0,0.85e-3+loc.get_normal()*gstep,0+loc.get_tangential()*gstep])
        transform = point.get_transform()
        transform.set_position_vector(new_coordinate)
        RN = self.define_rotation_matrix(loc.get_rotational()*rot_degree)
        transform.set_rotation_matrix(RN)
        
        axon = Axon({'diameter':fiberD})
        nodes = transform.transform_coords(axon.get_nodes())
        point.set_coordinate(nodes[0]/2+nodes[-1]/2)
        point.set_axon(axon)
        point.set_dbs_signal(dbs_signal)
        
        if DBSsignal.TYPE in dbs_signal.params and dbs_signal.params[DBSsignal.TYPE] == 'voltage':
            potential = field.evaluate_quantity(field.get_potential(), nodes)
        else:
            potential = Z*field.evaluate_quantity(field.get_potential(), nodes)
        point.set_potential(potential)
        
        request_queue.put({'point':point,'stimmult':stimmult})
        
    def compute_optimal_activation(self, number_of_workers, stimmult):
        """
        Compute the minimum required stimulation amplitude multiplier for all axon
        models within the volume.
        
        The NEURON workers get started and will be terminated after a
        zero-activation hull is found. 
        
        Parameters
        ----------
        number_of_workers : int
            Number of NEURON workers (processes) that should be statted.
            
        stimmult : float
            The first estimate for the maximum stimulation multiplier used by the
            bisection method.
            
        """
        start = pytime.time()
        request_queue = JoinableQueue()
        resultqueue = Queue()
        workers = self._generate_workers(number_of_workers, request_queue, resultqueue)
        
        points = self.get_points()
        npoints = len(points)
        for point in points:
            request_queue.put({'point':point,
                               'stim_max':stimmult,
                               'mode':'optimal'})
        
        # terminate workers with poison pill
        for _ in workers:
            request_queue.put(None)
                
        request_queue.join()
        
        for _ in xrange(npoints):  
            result = resultqueue.get()
            if result is None:
                print "Error during neural activation computation"
                sys.exit()
            point = result['point']
            self.get_point(point).apply_state(point)
                
        end = pytime.time()
        self._runtime += (end-start)
        
        
    def get_volume_without_zero_hull(self):
        """
        Returns
        ----------
        Threshold_Volume
            Return all points within this volume without the enclosing
            zero-activation hull.
        """ 
        new_volume = Threshold_Volume()
        new_volume.set_state(self.get_state())
        for point in self.get_points():
            if (point.get_activated() is False):
                continue
            new_volume.add_point(point)
            
        return new_volume
    
    def get_volume_without_outer_hull(self):
        """
        Returns
        ----------
        Threshold_Volume
            Return all points within this volume without the enclosing
            outer hull.
        """ 
        new_volume = Threshold_Volume()
        new_volume.set_state(self.get_state())
        for point in self.get_points():
            rloc = point.get_location()
            loc_up = Threshold_Location(rloc.get_rotational(), rloc.get_tangential()+1,
                                        rloc.get_normal())
            loc_down = Threshold_Location(rloc.get_rotational(), rloc.get_tangential()-1,
                                        rloc.get_normal())
            loc_right = Threshold_Location(rloc.get_rotational(), rloc.get_tangential(),
                                        rloc.get_normal()+1)
            if self.get_approxfield() == True:
                if not self.has_point(Threshold_Point(loc_right)):
                    continue
            else:
                if (not self.has_point(Threshold_Point(loc_up)) or
                    not self.has_point(Threshold_Point(loc_down)) or
                    not self.has_point(Threshold_Point(loc_right))):
                    continue
            new_volume.add_point(point)
            
        return new_volume
        
    def get_volume_shell(self, inner_volume):
        """
        Get a volume including all points without the points of the
        inner volume subtracted by its zero-activation hull and resulting outer
        hull.
        
        Details:
        The inner volumes zero activation hull comprises locations, at which
        stim_min was not sufficient to activate the location. This means that
        the shell of the inner volume without the zero activation hull together
        with the zero activation hull comprises a region in which stim_min was
        sufficient to activate a location.
        Since we want stim_max to be included in our region, we have to consider
        the zero activation hull of our outer volume. The shell including
        activations at stim_min as well as no activation at stim_max is then
        given by this approach.
        
        Parameters
        ----------
        inner_volume : Threshold_Volume
            The inner volume, which should be subtracted from this volume.

        Returns
        ----------
        Threshold_Volume
            Return all points within this volume without the inner volume
           
        """
        new_volume = Threshold_Volume()
        new_volume.set_state(self.get_state())
        hinner_volume = inner_volume.get_volume_without_zero_hull().get_volume_without_outer_hull()
        for point in self.get_points():
            # cut the points only if they are inner points of the other volume and
            # one point left in up, down, and right direction
            rloc = point.get_location()
            loc_up = Threshold_Location(rloc.get_rotational(), rloc.get_tangential()+1,
                                        rloc.get_normal())
            loc_down = Threshold_Location(rloc.get_rotational(), rloc.get_tangential()-1,
                                        rloc.get_normal())
            loc_right = Threshold_Location(rloc.get_rotational(), rloc.get_tangential(),
                                        rloc.get_normal()+1)
            if (hinner_volume.has_point(point) and
                self.has_point(Threshold_Point(loc_up)) and
                self.has_point(Threshold_Point(loc_down)) and
                self.has_point(Threshold_Point(loc_right)) 
                ):
                continue
            new_volume.add_point(point)
            
        return new_volume
    
    
    def _transform_coords_to_plane(self, rot_degree, coords, ref_transform):
        plane_coords = coords
        new_transform = ref_transform.create_copy()
        RN = self.define_rotation_matrix(rot_degree)
        new_transform.set_rotation_matrix(RN)
        transformed_coords = new_transform.transform_coords_to_plane(plane_coords)
        return transformed_coords  
        
   
    def get_coords(self):
        """
        Returns
        ----------
        numpy array
            The (minimum) coordinates of axons which would elicit an action
            potential for the given stimulation multiplier.       
        """
        coords = np.zeros((0,3))
        sorted_keys = sorted(self.get_planes())
        if not self.get_symmetric():
            for key in sorted_keys:
                coords = np.concatenate((coords, self.get_planes()[key].get_coords()), axis=0)
        else:
            plane_coords = self.get_planes()[0].get_coords()
            ref_transform = self.get_points()[0].get_transform()
            rot_degree = float(self.get_rotation_step())
            for i in range(int(360/rot_degree)):
                transformed_coords = self._transform_coords_to_plane(i*rot_degree, plane_coords, ref_transform)
                coords = np.concatenate((coords,transformed_coords), axis=0)
        return coords
    
    def get_coord_threshold_matrix(self):
        """
        Returns
        ----------
        numpy array
            A numpy (n,4) array including all coords and the threshold values
            of the threshold points on this line.       
        """
        sorted_keys = sorted(self.get_planes())
        coord_threshs = np.zeros((0,4))
        if not self.get_symmetric():
            for key in sorted_keys:
                coord_threshs = np.concatenate((coord_threshs, self.get_planes()[key].get_coord_threshold_matrix()), axis=0)
        else:
            plane_coords_threshs = self.get_planes()[0].get_coord_threshold_matrix()
            plane_coords = plane_coords_threshs[:,0:3]
            ref_transform = self.get_points()[0].get_transform()
            rot_degree = float(self.get_rotation_step())
            for i in range(int(360/rot_degree)):
                transformed_coords = self._transform_coords_to_plane(i*rot_degree, plane_coords, ref_transform)
                transformed_plane_coords_threshs = np.zeros((plane_coords.shape[0],4))
                transformed_plane_coords_threshs[:,0:3]=transformed_coords[:,0:3]
                transformed_plane_coords_threshs[:,3]=plane_coords_threshs[:,3]
                coord_threshs = np.concatenate((coord_threshs,transformed_plane_coords_threshs), axis=0)
        return coord_threshs
    
    def get_coords_for_optimal_threshold(self, stimmult):
        """
        Returns
        ----------
        numpy array
            The (minimum) coordinates of axons which would elicit an action
            potential for the given stimulation multiplier.       
        """
        sorted_keys = sorted(self.get_planes())
        coords = np.zeros((0,3))
        if not self.get_symmetric():
            for key in sorted_keys:
                hcoords = self.get_planes()[key].get_coords_for_optimal_threshold(stimmult)
                coords = np.concatenate((coords,hcoords), axis=0)
        else:
            plane_coords = self.get_planes()[0].get_coords_for_optimal_threshold(stimmult)
            ref_transform = self.get_points()[0].get_transform()            
            rot_degree = float(self.get_rotation_step())
            for i in range(int(360/rot_degree)):
                transformed_coords = self._transform_coords_to_plane(i*rot_degree, plane_coords, ref_transform)
                coords = np.concatenate((coords,transformed_coords), axis=0) 
        return coords
        
    def get_planes(self):
        """
        Returns
        ----------
        dict
            keys are the rotational indices and values the corresponding
            Threshold_Plane objects.
            
        """
        return self._planes
        
    def define_rotation_matrix(self, alpha_deg):
        """
        Define the rotation matrix for a given rotation degree value.
        
        Parameters
        ----------
        alpha_deg : float
            Rotation degree value in degrees.

        Returns
        ----------
        numpy array
            The (4,4) rotation matrix.
           
        """
        api=float(alpha_deg)/180*np.pi
        RN=np.array([[np.cos(api),-np.sin(api),0,0],
                     [np.sin(api),np.cos(api),0,0],
                     [0,0,1,0],
                     [0,0,0,1]])
        return RN
    
    def store_state(self, path, filename):
        """
        Store the state of the Threshold_Volume object in a file.
        
        Parameters
        ----------
        path : string
            path to the file
        filename : string
            filename without ending
           
        """
        if path.endswith('/'):
            newpath = path
        else:
            newpath = path+'/'
        if not os.path.exists(newpath):
            os.mkdir(newpath)
        with open(newpath+filename+'.pkl', 'wb') as output:
            pickle.dump(self, output)
            
    def set_runtime(self, runtime):
        """
        Set the runtime of the simulation.
        
        Parameters
        ----------
        runtime : float
            runtime of the stimulation in seconds.
           
        """
        self._runtime = runtime
        
    def get_runtime(self):
        """
        Returns
        ----------
        float
            Runtime of the simulation in seconds
            
        """
        return self._runtime
    
    def set_number_of_points_filled(self, number):
        """
        (Experimental for the study evaluation) Number of (theoretical)
        axons if no inner axons would be subtracted.
        
        Parameters
        ----------
        number : int
            Number of axon models.
           
        """
        self._number_of_points_filled = number
        
    def get_number_of_points_filled(self):
        """
        Returns
        ----------
        int
            (Experimental for the study evaluation) Number of (theoretical)
            axons if no inner axons would be subtracted.
            
        """
        return self._number_of_points_filled
    
class ApproxField_Threshold_Volume(Threshold_Volume):
    """
    Represents a collection of Threshold Planes in abstract volume.
    
    This class is the main class for the computation of the volume of 
    tissue activated (VTA) by using field threshold value approximations.
    It uses for that the class structures of threshold planes, lines and points.
    
    Inherits Threshold_Volume
    
    Parameters
    ----------
    neuron_volume : Threshold_Volume
        Containing the threshold distance relationship necessary to compute
        the field thresholdvalue at a distance derived for a given stimulation
        multiplier.
        
    quantity : string
        Identifier of the quantity with which the field threshold computation
        should be carried out. (Default 'potential'). Also possible 'normE' for
        electric field norm, and 'normJ' for current density norm.
    """
    
    def __init__(self, neuron_volume, quantity):
        Threshold_Volume.__init__(self)
        self.neuron_volume = neuron_volume
        if not(quantity == 'potential' or quantity == 'normE' or quantity == 'normJ'):
            raise Exception('Expecting potential, normE, or normJ as quantity for field threshold approximation')
        self.quantity = quantity
        
    def get_field_quantity(self, params):
        """
        Get the field quantity object from the field model based on the
        field quantity identifier in the parameters.
        
        Parameters
        ----------
        params : dict
            'field' : The field model
            
        Returns
        ----------
        Function - FEniCS
            Function for the given FunctionSpace and Mesh in the FEniCS field
            model representing the requested field quantity.
        """
        field = params['field']
        if self.quantity == 'normE':
            fieldquantity = field.get_electric_field_norm()
        elif self.quantity == 'normJ':
            fieldquantity = field.get_current_density_norm()
        else:
            fieldquantity = field.get_potential()
        return fieldquantity
        
    def get_threshold_for_stimmult(self, params, stimmult):
        """
        Get the threshold value for the given stimulation multiplier by using
        the threshold-distance relation.
        
        Parameters
        ----------
        params : dict
            'field' : The field model
        
        stimmult: float
            Stimulation multiplier
            
        Returns
        ----------
        float
            The threshold value for the given field quantity.
        """
        field = params['field']
        fieldquantity = self.get_field_quantity(params)
        planes = self.neuron_volume.get_planes()
        thresholds = np.zeros((len(planes),))
        for idx, plane in enumerate(planes.values()):
            if len(plane.get_lines())>1:
                raise Exception('Expecting only one threshold line in each plane for field threshold approximation')
            line = plane.get_lines().values()[0]
            thresholds[idx] = field.evaluate_quantity(fieldquantity, line.get_coord_for_optimal_threshold(stimmult))
        threshold = np.mean(thresholds)
        return threshold
    
    def get_threshold_for_stimmult_post(self, stimmult):
        """
        Get the threshold value for the given stimulation multiplier by using
        the threshold-distance relation in postprocessing, without requiring the
        full field model.
        
        Parameters
        ----------     
        stimmult: float
            Stimulation multiplier
            
        Returns
        ----------
        float
            The threshold value for the given field quantity.
        """
        planes = self.neuron_volume.get_planes()
        thresholds = np.zeros((len(planes),))
        for idx, plane in enumerate(planes.values()):
            if len(plane.get_lines())>1:
                raise Exception('Expecting only one threshold line in each plane for field threshold approximation')
            line = plane.get_lines().values()[0]
            ct_matrix = line.get_coord_threshold_matrix()
            transform = plane.get_points()[0].get_transform()
            coords_in_std_plane = transform.inverse_transform_coords_to_plane(ct_matrix[:,0:3])
            #distance = np.interp(stimmult, ct_matrix[:,3], coords_in_std_plane[:,1])
            interpolator = InterpolatedUnivariateSpline(ct_matrix[:,3], coords_in_std_plane[:,1], k=1)
            distance = interpolator(stimmult)
            approx_line = self.get_plane(line.get_rotational()).get_line(line.get_tangential())
            approx_ct_matrix = approx_line.get_coord_threshold_matrix()
            coords_in_std_plane = transform.inverse_transform_coords_to_plane(approx_ct_matrix[:,0:3])
            value = np.interp(distance, coords_in_std_plane[:,1], approx_ct_matrix[:,3])
            thresholds[idx] = value
        threshold = np.mean(thresholds)
        return threshold       
        
    def compute_activation_range(self, number_of_workers, params, stimmult):
        """
        Determines all axon locations which get activated by the given field
        distribution and stimulation multiplier, and given initial seed
        points.
        
        Only the field model gets evaluated, therefore no NEURON workers are required.
        
        Parameters
        ----------
        number_of_workers : int
            Arbitrary, e.g. 1
            
        stimmult : float
            The stimulation multiplier
            
        params : dict
            'fiberD' : float
                axon fiber diameter in um. (Default 5.7)
            'g_step' : float
                spatial stepping in normal and tangential direction in m. (Default 0.5)
            'rot_degree_step' : float
                rotation degree stepping in degree. (Default 10.0)
            'symmetric' : boolean
                Rotational symmetry flag. (Default False)
            'approx_by_field' : boolean
                Field Threshold Apporoximation flag. (Default False)
                
            'dbs_signal' : DBSsignal
                The DBSsignal.
            'field' : AbstractFenicsFieldModel
                The Field model.
            'Z' : float
                The field model impedance.
                                    
        """
        start = pytime.time()
        fieldquantity = self.get_field_quantity(params)
        threshold = self.get_threshold_for_stimmult(params, stimmult)        
        initial_points = self.get_initial_points()
        if 'rot_degree_step' in params:
            rot_degree = params['rot_degree_step']
            if rot_degree > 0 and rot_degree < 180:
                self.set_rotation_step(rot_degree)
            else:
                print ('Rotation degree step of '+str(rot_degree)
                       +' not applicable. Using default rotation degree step of '
                       +str(self.get_rotation_step())+'.')
        if 'symmetric' in params:
            symmetric = params['symmetric']
            if symmetric == True or symmetric == False:
                self.set_symmetric(symmetric)
            else:
                print ('Awaiting symmetric flag to be True or False, but is '
                       +str(symmetric)+'. Using default value of '+str(self.get_symmetric()))
        
        if len(initial_points) == 0:
            # seed a single centered activation point in the standard plane
            print 'No initial points found. Setting point at 0,0,0'
            if symmetric == True:
                # only in plane
                initial_points.append(Threshold_Point(Threshold_Location(0,0,0)))
            else:
                n_rot = int(np.ceil(360/rot_degree))
                for i in range(n_rot):
                    initial_points.append(Threshold_Point(Threshold_Location(i,0,0)))
        
        point_stack = []
        for initial_point in initial_points:   
            self.add_point(initial_point)
            point_stack.append(self.request_activation_computation(initial_point, params, fieldquantity, threshold))
        
        while not self.has_zero_activation_hull() or not len(point_stack) == 0:
            point = point_stack[-1]
            point_stack.remove(point)
            self.get_point(point).apply_state(point)
            if point.get_activated() is True:
                rloc = point.get_location().get_as_array()
                rloc_right = Threshold_Location(rloc[0],rloc[1],rloc[2]+1)
                point_right = Threshold_Point(rloc_right)
                if not self.has_point(point_right):
                    self.add_point(point_right)
                    point_stack.append(self.request_activation_computation(point_right, params, fieldquantity, threshold))
                rloc_up = Threshold_Location(rloc[0],rloc[1]+1,rloc[2])
                point_up = Threshold_Point(rloc_up)
                if not self.has_point(point_up):
                    self.add_point(point_up)
                    point_stack.append(self.request_activation_computation(point_up, params, fieldquantity, threshold))
                rloc_down = Threshold_Location(rloc[0],rloc[1]-1,rloc[2])
                point_down = Threshold_Point(rloc_down)
                if not self.has_point(point_down):
                    self.add_point(point_down)
                    point_stack.append(self.request_activation_computation(point_down, params, fieldquantity, threshold))
        
        end = pytime.time()
        
        self._runtime += (end-start)
        
    def request_activation_computation(self, point, params, fieldquantity, threshold):
        """
        Request the computation of the activation using the threshold-distance
        relation and the field model.
        
        Parameters
        ----------
        point : Threshold_Point
            The Threshold Point object linking the axon model, the time-dependent
            electric potential and the axon location.
            
        params : dict
            See compute_activation_range for further details on the keys.
            
        fieldquantity : Function - FEniCS
            The field quantity from which the VTA should be approximated
            
        threshold : float
            The field threshold value from which the isovolume to approximate
            the VTA should be determined
            
        """
        gstep = params['g_step']
        rot_degree = params['rot_degree_step']
        dbs_signal = params['dbs_signal']
        field = params['field']
        fiberD = params['fiberD']
        
        loc = point.get_location()
        new_coordinate = np.array([0,0.85e-3+loc.get_normal()*gstep,0+loc.get_tangential()*gstep])
        transform = point.get_transform()
        transform.set_position_vector(new_coordinate)
        RN = self.define_rotation_matrix(loc.get_rotational()*rot_degree)
        transform.set_rotation_matrix(RN)
        
        axon = Axon({'diameter':fiberD})
        nodes = transform.transform_coords(axon.get_nodes())
        point.set_coordinate(nodes[0]/2+nodes[-1]/2)
        point.set_axon(axon)
        point.set_dbs_signal(dbs_signal)
        
        value = field.evaluate_quantity(fieldquantity, point.get_coordinate())
        point.set_threshold(value)
        point.set_activated((value>threshold).item())
        print point
        return point
    
    def compute_optimal_activation(self, number_of_workers, stimmult):
        """ Do nothing here, because already done in request_activation_computation """
        return
    
    def get_coords_for_optimal_threshold(self, stimmult):
        """
        Returns
        ----------
        numpy array
            The (minimum) coordinates of axons which would elicit an action
            potential for the given stimulation multiplier.       
        """
        threshold = self.get_threshold_for_stimmult_post(stimmult)
        sorted_keys = sorted(self.get_planes())
        coords = np.zeros((0,3))
        if not self.get_symmetric():
            for key in sorted_keys:
                hcoords = self.get_planes()[key].get_coords_for_optimal_threshold(threshold)
                coords = np.concatenate((coords,hcoords), axis=0)
        else:
            plane_coords = self.get_planes()[0].get_coords_for_optimal_threshold(threshold)
            ref_transform = self.get_points()[0].get_transform()            
            rot_degree = float(self.get_rotation_step())
            for i in range(int(360/rot_degree)):
                transformed_coords = self._transform_coords_to_plane(i*rot_degree, plane_coords, ref_transform)
                coords = np.concatenate((coords,transformed_coords), axis=0) 
        return coords
   
def load_state(path, filename):
    """
    Load the state of the Threeshold_Volume from a file
    
    Parameters
    ----------
    path : string
        Path to the file
        
    filename : string
        Filename of the file without ending (should be .pkl file)

    Returns
    ----------
    Threshold_Volume
        The threshold volume with the loaded state.
        
    """
    
    if path.endswith('/'):
        newpath = path
    else:
        newpath = path+'/'
    with open(newpath+filename+'.pkl', 'rb') as fin:
        vta = pickle.load(fin)
    return vta
    
def compute_vta(number_of_workers, params):
    """
    Compute the volume of tissue activated for a given field model.
    
    To compute the VTA with the fast "threshold-distance relation"-approach,
    the parameter 'approx_by_field' has to be True.
    
    Parameters
    ----------
    number_of_workers : int 
        Number of NEURON workers (processes) that should be used.
        
    params : dict
        'fiberD' : float
            axon fiber diameter in um. (Default 5.7)
        'g_step' : float
            spatial stepping in normal and tangential direction in m. (Default 0.5)
        'rot_degree_step' : float
            rotation degree stepping in degree. (Default 10.0)
        'symmetric' : boolean
            Rotational symmetry flag. (Default False)
        'approx_by_field' : boolean
            Field Threshold Apporoximation flag. (Default False)
            
        'dbs_signal' : DBSsignal
            The DBSsignal.
        'field' : AbstractFenicsFieldModel
            The Field model.
        'Z' : float
            The field model impedance.
        

    Returns
    ----------
    Threshold_Volume
        The threshold volume (VTA) object.
        
    """
    required_keys = ['g_step','dbs_signal','field',
                     'impedance','stim_min','stim_max']
    missing_keys = []
    for key in required_keys:
        if key not in params:
            missing_keys.append(key)
    
    if len(missing_keys) > 0:
        raise Exception('Missing params for VTA computation: '+str(missing_keys))
    
    if 'approx_by_field' in params and params['approx_by_field'] == True:
        approx_field_case = True
    else:
        approx_field_case = False
    
    # Compute activation range for stim_min
    inner_volume = Threshold_Volume()
    inner_volume.compute_activation_range(number_of_workers, params,
                                          params['stim_min'])
    # Add points to new volume with zero activation hull as new initial points
    outer_volume = Threshold_Volume()
    for point in inner_volume.get_points():
        # If point is not activated, add a new initial point
        if point.get_activated() is False:
            new_point = Threshold_Point(point.get_location())
        else:
            new_point = point
        outer_volume.add_point(new_point)
    # Compute activation range for stim_max
    outer_volume.compute_activation_range(number_of_workers, params,
                                          params['stim_max'])
    
    number_find_activation_range = outer_volume.get_number_of_runs()
    # get the shell for the stim_min stim_max range
    volume_shell = outer_volume.get_volume_shell(inner_volume)
    number_before_optimization = volume_shell.get_number_of_runs()
    
    # perform accurate computation of the minimal stimulation amplitude at 
    # each location in the shell
    volume_shell.compute_optimal_activation(number_of_workers, params['stim_max'])
    
    print 'Already carried out number of computations:', number_find_activation_range
    print 'All points of outer volume:',len(outer_volume.get_points())
    print 'Points in shell to be computed:',len(volume_shell.get_points())

    print 'Number of optimization runs:', volume_shell.get_number_of_runs() - number_before_optimization
    
    volume_shell.set_runtime(volume_shell.get_runtime()
                             +outer_volume.get_runtime()
                             +inner_volume.get_runtime())
    volume_shell.set_number_of_points_filled(len(outer_volume.get_points()))
    
    if approx_field_case:
        if 'approx_by_field_quantity' in params:
            approx_by_field_quantity = params['approx_by_field_quantity']
        else:
            approx_by_field_quantity = 'potential'
        approx_volume = ApproxField_Threshold_Volume(volume_shell, approx_by_field_quantity)
        approx_volume.compute_activation_range(number_of_workers, params, params['stim_max'])
        approx_volume.set_runtime(volume_shell.get_runtime()+approx_volume.get_runtime())
        return approx_volume
    else:
        return volume_shell
