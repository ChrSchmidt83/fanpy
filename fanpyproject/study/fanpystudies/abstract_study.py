'''
Created on Mar 29, 2017

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPyStudy
'''

import os
import multiprocessing

class FanPyPaperStudy():
    
    def __init__(self, params):
        debug_suffix = '' if (not 'debug' in params) else '_debug/'
        tmpdir = 'tmpdata' if (not 'tmpdir' in params) else params['tmpdir']
        if tmpdir.endswith('/'):
            tmpdir = tmpdir[:-1]+debug_suffix
        else:
            tmpdir = tmpdir+debug_suffix
        srcdir = 'sources' if (not 'srcdir' in params) else params['srcdir']
        figdir = 'results' if (not 'figdir' in params) else params['figdir']
        if figdir.endswith('/'):
            figdir = figdir[:-1]+debug_suffix
        else:
            figdir = figdir+debug_suffix
        self._debug = bool(False if (not 'debug' in params) else params['debug'])
        self._ncpus = multiprocessing.cpu_count() if (not 'ncpus' in params) else int(params['ncpus'])
        
        if not os.path.exists(tmpdir):
            os.mkdir(tmpdir)
            
        if not os.path.exists(figdir):
            os.mkdir(figdir)
        
        self._tmpdir = os.path.abspath(tmpdir)+'/'
        self._srcdir = os.path.abspath(srcdir)+'/'
        self._figdir = os.path.abspath(figdir)+'/'
            
    