'''
Created on Mar 29, 2017

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPyStudy
'''
import numpy as np
from abstract_study import FanPyPaperStudy
import matplotlib.pyplot as plt
import os
from fanpy.fieldmodels.fenics.analytic_example_model import AnalyticFieldModel

class AnalyticValidationModel(FanPyPaperStudy):
    
    def __init__(self, params):   
        FanPyPaperStudy.__init__(self, params)
        self._tmpdir = self._tmpdir + 'analytic_validation/'
        if not os.path.exists(self._tmpdir):
            os.mkdir(self._tmpdir)
        self._figdir = self._figdir + 'analytic_validation/'
        if not os.path.exists(self._figdir):
            os.mkdir(self._figdir)
        self.fieldmodelname_sf = 'fieldmodel_sf.hdf5'
        self.fieldmodelname_eqs = 'fieldmodel_eqs.hdf5'
    
    def postprocessing(self):
        
        equations = []
        equations.append({
            'ident': 'sf',
            'name': 'Stationary Field',
            'modelname':self.fieldmodelname_sf})
        equations.append({
            'ident': 'eqs',
            'name': 'Quasistatic Field',
            'modelname':self.fieldmodelname_eqs})
        
        quantities = []
        quantities.append({
            'ident': 'pot',
            'name': 'Potential',
            'variable': '$\varphi$',
            'unit': 'V',
            'color': 'g'
            })
        quantities.append({
            'ident': 'normE',
            'name': 'Electric field norm',
            'variable': '$E$',
            'unit': 'V/m',
            'color': 'b'
            })
        
        
        plt.rcParams["font.family"] = "serif"
        points = np.outer(np.linspace(-0.03,0.03,100),np.array([1,0,0]))
        points_marker = np.outer(np.linspace(-0.03,0.03,22),np.array([1,0,0])) # 13 or 25 points
        
        for equation in equations:
            plt.figure(figsize=(8, 4), dpi=100)
            fieldmodel = AnalyticFieldModel()
            fieldmodel.load_model_state(self._tmpdir, equation['modelname'])
            
            for idx, quantity in enumerate(quantities):
                if quantity['ident']=='pot':
                    fieldmodel.export_quantity(fieldmodel.get_potential(), self._tmpdir+'exported_'+quantity['ident']+'_'+equation['ident'])
                    fenics_sol = fieldmodel.evaluate_quantity(fieldmodel.get_potential(), points)
                    fenics_sol_marker = fieldmodel.evaluate_quantity(fieldmodel.get_potential(), points_marker)
                    analytic_sol = fieldmodel.get_exact_analytic_potential(points)
                    analytic_sol_marker = fieldmodel.get_exact_analytic_potential(points_marker)
                elif quantity['ident']=='normE':
                    fieldmodel.export_quantity(fieldmodel.get_electric_field_norm(), self._tmpdir+'exported_'+quantity['ident']+'_'+equation['ident'])
                    fenics_sol = fieldmodel.evaluate_quantity(fieldmodel.get_electric_field_norm(), points)    
                    fenics_sol_marker = fieldmodel.evaluate_quantity(fieldmodel.get_electric_field_norm(), points_marker)      
                    analytic_sol = fieldmodel.get_exact_analytic_fieldnorm(points) 
                    analytic_sol_marker = fieldmodel.get_exact_analytic_fieldnorm(points_marker)
                    
                if equation['ident'] == 'eqs':
                    fenics_sol = np.sign(fenics_sol.real)*np.sqrt(pow(fenics_sol.real,2)+pow(fenics_sol.imag,2))                
                    fenics_sol_marker = np.sign(fenics_sol_marker.real)*np.sqrt(pow(fenics_sol_marker.real,2)+pow(fenics_sol_marker.imag,2))
                    analytic_sol = np.sign(analytic_sol.real)*np.sqrt(pow(analytic_sol.real,2)+pow(analytic_sol.imag,2))
                    analytic_sol_marker = np.sign(analytic_sol_marker.real)*np.sqrt(pow(analytic_sol_marker.real,2)+pow(analytic_sol_marker.imag,2))
                
                ax = plt.subplot(2,2,3+idx)
                deviation = np.linalg.norm(fenics_sol-analytic_sol)/np.linalg.norm(analytic_sol)
                plt.plot(points[:,0], analytic_sol, '-k', label='Analytic')
                plt.plot(points[:,0], fenics_sol, '--'+quantity['color'], label='FEniCS')
                plt.plot(points_marker[:,0], analytic_sol_marker, 'ok', mfc='None')
                plt.plot(points_marker[:,0], fenics_sol_marker, 'x'+quantity['color'])
                print equation['name']+' '+quantity['name']+' deviation: '+str(deviation)
                ax.set_xlim([points[0,0]*1.1,points[-1,0]*1.1])
                ax.set_xticklabels(["%d" % int(x) for x in ax.get_xticks()*1e2])    
                plt.legend(loc=2, ncol=2)
                plt.xlabel('Location [cm]')
                plt.ylabel(quantity['name']+' ['+quantity['unit']+']')
                plt.title(equation['name'])
                
                plt.savefig(self._figdir+'fenics_validation_'+equation['ident']+'.svg', bbox_inches="tight")
        
    def run(self):
        
        fieldparams = {
            'dielectric_properties': {
                'inner_material': {'conductivity': 2.0, 'permittivity': 120},
                'shell_material': {'conductivity': 0.1, 'permittivity': 2e6},
                'outer_material': {'conductivity': 1.0, 'permittivity': 80}
                },
            'frequency': 3.5e4
            }
        
        fieldmodel_sf = AnalyticFieldModel()
        if self._debug:
            fieldmodel_sf.set_coarse_mesh(2)
        else:
            fieldmodel_sf.set_basis_order(2)
            
        fieldmodel_sf.init()
        
        fieldmodel_sf.run(fieldparams)
        
        fieldmodel_sf.store_model_state(self._tmpdir, self.fieldmodelname_sf)
        fieldmodel_sf.export_quantity(fieldmodel_sf.get_potential(), self._tmpdir+'exported_potential_sf')
        
        fieldmodel_eqs = AnalyticFieldModel()
        fieldmodel_eqs.set_quasistatic(True)
        if self._debug:
            fieldmodel_eqs.set_coarse_mesh(2)
        else:
            fieldmodel_eqs.set_basis_order(2)
            
        fieldmodel_eqs.init()
        
        fieldmodel_eqs.run(fieldparams)
        
        fieldmodel_eqs.store_model_state(self._tmpdir, self.fieldmodelname_eqs)
        