'''
Created on Mar 29, 2017

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPyStudy
'''
import numpy as np
from abstract_study import FanPyPaperStudy
import matplotlib.pyplot as plt
import os
from fanpy.fieldmodels.fenics.dbs_field_models import VolumeConductorDBSElectrodeWithSTNInMotorRegion
from fanpy.core.dbssignal import DBSsignal
from fanpy.neuronmodels.mam_axon import vtautil
from fanpy.core.tissue_dielectrics import DielectricProperties as DielP, ETissue
from scipy.spatial import ConvexHull
from fanpy.neuronmodels.mam_axon.vtautil import Threshold_Volume, Threshold_Point,\
    Threshold_Location, ApproxField_Threshold_Volume
from fanpy.core.axon import Axon
from pyevtk.hl import pointsToVTK

class ApproximateVTAByFieldThresholds(FanPyPaperStudy):
    
    def __init__(self, params, fiberD):   
        FanPyPaperStudy.__init__(self, params)
        self.fiberD = fiberD
        fiberDstr = str(fiberD).replace('.','_')
        self._tmpdir = self._tmpdir + 'approx_vta_by_field_thresholds'+fiberDstr+'/'
        if not os.path.exists(self._tmpdir):
            os.mkdir(self._tmpdir)
        self._figdir = self._figdir + 'approx_vta_by_field_thresholds'+fiberDstr+'/'
        if not os.path.exists(self._figdir):
            os.mkdir(self._figdir)
        self.fieldmodelname = 'fieldmodel.hdf5'
        self.axonsname = 'axon_data'
        
        """ material properties """
        wmatter = DielP(ETissue.BRAIN_WHITE_MATTER)
        gmatter = DielP(ETissue.BRAIN_GREY_MATTER)
        csf = DielP(ETissue.CEREBRO_SPINAL_FLUID)

        frequency = 2e3
        wmatter_diel = wmatter.get_dielectrics(frequency)
        gmatter_diel = gmatter.get_dielectrics(frequency)
        csf_diel = csf.get_dielectrics(frequency)
        
        """ dbs signal """
        if self._debug:
            npulses = 1
            dt = 1e-5
            freq = 1300
        else:
            npulses = 10 # ToDo: has to be 10
            dt = 5e-6 # ToDo. has to be 5e-6
            freq = 130
                    
        signal_params = {
        DBSsignal.FREQUENCY: freq,
        DBSsignal.AMPLITUDE: -1e-3, # 1 mA
        DBSsignal.DT: dt,
        DBSsignal.NUMBER_OF_PULSES: npulses,
        DBSsignal.PULSE_DURATION: 6e-5}
        dbs_signal = DBSsignal(signal_params) 
        
        """ study cases """
        if self._debug:
            rot_degree_step = 45
            g_step = 0.5e-3
            symmetric = True
        else:
            rot_degree_step = 10
            g_step = 0.5e-3
            symmetric = False
            
        self.cases = []
        self.cases.append({
                'name': 'homogeneous',
                'pltname': 'Model 1',
                'color': 'g',
                'marker': 'x',
                'fieldparams': {
                    'dielectric_properties': {
                        'brain': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()},
                        'encapsulation': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()},
                        'stn': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()}
                            },
                        },
                'neuronparams': {
                    'g_step': g_step,
                    'rot_degree_step': rot_degree_step,
                    'dbs_signal': dbs_signal,
                    'fiberD': fiberD,
                    #'stim_min': 0.5,
                    #'stim_max': 3.0,
                    'symmetric': True,
                    }
                })
        self.cases.append({
                'name': 'high_encap',
                'pltname': 'Model 2',
                'color': 'b',
                'marker': 'o',
                'fieldparams': {
                    'dielectric_properties': {
                        'brain': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()},
                        'encapsulation': {'conductivity': csf_diel[DielP.CONDUCTIVITY].item()},
                        'stn': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()}
                            },
                        },
                'neuronparams': {
                    'g_step': g_step,
                    'rot_degree_step': rot_degree_step,
                    'dbs_signal': dbs_signal,
                    'fiberD': fiberD,
                    #'stim_min': 0.5,
                    #'stim_max': 3.0,
                    'symmetric': True,
                    }
                })
        self.cases.append({
                'name': 'low_encap',
                'pltname': 'Model 3',
                'color': 'r',
                'marker': 'd',
                'fieldparams': {
                    'dielectric_properties': {
                        'brain': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()},
                        'encapsulation': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()/2.},
                        'stn': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()}
                            },
                        },
                'neuronparams': {
                    'g_step': g_step,
                    'rot_degree_step': rot_degree_step,
                    'dbs_signal': dbs_signal,
                    'fiberD': fiberD,
                    #'stim_min': 0.5,
                    #'stim_max': 3.0,
                    'symmetric': True,
                    }
                })
        self.cases.append({
                'name': 'low_encap_with_stn',
                'pltname': 'Model 4',
                'color': 'k',
                'marker': 's',
                'fieldparams': {
                    'dielectric_properties': {
                        'brain': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()},
                        'encapsulation': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()/2.},
                        'stn': {'conductivity': gmatter_diel[DielP.CONDUCTIVITY].item()}
                            },
                        },
                'neuronparams': {
                    'g_step': g_step,
                    'rot_degree_step': rot_degree_step,
                    'dbs_signal': dbs_signal,
                    'fiberD': fiberD,
                    #'stim_min': 0.5,
                    #'stim_max': 3.0,
                    'symmetric': symmetric,
                    }
                })
    
    def postprocessing(self):
        plt.rcParams["font.family"] = "serif"
        
        self.quantities = []    
        self.quantities.append({
            'name': 'potential',
                'color': 'g',
                'marker': 'o',
                'line': '-'
                        })
        self.quantities.append({
            'name': 'normE',
                'color': 'b',
                'marker': 'x',
                'line': '-'
            })
        
        """ load the data from the study cases """
        models = self._postprocess_cases()
                
        """ plot the data for each case """
        plt.figure(figsize=(18, 2), dpi=100)
        nplots = len(self.cases)
        axs = []
        thresh_min = 1
        thresh_max = 1
        for idx, case in enumerate(self.cases):
            model = models[case['name']]
            fieldmodel = model['fieldmodel']
            vta = model['vtamodel']
            name = case['name']
            report = name+' ('+case['pltname']+') fiberD='+str(+self.fiberD)+'\n'
            unit_stim_amp = case['neuronparams']['dbs_signal'].params[DBSsignal.AMPLITUDE]
            ax = plt.subplot(1,nplots,idx+1)
            plt.title(case['pltname']+' ($f_d$='+str(self.fiberD)+' $\\mu m$)')
            if len(axs)==0:
                plt.ylabel('Normalized\nActivation Threshold')
            plt.xlabel('Stimulation Amplitude [mA]')
            axs.append(ax)
            stimmultipliers = model['stimmultipliers']
            stim_amps = stimmultipliers*unit_stim_amp
            vtavolumes = model['vtavolumes']
            for quantity in self.quantities:
                quantity_name = quantity['name']
                norm_data = model[quantity_name]['normalized']
                mean_data = model[quantity_name]['mean']
                fit = model[quantity_name]['polyfit']
                norm_data_min = np.min(norm_data)
                norm_data_max = np.max(norm_data)
                fit_data_min = np.min(fit(stim_amps))
                fit_data_max = np.max(fit(stim_amps))
                if norm_data_min > fit_data_min:
                    norm_data_min = fit_data_min
                if norm_data_max < fit_data_max:
                    norm_data_max = fit_data_max
                if thresh_min > norm_data_min:
                    thresh_min = norm_data_min
                if thresh_max < norm_data_max:
                    thresh_max = norm_data_max
                ax.set_ylim([thresh_min*0.9, thresh_max*1.1])
                plt.plot(stim_amps, norm_data, quantity['marker']+quantity['color'])#, markerfacecolor='None', markersize=10)
                lbl = '$\\varphi$' if quantity_name == 'potential' else '$E$'
                plt.plot(stim_amps, fit(stim_amps), quantity['line']+quantity['color'], label=lbl)
                report += 'max_increase '+quantity_name+' = '+str(fit_data_max) +', min_increase '+quantity_name+' = '+str(fit_data_min)+'\n'
                report += ('initial absolute value '+quantity_name+' = '
                       +str(mean_data[0])
                       + (' V' if quantity_name == 'potential' else ' V/m')
                       + ' for stimmult='+str(stimmultipliers[0])+'\n')
                vtafieldvolumes = model[quantity['name']]['vtavolumes']
                report += 'VTA field volume sizes ('+quantity_name+') = '+str(vtafieldvolumes)+'\n'
                vtafieldvolumes_constant = model[quantity['name']]['vtavolumes_constant']
                report += 'VTA field volume sizes with constant field threshold value ('+quantity_name+') = '+str(vtafieldvolumes_constant)+'\n'
                vtadevnorm = np.linalg.norm(vtafieldvolumes-vtavolumes)/np.linalg.norm(vtavolumes)*100
                report += 'Deviation VTA volumes sizes ('+quantity_name+') DEV NORM = '+str(np.max(vtadevnorm))+'%\n'
                vtadev = np.abs(vtafieldvolumes-vtavolumes)/np.abs(vtavolumes)*100
                report += 'Deviation VTA volumes sizes ('+quantity_name+') DEV MIN = '+str(np.min(vtadev))+'%\n'
                report += 'Deviation VTA volumes sizes ('+quantity_name+') DEV MEAN = '+str(np.mean(vtadev))+'%\n'
                report += 'Deviation VTA volumes sizes ('+quantity_name+') DEV MAX = '+str(np.max(vtadev))+'%\n'
                report += 'Deviation VTA volumes sizes ('+quantity_name+') DEV STD = '+str(np.std(vtadev))+'%\n'
                vtadevnorm_constant = np.linalg.norm(vtafieldvolumes_constant-vtavolumes)/np.linalg.norm(vtavolumes)*100
                report += 'Deviation VTA volumes sizes with constant field threshold value ('+quantity_name+') DEV NORM = '+str(np.max(vtadevnorm_constant))+'%\n'
                vtadev_constant = np.abs(vtafieldvolumes_constant-vtavolumes)/np.abs(vtavolumes)*100
                report += 'Deviation VTA volumes sizes with constant field threshold value ('+quantity_name+') DEV MIN = '+str(np.min(vtadev_constant))+'%\n'
                report += 'Deviation VTA volumes sizes with constant field threshold value ('+quantity_name+') DEV MEAN = '+str(np.mean(vtadev_constant))+'%\n'
                report += 'Deviation VTA volumes sizes with constant field threshold value ('+quantity_name+') DEV MAX = '+str(np.max(vtadev_constant))+'%\n'
                report += 'Deviation VTA volumes sizes with constant field threshold value ('+quantity_name+') DEV STD = '+str(np.std(vtadev_constant))+'%\n'
            plt.legend(loc=2)
            xmin = np.floor(stim_amps[0]*2/unit_stim_amp)/float(2/unit_stim_amp)
            xmax = np.ceil(stim_amps[-1]*2/unit_stim_amp)/float(2/unit_stim_amp)
            xsteps = float(xmax-xmin)/5
            xticks = np.arange(xmin,xmax,xsteps)
            if abs(xticks[-1])<abs(xmax):
                xticks = np.concatenate((xticks, [xmax]), axis=0)
            if abs(abs(xticks[-1])-abs(stim_amps[-1]))<abs(stim_amps[-1]-stim_amps[-2])/2.:
                xticks = np.concatenate((xticks, [xmax+xsteps]), axis=0)
            ax.set_xticks(xticks)
            ax.set_xticklabels(["%.1f" % stim for stim in ax.get_xticks()*1e3])    
            ax.set_xlim([xticks[0], xticks[-1]]) 
            
            report += 'VTA volume sizes = '+str(vtavolumes)+'\n'
            report += 'Stimulus range = '+str(stim_amps[0]*1e3)+','+str(stim_amps[-1]*1e3)+' mA'+'\n'
            report += 'Model impedance = '+str(fieldmodel.get_impedance())+' Ohm'+'\n'
            
            vtacoords_min = vta.get_coords_for_optimal_threshold(stimmultipliers[0])
            vtacoords_max = vta.get_coords_for_optimal_threshold(stimmultipliers[-1])
            for quantity in self.quantities:
                unit = 'V/m' if quantity['name'] == 'normE' else 'V'
                report += ('Corresponding '+quantity['name']+' value VTA_MIN (stimmult='+str(stimmultipliers[0])+'): '
                           +str(model[quantity['name']]['mean'][0]) + unit
                       +' for unit amplitude: '+str(model[quantity['name']]['vta_unit_threshs'][0]) 
                       + unit+'\n')
                report += ('Corresponding '+quantity['name']+' value VTA_MAX (stimmult='+str(stimmultipliers[-1])+'): '
                           +str(model[quantity['name']]['mean'][-1]) + unit
                       +' for unit amplitude: '+str(model[quantity['name']]['vta_unit_threshs'][-1]) 
                       + unit+'\n')
                
            pointsToVTK(self._tmpdir+name+'/visualvta_min.pvd', vtacoords_min[:,0], vtacoords_min[:,1], vtacoords_min[:,2], data={"vta":np.ones(vtacoords_min.shape[0],)})
            pointsToVTK(self._tmpdir+name+'/visualvta_max.pvd', vtacoords_max[:,0], vtacoords_max[:,1], vtacoords_max[:,2], data={"vta":np.ones(vtacoords_max.shape[0],)})
            fieldmodel.export_quantity(fieldmodel.get_potential(), self._tmpdir+name+'/electric_potential')
            fieldmodel.export_quantity(fieldmodel.get_electric_field_norm(), self._tmpdir+name+'/electric_field')
                
            vtapoints = vta.get_points()
            nruns = vta.get_number_of_runs()
            points_filled = vta.get_number_of_points_filled()
            report += 'Number of Axons = '+str(len(vtapoints))+'\n'
            report += 'Number of Pre-Opt Runs = '+str(points_filled)+'\n'
            report += 'Number of Runs = '+str(nruns)+'\n'
            noptiter = (nruns-len(vtapoints))/float(len(vtapoints))
            report += 'Number of Iterations per Optimization = '+str(noptiter)+'\n'
            report += 'Runtime VTA = '+str(vta.get_runtime())+'\n'
            report += 'Number of Processes = '+str(self._ncpus)+'\n'
                       
            maxtangential = None 
            mintangential = None
            maxnormal = None
            for point in vta.get_points():
                tangential = point.get_location().get_tangential()
                normal = point.get_location().get_normal()
                if maxtangential == None or maxtangential < tangential:
                    maxtangential = tangential
                if mintangential == None or mintangential > tangential:
                    mintangential = tangential
                if maxnormal == None or maxnormal < normal:
                    maxnormal = normal
            
            theoruns_with_interior = points_filled*noptiter+points_filled
            theopoints = (maxtangential-mintangential+1)*(maxnormal+1)*len(vta.get_planes())
            theoruns = round(theopoints*len(vta.get_planes())*noptiter)
            report += 'Theoretical required grid size: ['+str(mintangential)+','+str(maxtangential)+']x[0,'+str(maxnormal)+'] '+str(theopoints)+' points'+'\n'
            report += 'Theoretical required runs = '+str(theoruns)+'\n'
            theoruntime = (vta.get_runtime()*theoruns)/float(nruns)
            report += 'Theoretical runtime = '+str(theoruntime)+'\n'   
            report += 'Speed up = '+str(theopoints*noptiter/(len(vtapoints)*noptiter+points_filled))+'\n'
            report += 'Spped up by removing interior points = '+str(theoruns_with_interior/nruns)+'\n'     
            
            
            report += 'Runtime Fieldsolution = '+str(fieldmodel.get_runtime())+'\n'
            with open(self._figdir+'study_report_'+name+'.txt','w') as reportfile:
                reportfile.write(report)
            print report
        for ax in axs:
            ax.set_ylim([thresh_min*0.9, thresh_max*1.1])
            
        plt.savefig(self._figdir+'vta_by_field_threshold_approximation_by_cases.png', bbox_inches="tight")
        plt.savefig(self._figdir+'vta_by_field_threshold_approximation_by_cases.svg', bbox_inches="tight")
        
        def autolabel(ax, rects):
            for rect in rects:
                if rect.get_y() < 0:
                    height = rect.get_y()
                else:
                    height = rect.get_height()
                scale = 1.05 if height > 0 else 1.05
                ax.text(rect.get_x()+rect.get_width()/2.,scale*height, '%.1f' % height, ha='center', va='bottom')        
        
        if self.fiberD == 5.7:
            """ Impedance plot """
            plt.figure(figsize=(10, 3), dpi=100)
            ax = plt.subplot(111)
            ind = np.arange(len(models))+1
            modelnames = []
            impedance = np.zeros((len(models),))
            for idx, case in enumerate(self.cases):
                model = models[case['name']]
                impedance[idx] = model['fieldmodel'].get_impedance()
                modelnames.append(model['pltname'])
            width=0.7
            rects = plt.bar(ind, impedance, width)
            autolabel(ax, rects)
            plt.ylabel('Impedance [$\\Omega$]')
            plt.xticks(ind+width/2.)
            plt.axes().set_xlim([ind[0]-width/2.,ind[-1]+width*3/2.])
            plt.axes().set_xticklabels(modelnames)
            plt.axes().set_ylim([0, np.max(impedance)*1.2])
            
            plt.savefig(self._figdir+'impedance_for_models.svg', bbox_inches="tight")
            
        """ Field Approximation Plot for Min and Max Stim Range """
        fig = plt.figure(figsize=(10, 3), dpi=100)
        ax = fig.add_subplot(111)
        ind = np.arange(len(models))+1
        modelnames = []
        threshEmin = np.zeros((len(models),))
        threshEmax = np.zeros((len(models),))
        for idx, case in enumerate(self.cases):
            model = models[case['name']]
            threshEmin[idx] = model['normE']['mean'][0]
            threshEmax[idx] = model['normE']['mean'][-1]
            modelnames.append(model['pltname'])
        width=0.35
        p1 = ax.bar(ind, threshEmin, width, color='b')
        autolabel(ax, p1)
        p2 = ax.bar(ind+width, threshEmax, width, color='r')
        autolabel(ax, p2)
        plt.ylabel('Electric field norm\nthreshold [V/m]')
        plt.xticks(ind+width)
        plt.axes().set_xlim([ind[0]-width/2.,ind[-1]+width*5/2.])
        if unit_stim_amp < 0:
            plt.axes().set_ylim([np.min(np.concatenate((threshEmin, threshEmax), axis=0))*1.2, 0])
        else:
            plt.axes().set_ylim([0, np.max(np.concatenate((threshEmin, threshEmax), axis=0))*1.2])
        plt.axes().set_xticklabels(modelnames)
        if unit_stim_amp < 0:
            plt.gca().invert_yaxis()
        
        plt.savefig(self._figdir+'min_and_max_normE_approx_barchart.svg', bbox_inches="tight")
        
        """ compare VTA sizes """
        fig = plt.figure(figsize=(10, 5), dpi=100)
        axsvta = []
        for caseidx, case in enumerate(self.cases):
            ax = plt.subplot(2,2,caseidx+1)
            model = models[case['name']]
            stim_amps = model['stim_amps']
            vtavolumes = model['vtavolumes']
            plt.plot(stim_amps,vtavolumes, '-k',linewidth='2', label='VTA')
            plt.plot(stim_amps,model[quantity['name']]['vtavolumes_constant'], 'dr', label='VTA $E_\mathsf{const}$')
            for quantity in self.quantities:
                lbl = 'VTA $\\varphi$' if quantity['name']=='potential' else 'VTA $E$'
                plt.plot(stim_amps,model[quantity['name']]['vtavolumes'], quantity['marker']+quantity['color'], label=lbl)
                xmin = np.floor(stim_amps[0]*2/unit_stim_amp)/float(2/unit_stim_amp)
                xmax = np.ceil(stim_amps[-1]*2/unit_stim_amp)/float(2/unit_stim_amp)
                xsteps = float(xmax-xmin)/5
                xticks = np.arange(xmin,xmax,xsteps)
                if abs(xticks[-1])<abs(xmax):
                    xticks = np.concatenate((xticks, [xmax]), axis=0)
                if abs(abs(xticks[-1])-abs(stim_amps[-1]))<abs(stim_amps[-1]-stim_amps[-2])/2.:
                    xticks = np.concatenate((xticks, [xmax+xsteps]), axis=0)
                ax.set_xticks(xticks)
                ax.set_xticklabels(["%.1f" % stim for stim in ax.get_xticks()*1e3])    
                ax.set_xlim([xticks[0], xticks[-1]])
                ax.set_ylim([0, model[quantity['name']]['vtavolumes_constant'][-1]*1.1])
                plt.title(case['pltname']+' ($f_d$='+str(self.fiberD)+' $\\mu m$)')
                if caseidx == 0 or caseidx == 2:    
                    plt.ylabel('Activation Volume [mm$^3$]')
                if caseidx > 1:
                    plt.xlabel('Stimulation Amplitude [mA]')
                ax.set_yticklabels(["%d" % int(vta) for vta in ax.get_yticks()*1e9])
            
            
            axsvta.append(ax)
            if len(axsvta)==1:
                plt.legend(loc=2, ncol=4)
        plt.savefig(self._figdir+'field_approximation.svg', bbox_inches="tight")
        
            
        
    def _postprocess_cases(self):
        models = dict()
        for case in self.cases:
            name = case['name']
            fieldmodel = VolumeConductorDBSElectrodeWithSTNInMotorRegion()
            fieldmodel.load_model_state(self._tmpdir+name, self.fieldmodelname)
            vta = vtautil.load_state(self._tmpdir+name, self.axonsname)
            models[name] = {'name': name,
                            'pltname': case['pltname'],
                            'fieldmodel': fieldmodel,
                            'vtamodel': vta}
        
        """ determine approximation thresholds for vta """
        for case in self.cases:
            model = models[case['name']]
            model['pltname']=case['pltname']
            model['name']=case['name']
            fieldmodel = model['fieldmodel']
            vta = model['vtamodel']
            Z = fieldmodel.get_impedance()
            unit_stim_amp = case['neuronparams']['dbs_signal'].params[DBSsignal.AMPLITUDE]
            
            stimmult_min = vta.get_stim_min()
            stimmult_max = vta.get_stim_max()
            stimmultipliers = np.linspace(stimmult_min, stimmult_max, 10)
            model['stimmultipliers']=stimmultipliers
            model['unit_stim_amp']= unit_stim_amp
            model['stim_amps'] = stimmultipliers*unit_stim_amp
            model['impedance']=Z
            
            thresh_fit_coords= {}
            vtavolumes = np.zeros((len(stimmultipliers),))
            for idx in xrange(len(stimmultipliers)):
                # use all hull coords for field norm matching
                #
                #hull = ConvexHull(coords)
                #hull_coords[idx] = coords[hull.vertices,:]
                
                # use only threshold distance centered to electrode in each plane
                coords = np.zeros((len(vta.get_planes()),3))
                for idxplane, plane in enumerate(vta.get_planes().values()):
                    coords[idxplane,:] = plane.get_line(0).get_coord_for_optimal_threshold(stimmultipliers[idx])
                    
                thresh_fit_coords[idx]=coords
                
                hull_coords= vta.get_coords_for_optimal_threshold(stimmultipliers[idx])
                hull = ConvexHull(hull_coords)
                
                vtavolumes[idx] = hull.volume
            
            model['vtavolumes'] = vtavolumes
            
            for quantity in self.quantities:
                self.evaluate_vta_quantity_fit(model, quantity, thresh_fit_coords)
                
                vta_minimal = Threshold_Volume()
                vta_minimal.set_state(vta.get_state())
                vta_minimal.set_approxfield(True)
                for plane in vta.get_planes().values():
                    for point in plane.get_line(0).get_points():
                        vta_minimal.add_point(point)
                
                vta_field = ApproxField_Threshold_Volume(vta_minimal, quantity['name'])
                maxstim = model[quantity['name']]['normalized'][-1]*model['stimmultipliers'][-1]
                params = case['neuronparams']
                params['field'] = fieldmodel
                vta_field.compute_activation_range(1, params, maxstim)
                
                vta_field_constant = Threshold_Volume()
                vta_field_constant.set_state(vta_field.get_state())
                for point in vta_field.get_points():
                    vta_field_constant.add_point(point)
                
                model[quantity['name']]['vta_field'] = vta_field
                
                vta_field_volumes = np.zeros((len(stimmultipliers),))
                vta_field_volumes_constant = np.zeros((len(stimmultipliers),))
                unit_threshs_constant = model[quantity['name']]['vta_unit_threshs_constant']
                for idx in xrange(len(stimmultipliers)):
                    hull_coords = vta_field.get_coords_for_optimal_threshold(stimmultipliers[idx])
                    if hull_coords.shape[0] == 0:
                        print("WARN: Couldn't find hull coordinates for stimmultiplier "+str(stimmultipliers[idx]))
                        vta_field_volumes[idx] = np.nan
                    else:
                        hull = ConvexHull(hull_coords)
                        vta_field_volumes[idx] = hull.volume
                    
                    if np.isnan(unit_threshs_constant[idx]):
                        # this happens if the threshold-distance relationship was not monotonous!
                        vta_field_volumes_constant[idx] = np.nan
                        print("WARN: Found probably a not monotonous threshold distance relationship!")
                        continue
                    
                    hull_coords_constant = vta_field_constant.get_coords_for_optimal_threshold(unit_threshs_constant[idx])
                    if hull_coords_constant.shape[0] == 0:
                        # this happens if the constant field approach deviates so much from the actual threshold-distance-relatoinship,
                        # meaning that the slop of the relationship is so large, that the actual estimates for the corresponding
                        # stimulation multipliers yield a VTA out of range of the computed values around the stimulation electrode.
                        # Means: The VTA is not computable for these values.
                        vta_field_volumes_constant[idx] = np.nan
                        print("WARN! The constant threshold field approach might not be applicable, because of a too large slope in the normalization routine for the field thresholds.")
                        continue
                    
                    hull_constant = ConvexHull(hull_coords_constant)
                    vta_field_volumes_constant[idx] = hull_constant.volume
                model[quantity['name']]['vtavolumes'] = vta_field_volumes
                model[quantity['name']]['vtavolumes_constant'] = vta_field_volumes_constant
                
        return models
        
    def evaluate_vta_quantity_fit(self, model, quantity, hull_coords):
        fieldmodel = model['fieldmodel']
        Z = model['impedance']
        quantity_name = quantity['name']
        stimmultipliers = model['stimmultipliers']
        stim_amps = stimmultipliers*model['unit_stim_amp']
        if quantity_name == 'potential':
            phys_quantity = fieldmodel.get_potential()
        elif quantity_name == 'normE':
            phys_quantity = fieldmodel.get_electric_field_norm()
        else:
            raise Exception('Unknown quantity name '+quantity_name)
        
        mean_thresh_quantity = np.zeros((len(hull_coords),))
        for idx in xrange(len(stimmultipliers)):
            if np.max(np.isnan(hull_coords[idx])):
                mean_thresh_quantity[idx] = np.nan
            else:
                thresh_quantity = fieldmodel.evaluate_quantity(phys_quantity, hull_coords[idx])*Z*stim_amps[idx]
                mean_thresh_quantity[idx] = np.mean(thresh_quantity)
        
        normalized_thresh_quantity = mean_thresh_quantity/(mean_thresh_quantity[0])
        thresh_quantity_polyfit = self.polyfit_with_fixed_points(1, stim_amps,
                                                                 normalized_thresh_quantity,
                                                                 np.array([stim_amps[0]]),
                                                                 np.array([1.0]))
        thresh_quantity_poly = np.polynomial.Polynomial(thresh_quantity_polyfit)
        
        model[quantity_name] = {
            'mean': mean_thresh_quantity,
            'normalized': normalized_thresh_quantity,
            'polyfit': thresh_quantity_poly,
            'vta_unit_threshs': mean_thresh_quantity/(stim_amps*fieldmodel.get_impedance()),
            'vta_unit_threshs_constant': np.mean(mean_thresh_quantity)/(stim_amps*fieldmodel.get_impedance())
            }
      
            
    def run(self):
        for case in self.cases:
            self.run_single_case(case)
    
    def run_single_case(self, case):
        name = case['name']
        fieldparams = case['fieldparams']
        neuronparams = case['neuronparams']
        fieldmodel = VolumeConductorDBSElectrodeWithSTNInMotorRegion()
        if self._debug:
            fieldmodel.set_coarse_mesh(1)
        else:
            fieldmodel.set_basis_order(2)
        fieldmodel.init()
        fieldmodel.run(fieldparams)
        fieldmodel.store_model_state(self._tmpdir+name, self.fieldmodelname)
        
        neuronparams['field'] = fieldmodel
        neuronparams['impedance'] = fieldmodel.get_impedance()
        
        """ determine min and max stimulation amplitude to activate a distance
        between 2.0 and 4.0 mm to electrode center and in tangential distance
        to active electrode contact center """
        dbs_signal = neuronparams['dbs_signal']
        axon = Axon({'diameter': case['neuronparams']['fiberD']})
        axon_min_max = Threshold_Volume()
        point_min_right = self.__create_single_threshold_point(axon_min_max, 0, 2.0e-3, axon, dbs_signal, fieldmodel)
        #point_min_up = self.__create_single_threshold_point(axon_min_max, 2.0e-3, 0.835e-3, axon, dbs_signal, fieldmodel)
        #point_min_down = self.__create_single_threshold_point(axon_min_max, -2.0e-3, 0.835e-3, axon, dbs_signal, fieldmodel)
        point_max_right = self.__create_single_threshold_point(axon_min_max, 0, 4.0e-3, axon, dbs_signal, fieldmodel)
        #point_max_up = self.__create_single_threshold_point(axon_min_max, 4.0e-3, 0.835e-3, axon, dbs_signal, fieldmodel)
        #point_max_down = self.__create_single_threshold_point(axon_min_max, -4.0e-3, 0.835e-3, axon, dbs_signal, fieldmodel)
        axon_min_max.compute_optimal_activation(self._ncpus, 1.0)
        stimmult_min = axon_min_max.get_point(point_min_right).get_threshold()
        stimmult_max = axon_min_max.get_point(point_max_right).get_threshold()
        #=======================================================================
        # stimmult_min = np.min([axon_min_max.get_point(point_min_right).get_threshold(),
        #                        axon_min_max.get_point(point_min_up).get_threshold(),
        #                        axon_min_max.get_point(point_min_down).get_threshold()])
        # stimmult_max = np.min([axon_min_max.get_point(point_max_right).get_threshold(),
        #                        axon_min_max.get_point(point_max_up).get_threshold(),
        #                        axon_min_max.get_point(point_max_down).get_threshold()])
        #=======================================================================
        # round closest to one digit precision
        stimmult_min_rounded = np.round(stimmult_min, 1)
        stimmult_max_rounded = np.round(stimmult_max, 1)
        prec = 2
        while stimmult_min_rounded == stimmult_max_rounded:
            stimmult_min_rounded = np.round(stimmult_min, prec)
            stimmult_max_rounded = np.round(stimmult_max, prec)
            prec += 1
        
        stimmult_min = stimmult_min_rounded
        stimmult_max = stimmult_max_rounded
            
        case['neuronparams']['stim_min'] = stimmult_min
        case['neuronparams']['stim_max'] = stimmult_max
        
        print name+": Determined stim_min = "+str(stimmult_min)+" and stim_max = "+str(stimmult_max)                
        
        vta = vtautil.compute_vta(self._ncpus, neuronparams)
        vta.set_stim_min(stimmult_min)
        vta.set_stim_max(stimmult_max)
        vta.store_state(self._tmpdir+name, self.axonsname)
        print "Full VTA runtime: "+str(vta.get_runtime())
        coords_min = vta.get_coords_for_optimal_threshold(stimmult_min)
        coords_max = vta.get_coords_for_optimal_threshold(stimmult_max)
        vol_min = ConvexHull(coords_min).volume
        vol_max = ConvexHull(coords_max).volume
        print "Stimmult min "+str(stimmult_min)+", VTA size "+str(vol_min)
        print "Stimmult max "+str(stimmult_max)+", VTA size "+str(vol_max)
        
    def __create_single_threshold_point(self, axon_min_max, tangential, normal, axon, dbs_signal, fieldmodel):
        number_of_points = len(axon_min_max.get_points())
        point = Threshold_Point(Threshold_Location(0,0,number_of_points))
        transform = point.get_transform()
        coordinate = np.array([0, normal, tangential])
        transform.set_position_vector(coordinate)
        nodes = transform.transform_coords(axon.get_nodes())
        point.set_coordinate(nodes[0]/2+nodes[-1]/2)
        point.set_axon(axon)
        point.set_dbs_signal(dbs_signal)
        potential = fieldmodel.evaluate_quantity(fieldmodel.get_potential(), nodes)*fieldmodel.get_impedance()
        point.set_potential(potential)
        axon_min_max.add_point(point)
        return point
    
    def polyfit_with_fixed_points(self, n, x, y, xf, yf) :
        mat = np.empty((n + 1 + len(xf),) * 2)
        vec = np.empty((n + 1 + len(xf),))
        x_n = x**np.arange(2 * n + 1)[:, None]
        yx_n = np.sum(x_n[:n + 1] * y, axis=1)
        x_n = np.sum(x_n, axis=1)
        idx = np.arange(n + 1) + np.arange(n + 1)[:, None]
        mat[:n + 1, :n + 1] = np.take(x_n, idx)
        xf_n = xf**np.arange(n + 1)[:, None]
        mat[:n + 1, n + 1:] = xf_n / 2
        mat[n + 1:, :n + 1] = xf_n.T
        mat[n + 1:, n + 1:] = 0
        vec[:n + 1] = yx_n
        vec[n + 1:] = yf
        params = np.linalg.solve(mat, vec)
        return params[:n + 1]