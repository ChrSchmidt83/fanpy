'''
Created on Mar 29, 2017

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPyStudy
'''
import numpy as np
from abstract_study import FanPyPaperStudy
import matplotlib.pyplot as plt
import os
from fanpy.fieldmodels.fenics.dbs_field_models import VolumeConductorDBSElectrodeWithSTNInMotorRegion
from fanpy.core.dbssignal import DBSsignal
from fanpy.neuronmodels.mam_axon import vtautil
from fanpy.core.tissue_dielectrics import DielectricProperties as DielP, ETissue
from scipy.spatial import ConvexHull
from fanpy.neuronmodels.mam_axon.vtautil import Threshold_Volume, Threshold_Point,\
    Threshold_Location
from fanpy.core.axon import Axon

class ApproximateVTAByFieldThresholdsSchemeRun(FanPyPaperStudy):
    
    def __init__(self, params, fiberD):   
        FanPyPaperStudy.__init__(self, params)
        self.fiberD = fiberD
        fiberDstr = str(fiberD).replace('.','_')
        self._tmpdir = self._tmpdir + 'approx_vta_by_field_thresholds_scheme_run'+fiberDstr+'/'
        if not os.path.exists(self._tmpdir):
            os.mkdir(self._tmpdir)
        self._figdir = self._figdir + 'approx_vta_by_field_thresholds_scheme_run'+fiberDstr+'/'
        if not os.path.exists(self._figdir):
            os.mkdir(self._figdir)
        self.fieldmodelname = 'fieldmodel.hdf5'
        self.axonsname = 'axon_data'
        
        """ material properties """
        wmatter = DielP(ETissue.BRAIN_WHITE_MATTER)
        gmatter = DielP(ETissue.BRAIN_GREY_MATTER)
        csf = DielP(ETissue.CEREBRO_SPINAL_FLUID)

        frequency = 2e3
        wmatter_diel = wmatter.get_dielectrics(frequency)
        gmatter_diel = gmatter.get_dielectrics(frequency)
        csf_diel = csf.get_dielectrics(frequency)
        
        """ dbs signal """
        if self._debug:
            npulses = 1
            dt = 1e-5
            freq = 1300
        else:
            npulses = 10 # ToDo: has to be 10
            dt = 5e-6 # ToDo. has to be 5e-6
            freq = 130
                    
        signal_params = {
        DBSsignal.FREQUENCY: freq,
        DBSsignal.AMPLITUDE: -1e-3, # 1 mA
        DBSsignal.DT: dt,
        DBSsignal.NUMBER_OF_PULSES: npulses,
        DBSsignal.PULSE_DURATION: 6e-5}
        dbs_signal = DBSsignal(signal_params) 
        
        """ study cases """
        if self._debug:
            rot_degree_step = 45
            g_step = 0.5e-3
            symmetric = True
        else:
            rot_degree_step = 10
            g_step = 0.5e-3
            symmetric = False
            
        self.cases = []
        self.cases.append({
                'name': 'homogeneous',
                'pltname': 'Model 1',
                'color': 'g',
                'marker': 'x',
                'fieldparams': {
                    'dielectric_properties': {
                        'brain': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()},
                        'encapsulation': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()},
                        'stn': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()}
                            },
                        },
                'neuronparams': {
                    'g_step': g_step,
                    'rot_degree_step': rot_degree_step,
                    'dbs_signal': dbs_signal,
                    'fiberD': fiberD,
                    'approx_by_field': True,
                    #'stim_min': 0.5,
                    #'stim_max': 3.0,
                    'symmetric': True,
                    }
                })
        self.cases.append({
                'name': 'high_encap',
                'pltname': 'Model 2',
                'color': 'b',
                'marker': 'o',
                'fieldparams': {
                    'dielectric_properties': {
                        'brain': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()},
                        'encapsulation': {'conductivity': csf_diel[DielP.CONDUCTIVITY].item()},
                        'stn': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()}
                            },
                        },
                'neuronparams': {
                    'g_step': g_step,
                    'rot_degree_step': rot_degree_step,
                    'dbs_signal': dbs_signal,
                    'fiberD': fiberD,
                    'approx_by_field': True,
                    #'stim_min': 0.5,
                    #'stim_max': 3.0,
                    'symmetric': True,
                    }
                })
        self.cases.append({
                'name': 'low_encap',
                'pltname': 'Model 3',
                'color': 'r',
                'marker': 'd',
                'fieldparams': {
                    'dielectric_properties': {
                        'brain': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()},
                        'encapsulation': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()/2.},
                        'stn': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()}
                            },
                        },
                'neuronparams': {
                    'g_step': g_step,
                    'rot_degree_step': rot_degree_step,
                    'dbs_signal': dbs_signal,
                    'fiberD': fiberD,
                    'approx_by_field': True,
                    #'stim_min': 0.5,
                    #'stim_max': 3.0,
                    'symmetric': True,
                    }
                })
        self.cases.append({
                'name': 'low_encap_with_stn',
                'pltname': 'Model 4',
                'color': 'k',
                'marker': 's',
                'fieldparams': {
                    'dielectric_properties': {
                        'brain': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()},
                        'encapsulation': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()/2.},
                        'stn': {'conductivity': gmatter_diel[DielP.CONDUCTIVITY].item()}
                            },
                        },
                'neuronparams': {
                    'g_step': g_step,
                    'rot_degree_step': rot_degree_step,
                    'dbs_signal': dbs_signal,
                    'fiberD': fiberD,
                    'approx_by_field': True,
                    #'stim_min': 0.5,
                    #'stim_max': 3.0,
                    'symmetric': symmetric,
                    }
                })
    
    def postprocessing(self):
        plt.rcParams["font.family"] = "serif"
        
        self.quantities = []    
        self.quantities.append({
            'name': 'potential',
                'color': 'g',
                'marker': 'o',
                'line': '-'
                        })
        self.quantities.append({
            'name': 'normE',
                'color': 'b',
                'marker': 'x',
                'line': '-'
            })
        
        """ load the data from the study cases """
        models = self._postprocess_cases()

        for case in self.cases:
            model = models[case['name']]
            vta = model['vtamodel']
            name = case['name']
            report = name+' ('+case['pltname']+') fiberD='+str(+self.fiberD)+'\n'
            unit_stim_amp = case['neuronparams']['dbs_signal'].params[DBSsignal.AMPLITUDE]
            stimmultipliers = model['stimmultipliers']
            stim_amps = stimmultipliers*unit_stim_amp
           
            report += 'Number of axons = '+str(vta.get_points())+'\n'
            report += 'Stimulus range = '+str(stim_amps[0]*1e3)+','+str(stim_amps[-1]*1e3)+' mA'+'\n'
            report += 'Runtime VTA = '+str(vta.get_runtime())+'\n'
                       
            with open(self._figdir+'study_report_'+name+'.txt','w') as reportfile:
                reportfile.write(report)
            print report

    def _postprocess_cases(self):
        models = dict()
        for case in self.cases:
            name = case['name']
            vta = vtautil.load_state(self._tmpdir+name, self.axonsname)
            models[name] = {'name': name,
                            'pltname': case['pltname'],
                            'vtamodel': vta}
        
        """ determine approximation thresholds for vta """
        for case in self.cases:
            model = models[case['name']]
            model['pltname']=case['pltname']
            model['name']=case['name']
            vta = model['vtamodel']
            unit_stim_amp = case['neuronparams']['dbs_signal'].params[DBSsignal.AMPLITUDE]
            
            stimmult_min = vta.get_stim_min()
            stimmult_max = vta.get_stim_max()
            stimmultipliers = np.linspace(stimmult_min, stimmult_max, 10)
            model['stimmultipliers']=stimmultipliers
            model['unit_stim_amp']= unit_stim_amp
            model['stim_amps'] = stimmultipliers*unit_stim_amp
                
        return models

    def run(self):
        for case in self.cases:
            self.run_single_case(case)
    
    def run_single_case(self, case):
        name = case['name']
        fieldparams = case['fieldparams']
        neuronparams = case['neuronparams']
        fieldmodel = VolumeConductorDBSElectrodeWithSTNInMotorRegion()
        if self._debug:
            fieldmodel.set_coarse_mesh(1)
        else:
            fieldmodel.set_basis_order(2)
        fieldmodel.init()
        fieldmodel.run(fieldparams)
        #fieldmodel.store_model_state(self._tmpdir+name, self.fieldmodelname)
        
        neuronparams['field'] = fieldmodel
        neuronparams['impedance'] = fieldmodel.get_impedance()
        
        """ determine min and max stimulation amplitude to activate a distance
        between 2.0 and 4.0 mm to electrode center and in tangential distance
        to active electrode contact center """
        dbs_signal = neuronparams['dbs_signal']
        axon = Axon({'diameter': case['neuronparams']['fiberD']})
        axon_min_max = Threshold_Volume()
        point_min_right = self.__create_single_threshold_point(axon_min_max, 0, 2.0e-3, axon, dbs_signal, fieldmodel)
        point_max_right = self.__create_single_threshold_point(axon_min_max, 0, 4.0e-3, axon, dbs_signal, fieldmodel)
        axon_min_max.compute_optimal_activation(self._ncpus, 1.0)
        stimmult_min = axon_min_max.get_point(point_min_right).get_threshold()
        stimmult_max = axon_min_max.get_point(point_max_right).get_threshold()

        # round closest to one digit precision
        stimmult_min_rounded = np.round(stimmult_min, 1)
        stimmult_max_rounded = np.round(stimmult_max, 1)
        prec = 2
        while stimmult_min_rounded == stimmult_max_rounded:
            stimmult_min_rounded = np.round(stimmult_min, prec)
            stimmult_max_rounded = np.round(stimmult_max, prec)
            prec += 1
        
        stimmult_min = stimmult_min_rounded
        stimmult_max = stimmult_max_rounded
            
        case['neuronparams']['stim_min'] = stimmult_min
        case['neuronparams']['stim_max'] = stimmult_max
        
        print name+": Determined stim_min = "+str(stimmult_min)+" and stim_max = "+str(stimmult_max)                
        
        vta = vtautil.compute_vta(self._ncpus, neuronparams)
        vta.set_stim_min(stimmult_min)
        vta.set_stim_max(stimmult_max)
        vta.store_state(self._tmpdir+name, self.axonsname)
        print "Neuron computation runtime: "+str(vta.neuron_volume.get_runtime())
        print "Full VTA runtime: "+str(vta.get_runtime())
        coords_min = vta.get_coords_for_optimal_threshold(stimmult_min)
        coords_max = vta.get_coords_for_optimal_threshold(stimmult_max)
        vol_min = ConvexHull(coords_min).volume
        vol_max = ConvexHull(coords_max).volume
        print "Stimmult min "+str(stimmult_min)+", VTA size "+str(vol_min)
        print "Stimmult max "+str(stimmult_max)+", VTA size "+str(vol_max)
        
    def __create_single_threshold_point(self, axon_min_max, tangential, normal, axon, dbs_signal, fieldmodel):
        number_of_points = len(axon_min_max.get_points())
        point = Threshold_Point(Threshold_Location(0,0,number_of_points))
        transform = point.get_transform()
        coordinate = np.array([0, normal, tangential])
        transform.set_position_vector(coordinate)
        nodes = transform.transform_coords(axon.get_nodes())
        point.set_coordinate(nodes[0]/2+nodes[-1]/2)
        point.set_axon(axon)
        point.set_dbs_signal(dbs_signal)
        potential = fieldmodel.evaluate_quantity(fieldmodel.get_potential(), nodes)*fieldmodel.get_impedance()
        point.set_potential(potential)
        axon_min_max.add_point(point)
        return point