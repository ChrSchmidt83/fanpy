'''
Created on Mar 29, 2017

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPyStudy
'''
import numpy as np
from abstract_study import FanPyPaperStudy
import os
from fanpy.fieldmodels.fenics.dbs_field_models import VolumeConductorDBSElectrodeWithSTNInMotorRegion
from fanpy.core.tissue_dielectrics import DielectricProperties as DielP, ETissue

class CompareFieldSolutionForBasisOrder(FanPyPaperStudy):
    
    def __init__(self, params):   
        FanPyPaperStudy.__init__(self, params)
        self._tmpdir = self._tmpdir + 'compare_field_solutions/'
        if not os.path.exists(self._tmpdir):
            os.mkdir(self._tmpdir)
        self._figdir = self._figdir + 'compare_field_solutions/'
        if not os.path.exists(self._figdir):
            os.mkdir(self._figdir)
        self.fieldmodelname_2 = 'fieldmodel_2.hdf5'
        self.fieldmodelname_3 = 'fieldmodel_3.hdf5'
        
        """ material properties """
        wmatter = DielP(ETissue.BRAIN_WHITE_MATTER)
        gmatter = DielP(ETissue.BRAIN_GREY_MATTER)
        csf = DielP(ETissue.CEREBRO_SPINAL_FLUID)

        frequency = 2e3
        wmatter_diel = wmatter.get_dielectrics(frequency)
        gmatter_diel = gmatter.get_dielectrics(frequency)
        csf_diel = csf.get_dielectrics(frequency)
            
        self.cases = []
        self.cases.append({
                'name': 'homogeneous',
                'pltname': 'Model 1',
                'color': 'g',
                'marker': 'x',
                'fieldparams': {
                    'dielectric_properties': {
                        'brain': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()},
                        'encapsulation': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()},
                        'stn': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()}
                            },
                        }
                })
        self.cases.append({
                'name': 'high_encap',
                'pltname': 'Model 2',
                'color': 'b',
                'marker': 'o',
                'fieldparams': {
                    'dielectric_properties': {
                        'brain': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()},
                        'encapsulation': {'conductivity': csf_diel[DielP.CONDUCTIVITY].item()},
                        'stn': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()}
                            },
                        }
                })
        self.cases.append({
                'name': 'low_encap',
                'pltname': 'Model 3',
                'color': 'r',
                'marker': 'd',
                'fieldparams': {
                    'dielectric_properties': {
                        'brain': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()},
                        'encapsulation': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()/2.},
                        'stn': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()}
                            },
                        }
                })
        self.cases.append({
                'name': 'low_encap_with_stn',
                'pltname': 'Model 4',
                'color': 'k',
                'marker': 's',
                'fieldparams': {
                    'dielectric_properties': {
                        'brain': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()},
                        'encapsulation': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()/2.},
                        'stn': {'conductivity': gmatter_diel[DielP.CONDUCTIVITY].item()}
                            },
                        }
                })
    
    def postprocessing(self):        
        """ load the data from the study cases """
        models = self._postprocess_cases()
                
        for case in self.cases:
            model = models[case['name']]
            fieldmodel_2 = model['fieldmodel_2']
            fieldmodel_3 = model['fieldmodel_3']
            
            name = case['name']
            report = name+' ('+case['pltname']+')\n'
            
            impedance_dev = np.abs(fieldmodel_2.get_impedance()-fieldmodel_3.get_impedance())/fieldmodel_3.get_impedance()*100
            report += 'Impedance deviation = '+str(impedance_dev)+' %\n'  
            
            testpoints = np.outer(np.linspace(2,4,5), np.array([1e-3,0,0]))
            value2 = fieldmodel_2.evaluate_quantity(fieldmodel_2.get_potential(), testpoints)
            value3 = fieldmodel_3.evaluate_quantity(fieldmodel_3.get_potential(), testpoints)
            potential_dev = np.linalg.norm(value2-value3)/np.linalg.norm(value3)*100
            report += 'Potential deviation = '+str(potential_dev)+' %\n'
            
            value2 = fieldmodel_2.evaluate_quantity(fieldmodel_2.get_electric_field_norm(), testpoints)
            value3 = fieldmodel_3.evaluate_quantity(fieldmodel_3.get_electric_field_norm(), testpoints)
            enorm_dev = np.linalg.norm(value2-value3)/np.linalg.norm(value3)*100
            report += 'Electric field norm deviation = '+str(enorm_dev)+' %\n'
                      
            with open(self._figdir+'study_report_'+name+'.txt','w') as reportfile:
                reportfile.write(report)
            print report
            
        
    def _postprocess_cases(self):
        models = dict()
        for case in self.cases:
            name = case['name']
            fieldmodel_2 = VolumeConductorDBSElectrodeWithSTNInMotorRegion()
            fieldmodel_2.load_model_state(self._tmpdir+name, self.fieldmodelname_2)
            fieldmodel_3 = VolumeConductorDBSElectrodeWithSTNInMotorRegion()
            fieldmodel_3.load_model_state(self._tmpdir+name, self.fieldmodelname_3)
            models[name] = {'name': name,
                            'pltname': case['pltname'],
                            'fieldmodel_2': fieldmodel_2,
                            'fieldmodel_3': fieldmodel_3
                            }
                
        return models
            
    def run(self):
        for case in self.cases:
            self.run_single_case(case)
    
    def run_single_case(self, case):
        name = case['name']
        fieldparams = case['fieldparams']
        fieldmodel_2 = VolumeConductorDBSElectrodeWithSTNInMotorRegion()
        if self._debug:
            fieldmodel_2.set_coarse_mesh(1)
        else:
            fieldmodel_2.set_basis_order(2)
        fieldmodel_2.init()
        fieldmodel_2.run(fieldparams)
        fieldmodel_2.store_model_state(self._tmpdir+name, self.fieldmodelname_2)
        
        fieldmodel_3 = VolumeConductorDBSElectrodeWithSTNInMotorRegion()
        if self._debug:
            fieldmodel_3.set_coarse_mesh(2)
        else:
            fieldmodel_3.set_basis_order(3)
        fieldmodel_3.init()
        fieldmodel_3.run(fieldparams)
        fieldmodel_3.store_model_state(self._tmpdir+name, self.fieldmodelname_3)