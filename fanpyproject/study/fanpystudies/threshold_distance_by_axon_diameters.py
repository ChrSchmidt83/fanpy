'''
Created on Mar 29, 2017

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPyStudy
'''
import numpy as np
from abstract_study import FanPyPaperStudy
from fanpy.fieldmodels.fenics.dbs_field_models import VolumeConductorDBSElectrodeWithSTNInMotorRegion,\
    VolumeConductorDBSElectrode
from fanpy.neuronmodels.mam_axon.vtautil import Threshold_Volume, Threshold_Point, Threshold_Location
from fanpy.core.axon import Axon
from fanpy.core.dbssignal import DBSsignal
from fanpy.neuronmodels.mam_axon import vtautil
import matplotlib.pyplot as plt
import os

class ThresholdDistanceByAxonDiameters(FanPyPaperStudy):
    
    def __init__(self, params):   
        FanPyPaperStudy.__init__(self, params)
        self._tmpdir = self._tmpdir + 'thresh_dist_axon_diam/'
        if not os.path.exists(self._tmpdir):
            os.mkdir(self._tmpdir)
        self._figdir = self._figdir + 'thresh_dist_axon_diam/'
        if not os.path.exists(self._figdir):
            os.mkdir(self._figdir)
        self.fieldmodelname = 'fieldmodel.hdf5'
        self.axonsname = 'axon_data'
        
        self.axon_diams = sorted(Axon.AXONPARAMETERS.keys())
    
    def postprocessing(self):
        plt.rcParams["font.family"] = "serif"
        #fieldmodel = VolumeConductorDBSElectrode()
        fieldmodel = VolumeConductorDBSElectrodeWithSTNInMotorRegion()
        fieldmodel.load_model_state(self._tmpdir, self.fieldmodelname)
        
        axons = vtautil.load_state(self._tmpdir, self.axonsname)
        
        marker=['x','o','d','s','x','o','d','s','x']
        color=['b','r','g','m','c','k','b']
        line=['-','--','-.',':','-.','--','-','--','-']
        plt.figure(figsize=(8, 3), dpi=100)
        for idx, diam in enumerate(self.axon_diams):
            if diam > 10.0:
                continue
            points = axons.get_plane(0).get_line(idx).get_points()
            distance = np.zeros(len(points),)
            threshold = np.zeros(len(points),)
            unit_amplitude = axons.get_points()[0].get_dbs_signal().get_params()[DBSsignal.AMPLITUDE]
            for i, point in enumerate(points):
                distance[i] = point.get_coordinate()[1]
                threshold[i] = point.get_threshold()*unit_amplitude
            plt.plot(distance*1e3, threshold, color[idx]+marker[idx])
            p = np.poly1d(np.polyfit(distance*1e3, threshold, 2))
            x = np.linspace(distance[0], distance[-1],100)
            p = np.polynomial.Polynomial(self.polyfit_with_fixed_points(2, distance*1e3, threshold, np.array([distance[0]*1e3]), np.array([threshold[0]])))
            plt.plot(x*1e3, p(x*1e3), color[idx]+line[idx], label='$f_d=$'+str(diam))
        plt.legend(loc=2, ncol=2)
        plt.xlabel('Distance from center [mm]')
        plt.ylabel('Stimulation Amplitude [V]')
        if unit_amplitude<0:
            plt.gca().invert_yaxis()
        
        plt.savefig(self._figdir+'threshold_distance_comparison_diameters.png', bbox_inches="tight")
        plt.savefig(self._figdir+'threshold_distance_comparison_diameters.svg', bbox_inches="tight")
        
    def polyfit_with_fixed_points(self, n, x, y, xf, yf) :
        mat = np.empty((n + 1 + len(xf),) * 2)
        vec = np.empty((n + 1 + len(xf),))
        x_n = x**np.arange(2 * n + 1)[:, None]
        yx_n = np.sum(x_n[:n + 1] * y, axis=1)
        x_n = np.sum(x_n, axis=1)
        idx = np.arange(n + 1) + np.arange(n + 1)[:, None]
        mat[:n + 1, :n + 1] = np.take(x_n, idx)
        xf_n = xf**np.arange(n + 1)[:, None]
        mat[:n + 1, n + 1:] = xf_n / 2
        mat[n + 1:, :n + 1] = xf_n.T
        mat[n + 1:, n + 1:] = 0
        vec[:n + 1] = yx_n
        vec[n + 1:] = yf
        params = np.linalg.solve(mat, vec)
        return params[:n + 1]
                    
            
    def run(self):
        
        fieldparams = {
            'dielectric_properties': {
                'brain': {'conductivity': 0.3},
                'encapsulation': {'conductivity': 0.15},
                'stn': {'conductivity': 0.3}
                }
            }
        
        fieldmodel = VolumeConductorDBSElectrodeWithSTNInMotorRegion()
        if self._debug:
            fieldmodel.set_coarse_mesh(1)
        else:
            fieldmodel.set_basis_order(2)
            
        fieldmodel.init()
        
        fieldmodel.run(fieldparams)
        
        fieldmodel.store_model_state(self._tmpdir, self.fieldmodelname)
        
        # dbs signal
        if self._debug:
            npulses = 1
            dt = 1e-5
            freq = 1500
        else:
            npulses = 10
            dt = 5e-6
            freq = 150
                    
        signal_params = {
        DBSsignal.FREQUENCY: freq,
        DBSsignal.AMPLITUDE: -1, # 1 V
        DBSsignal.DT: dt,
        DBSsignal.NUMBER_OF_PULSES: npulses,
        DBSsignal.PULSE_DURATION: 1e-4}
        dbs_signal = DBSsignal(signal_params)
        
        # create distributed axon models
        axons = Threshold_Volume()
        start_distance = 1e-3
        spatial_step = 0.5e-3
        naxons = 8
        for idx, diam in enumerate(self.axon_diams):
            axon = Axon({'diameter':diam})
            for i in xrange(naxons):
                location = Threshold_Location(0,idx,i)
                point = Threshold_Point(location)
                transform = point.get_transform()
                coordinate = np.array([0, start_distance+location.get_normal()*spatial_step, 0])
                transform.set_position_vector(coordinate)
                nodes = transform.transform_coords(axon.get_nodes())
                point.set_coordinate(nodes[0]/2+nodes[-1]/2)
                point.set_axon(axon)
                point.set_dbs_signal(dbs_signal)
                potential = fieldmodel.evaluate_quantity(fieldmodel.get_potential(), nodes)
                point.set_potential(potential)
                axons.add_point(point)
            
        axons.compute_optimal_activation(self._ncpus, 1.0)    
        axons.store_state(self._tmpdir, self.axonsname)

        