'''
Created on Apr 3, 2017

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPyStudy
'''
from abstract_study import FanPyPaperStudy
import os
from fanpy.core.tissue_dielectrics import DielectricProperties as DielP, ETissue
from fanpy.core.dbssignal import DBSsignal
from fanpy.fieldmodels.fenics.dbs_field_models import VolumeConductorDBSElectrodeWithSTNInMotorRegion
from fanpy.neuronmodels.mam_axon.vtautil import Threshold_Volume, Threshold_Point,\
    Threshold_Location, Threshold_Line, Threshold_Plane
from fanpy.neuronmodels.mam_axon import vtautil
import matplotlib.pyplot as plt
import numpy as np
from pyevtk.hl import pointsToVTK

class VisualizeAdaptiveAlgorithm(FanPyPaperStudy):
    
    def __init__(self, params):   
        FanPyPaperStudy.__init__(self, params)
        self._tmpdir = self._tmpdir + 'visualize_adaptive_algorithm/'
        if not os.path.exists(self._tmpdir):
            os.mkdir(self._tmpdir)
        self._figdir = self._figdir + 'visualize_adaptive_algorithm/'
        if not os.path.exists(self._figdir):
            os.mkdir(self._figdir)
        self.fieldmodelname = 'fieldmodel.hdf5'
        self.axonsname_min = 'axon_data_min'
        self.axonsname_max = 'axon_data_max'
        self.axonsname_shell = 'axon_data_shell'
        self.axonsname_non_adaptive = 'axon_data_non_adaptive'
        
        """ material properties """
        wmatter = DielP(ETissue.BRAIN_WHITE_MATTER)

        frequency = 2e3
        wmatter_diel = wmatter.get_dielectrics(frequency)
        
        """ dbs signal """
        if self._debug:
            npulses = 1
            dt = 1e-5
            freq = 1300
        else:
            npulses = 10 # ToDo: has to be 10
            dt = 5e-6 # ToDo. has to be 5e-6
            freq = 130
                    
        signal_params = {
        DBSsignal.FREQUENCY: freq,
        DBSsignal.AMPLITUDE: -1, # 1 V
        DBSsignal.DT: dt,
        DBSsignal.NUMBER_OF_PULSES: npulses,
        DBSsignal.PULSE_DURATION: 6e-5,
        DBSsignal.TYPE: 'voltage'}
        dbs_signal = DBSsignal(signal_params) 
        
        rot_degree_step = 10
        """ study cases """
        if self._debug:
            g_step = 1.0e-3
        else:
            g_step = 0.5e-3
            
        self.params ={
                'fieldparams': {
                    'dielectric_properties': {
                        'brain': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()},
                        'encapsulation': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()},
                        'stn': {'conductivity': wmatter_diel[DielP.CONDUCTIVITY].item()}
                            },
                        },
                'neuronparams': {
                    'g_step': g_step,
                    'rot_degree_step': rot_degree_step,
                    'dbs_signal': dbs_signal,
                    'stim_min': 2.0,
                    'stim_max': 4.0,
                    'symmetric': True,
                    }
                }

    def postprocessing(self):
        plt.rcParams["font.family"] = "serif"
        vta_min = vtautil.load_state(self._tmpdir, self.axonsname_min)
        vta_max = vtautil.load_state(self._tmpdir, self.axonsname_max)
        vta_shell = vtautil.load_state(self._tmpdir, self.axonsname_shell)
        vta_non_adaptive = vtautil.load_state(self._tmpdir, self.axonsname_non_adaptive)
        
        msize = 20
        stim_max = self.params['neuronparams']['stim_max']
        stim_min = self.params['neuronparams']['stim_min']
        
        minpoints = vta_min.get_points()
        coords_min = np.zeros((len(minpoints),3))
        for idx, point in enumerate(vta_min.get_points()):
            coords_min[idx,0:2] = point.get_coordinate()[1:3]
            coords_min[idx,2] = (int(point.get_activated())+1)/float(2)
        plt.figure(figsize=(10, 6), dpi=100)
        ax = plt.subplot(2,2,1)
        plt.scatter(coords_min[:,0]*1e3,coords_min[:,1]*1e3, coords_min[:,2]*msize)
        plt.ylabel('z-axis [mm]')
        plt.xlabel('y-axis [mm]')
        plt.title('Activation map')
        ax.set_xlim([0,6])
        ax.set_ylim([-6, 6])
        ax.set_aspect(1)
        
        vta_min_no = vta_min.get_volume_without_zero_hull()
        
        maxpoints = vta_max.get_points()
        coords_max = np.zeros((len(maxpoints),3))
        for idx, point in enumerate(vta_max.get_points()):
            if vta_min_no.has_point(point):
                continue
            coords_max[idx,0:2] = point.get_coordinate()[1:3]
            coords_max[idx,2] = (int(point.get_activated())+1)/float(2)
        ax = plt.subplot(2,2,2)
        coords_min_no = vta_min_no.get_plane(0).get_coords()
        plt.scatter(coords_min_no[:,1]*1e3,coords_min_no[:,2]*1e3, msize, c='b')
        plt.scatter(coords_max[:,0]*1e3, coords_max[:,1]*1e3, coords_max[:,2]*msize, c='r')
        plt.ylabel('z-axis [mm]')
        plt.xlabel('y-axis [mm]')
        plt.title('Activation map')
        ax.set_xlim([0, 6])
        ax.set_ylim([-6, 6])
        ax.set_aspect(1)
        
        coords_non_adaptive = vta_non_adaptive.get_plane(0).get_coord_threshold_matrix()
        ax = plt.subplot(2,2,4)
        cm = coords_non_adaptive[:,3]
        cm_max = np.max(cm)
        cm_min = np.min(cm)
        sc = plt.scatter(coords_non_adaptive[:,1]*1e3,coords_non_adaptive[:,2]*1e3, msize, c=cm, vmin=cm_min, vmax=cm_max)
        ocoords_min = vta_non_adaptive.get_plane(0).get_coords_for_optimal_threshold(stim_min)
        sort_min = np.argsort(ocoords_min[:,2])
        ocoords_min[:,1:3] = ocoords_min[sort_min,1:3]
        ocoords_max = vta_non_adaptive.get_plane(0).get_coords_for_optimal_threshold(stim_max)
        plt.title('Non-adaptive approach')
        sort_max = np.argsort(ocoords_max[:,2])
        ocoords_max[:,1:3] = ocoords_max[sort_max,1:3]
        plt.plot(ocoords_min[:,1]*1e3,ocoords_min[:,2]*1e3, '-b', label='$A_\\mathsf{stim}$ =-'+str(stim_min)+' V', linewidth='2')
        plt.plot(ocoords_max[:,1]*1e3,ocoords_max[:,2]*1e3, '-r', label='$A_\\mathsf{stim}$ =-'+str(stim_max)+' V', linewidth='2')
        plt.ylabel('z-axis [mm]')
        plt.xlabel('y-axis [mm]')
        ax.set_xlim([0, 6])
        ax.set_ylim([-6, 6])
        ax.set_aspect(1)
        
        coords_shell = vta_shell.get_plane(0).get_coord_threshold_matrix()
        ax = plt.subplot(2,2,3)
        cm = coords_shell[:,3]
        sc = plt.scatter(coords_shell[:,1]*1e3,coords_shell[:,2]*1e3, msize, c=cm, vmin=cm_min, vmax=cm_max)
        ocoords_min = vta_shell.get_plane(0).get_coords_for_optimal_threshold(stim_min)
        sort_min = np.argsort(ocoords_min[:,2])
        ocoords_min[:,1:3] = ocoords_min[sort_min,1:3]
        ocoords_max = vta_shell.get_plane(0).get_coords_for_optimal_threshold(stim_max)
        plt.title('Threshold map')
        sort_max = np.argsort(ocoords_max[:,2])
        ocoords_max[:,1:3] = ocoords_max[sort_max,1:3]
        plt.plot(ocoords_min[:,1]*1e3,ocoords_min[:,2]*1e3, '-b', label='$A_\\mathsf{stim}$ =-'+str(stim_min)+' V', linewidth='2')
        plt.plot(ocoords_max[:,1]*1e3,ocoords_max[:,2]*1e3, '-r', label='$A_\\mathsf{stim}$ =-'+str(stim_max)+' V', linewidth='2')
        plt.ylabel('z-axis [mm]')
        plt.xlabel('y-axis [mm]')
        plt.legend(loc=1, bbox_to_anchor=(4.0,1.0))
        ax.set_xlim([0, 6])
        ax.set_ylim([-6, 6])
        ax.set_aspect(1)        
        
        plt.savefig(self._figdir+'visualize_adaptive_algorithm.png', bbox_inches="tight")
        plt.savefig(self._figdir+'visualize_adaptive_algorithm.svg', bbox_inches="tight")
        
        plt.figure(figsize=(3, 2.5), dpi=100)
        plt.colorbar(sc)
        plt.savefig(self._figdir+'visualize_adaptive_algorithm_colorbar.png', bbox_inches="tight")
        plt.savefig(self._figdir+'visualize_adaptive_algorithm_colorbar.svg', bbox_inches="tight")
        
        vtacoords_min = vta_shell.get_coords_for_optimal_threshold(stim_min)
        vtacoords_max = vta_shell.get_coords_for_optimal_threshold(stim_max)
        pointsToVTK(self._tmpdir+self.axonsname_min+'_vta.pvd', vtacoords_min[:,0], vtacoords_min[:,1], vtacoords_min[:,2], data={"vta":np.ones(vtacoords_min.shape[0],)})
        pointsToVTK(self._tmpdir+self.axonsname_max+'_vta.pvd', vtacoords_max[:,0], vtacoords_max[:,1], vtacoords_max[:,2], data={"vta":np.ones(vtacoords_max.shape[0],)})
                
    def run(self):
        fieldparams = self.params['fieldparams']
        neuronparams = self.params['neuronparams']
        fieldmodel = VolumeConductorDBSElectrodeWithSTNInMotorRegion()
        if self._debug:
            fieldmodel.set_coarse_mesh(1)
        else:
            fieldmodel.set_basis_order(2)
        fieldmodel.init()
        fieldmodel.run(fieldparams)
        fieldmodel.store_model_state(self._tmpdir, self.fieldmodelname)
        
        neuronparams['field'] = fieldmodel
        neuronparams['impedance'] = fieldmodel.get_impedance()
        
        vta_min = Threshold_Volume()
        vta_min.compute_activation_range(self._ncpus, neuronparams, neuronparams['stim_min'])
        vta_min.store_state(self._tmpdir, self.axonsname_min)
        vta_max = Threshold_Volume()
        for point in vta_min.get_points():
            # If point is not activated, add a new initial point
            if point.get_activated() is False:
                new_point = Threshold_Point(point.get_location())
            else:
                new_point = point
            vta_max.add_point(new_point)
        # Compute activation range for stim_max
        vta_max.compute_activation_range(self._ncpus, neuronparams, neuronparams['stim_max'])
        vta_max.store_state(self._tmpdir, self.axonsname_max)
        volume_shell = vta_max.get_volume_shell(vta_min)   
        # perform accurate computation of the minimal stimulation amplitude at 
        # each location in the shell
        volume_shell.compute_optimal_activation(self._ncpus, neuronparams['stim_max'])
        volume_shell.store_state(self._tmpdir, self.axonsname_shell)
        
        vta_non_adaptive = Threshold_Volume()
        max_extend = 0
        for point in vta_max.get_points():
            normal_index = point.get_location().get_normal()
            if (max_extend < normal_index):
                max_extend = normal_index
                
        ref_point = vta_max.get_point(Threshold_Point(Threshold_Location(0, 0, 0)))
        axon = ref_point.get_axon()
        dbs_signal = ref_point.get_dbs_signal()
        gstep = neuronparams['g_step']
        Z = neuronparams['impedance']
        for t_idx in vta_max.get_planes()[0].get_lines().keys():
            for n_idx in range(0,max_extend+1):
                loc = Threshold_Location(0, t_idx, n_idx)
                point = Threshold_Point(loc)
                
                coordinate = np.array([0,0.85e-3+loc.get_normal()*gstep,0+loc.get_tangential()*gstep])
                transform = point.get_transform()
                transform.set_position_vector(coordinate)
                nodes = transform.transform_coords(axon.get_nodes())
                point.set_coordinate(nodes[0]/2+nodes[-1]/2)
                point.set_axon(axon)
                point.set_dbs_signal(dbs_signal)
                potential = fieldmodel.evaluate_quantity(fieldmodel.get_potential(), nodes)
                point.set_potential(potential)
                vta_non_adaptive.add_point(point)
                
        vta_non_adaptive.compute_optimal_activation(self._ncpus, neuronparams['stim_max'])
        vta_non_adaptive.store_state(self._tmpdir, self.axonsname_non_adaptive)