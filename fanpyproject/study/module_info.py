'''
Created on Apr 21, 2017

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPyStudy
'''

def handle_import_error(message):
    module_info = {
        'fenics':{
            'version': '2016.2.0',
            'pip': False,
            'url': 'https://fenicsproject.org/download/'
            },
        'matplotlib':{
            'version': '1.5.1',
            'pip': True,
            'url': 'http://www.matplotlib.org'
            },
        'numpy':{
            'version': '1.11.0',
            'pip': True,
            'url': 'http://www.numpy.org'
            },
        'scipy':{
            'version': '0.17.0',
            'pip': True,
            'url': 'https://www.scipy.org'
            },
        'neuron':{
            'version': '7.4',
            'pip': False,
            'url': 'https://www.neuron.yale.edu/neuron/',
            'msg': ("NEURON has to be installed by source with the python API."
                    +"Information on how to install it can  be found on the homepage.")
            }, 
        'pyevtk':{
            'version': '1.0.0',
            'pip': True,
            'url': 'https://bitbucket.org/pauloh/pyevtk'
            },
        'h5py':{
            'version': '2.6.0',
            'pip': True,
            'url': 'http://h5py.org'
            },
        'cPickle':{
            'version': '1.71',
            'pip': True,
            'url': 'https://wiki.python.org/moin/UsingPickle'
            },
        'enum':{
            'pip': True,
            },
        'fanpy':{
            'pip': False,
            'msg': "Check if FanPy folder is added to the python path."
            }           
        }
    msgmodule = message.replace('No module named ','')
    
    known_package = False
    for module_name, infos in module_info.items():
        if msgmodule.startswith(module_name):
            known_package = True
            print "Couldn't find module '"+module_name+"'."
            if 'version' in infos:
                print "Version "+infos['version']+" was used in this study."
            if 'msg' in infos:
                print infos['msg']
            if infos['pip'] == True:
                print "Try install it via pip with 'pip install "+module_name+" --user'."
            if 'url' in infos:
                print "For more information visit "+infos['url']
    if not known_package:
        print message