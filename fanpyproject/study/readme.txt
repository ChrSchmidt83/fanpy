FanPyStudy is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FanPyStudy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with FanPyStudy. If not, see <http://www.gnu.org/licenses/>.


When using this software please cite the following publication:
Schmidt, C and van Rienen, U, Adaptive Estimation of the Neural Activation
Extent during Deep Brain Stimulation. 2017, IEEE Transactions on Biomedical
Engineering, vol xx, pages xx (Full information given on publication)