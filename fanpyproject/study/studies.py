'''
Created on Mar 29, 2017

@author: Christian Schmidt cschmidt18057 (at) gmail.com
Copyright 2017
This file is part of FanPyStudy
'''

import sys
sys.path.append("../")

import module_info

try:
    from fanpystudies.reproduce_mcinytre2004 import ReproduceMcIntyre2004
    import matplotlib.pyplot as plt
    from fanpystudies.approx_vta_by_field_thresholds import ApproximateVTAByFieldThresholds
    from fanpystudies.threshold_distance_by_axon_diameters import ThresholdDistanceByAxonDiameters
    from fanpystudies.analytic_validation import AnalyticValidationModel
    from fanpystudies.visualize_adaptive_algorithm import VisualizeAdaptiveAlgorithm
    from fanpystudies.approx_vta_by_field_thresholds_scheme_run import ApproximateVTAByFieldThresholdsSchemeRun
    from fanpystudies.compare_field_solution_for_basis_order import CompareFieldSolutionForBasisOrder
except ImportError, e:
    module_info.handle_import_error(e.message)
    sys.exit()

params = dict()
''' debug mode: use coarser spatial resolution and time steps, shorter stimulation
pulses and lower ansatz functions for solving the field problem.
Comment out for running in normal mode. '''
params['debug'] = True

''' number of processes which should be used for neural activation computation.
Comment out to use all available physical cores. '''
params['ncpus'] = 1


''' run the studies '''

'''  compare field solution and derived quantities for quadratic and cubic
ansatz functions. (debug mode for linear and quadratic) '''
CompareFieldSolutionForBasisOrder(params).run()

''' generate figure data for visualizing the adaptive scheme for the estimation
of the neural activation extent '''
VisualizeAdaptiveAlgorithm(params).run()

''' compare field solution of the computational model solved with FEniCS with
the analytical solution for a layered sphere test model 
(debug mode: linear ansatz functions) '''
AnalyticValidationModel(params).run()

''' reproduce the data from the McIntyre et al 2004 paper Figure 2.
(debug mode: linear ansatz functions, coarser time step
and shorter pulse)'''
ReproduceMcIntyre2004(params).run()

''' investigate threshold distance relationship for varying fiber diameter
(debug mode: linear ansatz functions, coarser time step
and shorter pulse)'''
ThresholdDistanceByAxonDiameters(params).run()

''' apprximate the neural activation extent estimated by the adaptive scheme with
field threshold values of the electric potential and electric field norm for
fiber diameters 5.7 um to 10.0 um
(debug mode: linear ansatz functions, coarser spatial resoultion, time step
and shorter pulse)'''
ApproximateVTAByFieldThresholds(params, 2.0).run()
ApproximateVTAByFieldThresholds(params, 3.0).run()
ApproximateVTAByFieldThresholds(params, 5.7).run()
ApproximateVTAByFieldThresholds(params, 7.3).run()
ApproximateVTAByFieldThresholds(params, 8.7).run()
ApproximateVTAByFieldThresholds(params, 10.0).run()

''' Evaluate the computation time for the threshold distance approximation approach
(debug mode: linear ansatz functions, coarser spatial resoultion, time step
and shorter pulse)'''
ApproximateVTAByFieldThresholdsSchemeRun(params, 5.7).run()


''' post-processing '''
CompareFieldSolutionForBasisOrder(params).postprocessing()
VisualizeAdaptiveAlgorithm(params).postprocessing()
AnalyticValidationModel(params).postprocessing()
ReproduceMcIntyre2004(params).postprocessing()
ThresholdDistanceByAxonDiameters(params).postprocessing()
ApproximateVTAByFieldThresholds(params, 2.0).postprocessing()
ApproximateVTAByFieldThresholds(params, 3.0).postprocessing()
ApproximateVTAByFieldThresholds(params, 5.7).postprocessing()
ApproximateVTAByFieldThresholds(params, 7.3).postprocessing()
ApproximateVTAByFieldThresholds(params, 8.7).postprocessing()
ApproximateVTAByFieldThresholds(params, 10.0).postprocessing()
ApproximateVTAByFieldThresholdsSchemeRun(params, 5.7).postprocessing()

''' show plots '''
plt.show()



            
    
